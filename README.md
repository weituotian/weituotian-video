
# 驮神Java EE开发框架
---

大家好，我是韦驮天，今天给大家带来我的毕业设计项目，驮神java开发框架。  
驮神开发框架是一款Java EE开发的权限系统。基于 SpringMVC 4.35 + Spring 4.35 + Hibernater 5.25 + shiro + freemarker + AdminLTE Iframe

<a href="docs/imgs/logo.jpg"><img src="docs/imgs/logo.jpg"/></a>

## feature
1. 后台UI模版[AdminLte With Iframe](https://git.oschina.net/weituotian/AdminLTE-With-Iframe), 多tab页面，自适应后台(IE9+)
1. 使用Hibernate的JPA用法(参考了Spring Data JPA的源码)，hibernate动态搜索条件加分页用JPA的Criterial API，无动态搜索的直接用jpql查询
1. 基于url的权限框架，shiro自定义过滤器, 整合ehcache，修复登录容易超时问题
1. BaseController, BaseService, BaseDao 封装常用操作
1. 基于shiro实现自动登录过滤器，APP用户可用
1. 对hibernate查询进行了优化, sql语句每个页面控制在4条之内。处理懒加载问题时，尽早将需要的实体或者字段fetch出来，避免查询更多sql
1. 解决shiro realm导致的service层不能注解事务的问题
1. js在前台生成树，后台不再生成树，优化服务器性能



## 参考项目
1. [spring-shiro-training](https://git.oschina.net/wangzhixuan/spring-shiro-training)
1. [SpringMVC-Mybatis-shiro](https://github.com/baichengzhou/SpringMVC-Mybatis-shiro)
1. 后台ui框架 [AdminLte With Iframe](https://git.oschina.net/weituotian/AdminLTE-With-Iframe)

## 截图

### 电脑端
*用户*
<a href="docs/imgs/user.jpg"><img src="docs/imgs/user.jpg"/></a>
*角色*
<a href="docs/imgs/role.jpg"><img src="docs/imgs/role.jpg"/></a>
*资源*
<a href="docs/imgs/res.jpg"><img src="docs/imgs/res.jpg"/></a>
*分配资源*
<a href="docs/imgs/assignres.jpg"><img src="docs/imgs/assignres.jpg"/></a>
*分配角色*
<a href="docs/imgs/assignrole.jpg"><img src="docs/imgs/assignrole.jpg"/></a>
*菜单动态管理*
<a href="docs/imgs/menu.jpg"><img src="docs/imgs/menu.jpg"/></a>

### 手机端
<a href="docs/imgs/m_assign_menu.jpg"><img src="docs/imgs/m_assign_menu.jpg" width="30%"/></a>
<a href="docs/imgs/m_assign_role.jpg"><img src="docs/imgs/m_assign_role.jpg" width="30%"/></a>
<a href="docs/imgs/m_user_edit.jpg"><img src="docs/imgs/m_user_edit.jpg" width="30%"/></a>



## 部署步骤
1. 使用mysql数据库，导入doc文件下的sql(**docs/sql/rbac_hibernate20170421.sql**)
1. 修改hibernater_config.property的文件, 修改为自己的mysql的连接配置
```
jdbc_url=jdbc:mysql://localhost:3306/rbac_hibernate?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull
jdbc_username=root
jdbc_password=root
```
3. 假设tomcat或者jetty容器设置的context路径为webx，那么访问***http://localhost:8080/webx/login*** 为后台登录页面

### 初始账号
最高管理员 admin/123123

## 数据库设计

基于rbac, 数据库表描述
| 数据库表名称 | 数据库表描述 |
| --- | --- |
| user | 用户表，所有登录后台的用户 |
| role | 角色表，后台用户角色 |
| resource | 资源表，基于url的资源管理 |
| user\_role | 用户角色表 |
| role\_resource | 角色资源关联表 |
| menu | 菜单表，后台菜单 |

user 用户表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| loginname | varchar(64) | NO | 否 | 登录名 |
| password | varchar(64) | NO | 否 | 密码，md5加密 |
| email | varchar(40) | NO | 否 | 邮箱 |
| name | varchar(64) | YES | 否 | 姓名 |
| usertype | int(2) | YES | 否 | 0为普通用户,1为管理员 |
| status | enum(&#39;OPEN&#39;,&#39;CLOSE&#39;) | YES | 否 | 状态 |

role 角色表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | id |
| name | varchar(40) | NO | 否 | 名字 |
| seq | int(11) | NO | 否 | 顺序 |
| status | enum(&#39;CLOSE&#39;,&#39;OPEN&#39;) | NO | 否 | 状态,1正常,0停用 |

user\_role 用户角色表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| user\_id | int(11) | NO | 是 | 用户id |
| role\_id | int(11) | NO | 是 | 角色id |

resource 资源表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | 资源id |
| moudle\_id | int(11) | YES | 否 | 模块id |
| name | varchar(40) | NO | 否 | 名字 |
| url | varchar(100) | NO | 否 | 资源url |
| status | enum(&#39;CLOSE&#39;,&#39;OPEN&#39;) | NO | 否 | 状态 |
| controller | varchar(255) | NO | 否 | 控制器 |
| method | varchar(255) | NO | 否 | 方法 |
| moudle | varchar(255) | NO | 否 | 模块名称 |

role\_resource 角色资源关联表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| role\_id | int(11) | NO | 是 | 角色id |
| resource\_id | int(11) | NO | 是 | 资源id |



---
---
---

# 配套实例

## 韦驮天视频

基于驮神开发框架。在其权限系统的基础上开发了一个视频网站的实现。

## 使用此后台的手机APP

[韦驮天视频的android app](https://git.oschina.net/weituotian/WeituotianVideoAndroidApp)

## 安装须知

- WEB-INF同级目录下创建upload文件夹，再创建子文件夹
upload
avater
cover
file
images
video
用来存放上传的头像，截图和视频


### 初始账号

会员帐号: weituotian/123123

## feature

* 修改dmuploader上传插件，上传图片可裁剪，上传可显示速度
* 利用hibernate中的继续关系，Member会员实体继承于User后台用户实体，共用主键
* 使用idea插件生成vo和entity转换代码

##　截图

### 电脑端


*首页*
<a href="docs/imgs/home.jpg"><img src="docs/imgs/home.jpg"/></a>
*登录*
<a href="docs/imgs/login.png"><img src="docs/imgs/login.png"/></a>
*注册*
<a href="docs/imgs/reg.jpg"><img src="docs/imgs/reg.jpg"/></a>
*评论管理*
<a href="docs/imgs/comment.jpg"><img src="docs/imgs/comment.jpg"/></a>
*后台首页*
<a href="docs/imgs/index.jpg"><img src="docs/imgs/index.jpg"/></a>
<a href="docs/imgs/member.jpg"><img src="docs/imgs/member.jpg"/></a>
<a href="docs/imgs/member_edit.jpg"><img src="docs/imgs/member_edit.jpg"/></a>
*会员首页*
<a href="docs/imgs/member_index.jpg"><img src="docs/imgs/member_index.jpg"/></a>
*视频管理*
<a href="docs/imgs/video.jpg"><img src="docs/imgs/video.jpg"/></a>
头像上传
<a href="docs/imgs/avatar.jpg"><img src="docs/imgs/avatar.jpg"/></a>


### 手机端
<a href="docs/imgs/m_index.jpg"><img src="docs/imgs/m_index.jpg" width="30%"/></a>
<a href="docs/imgs/m_member_edit.jpg"><img src="docs/imgs/m_member_edit.jpg" width="30%"/></a>
<a href="docs/imgs/m_member_list.jpg"><img src="docs/imgs/m_member_list.jpg" width="30%"/></a>
<a href="docs/imgs/m_member_list2.jpg"><img src="docs/imgs/m_member_list2.jpg" width="30%"/></a>
<a href="docs/imgs/m_menu.jpg"><img src="docs/imgs/m_menu.jpg" width="30%"/></a>
<a href="docs/imgs/m_view_video.jpg"><img src="docs/imgs/m_view_video.jpg" width="30%"/></a>

## 用例
<a href="docs/imgs/yongli2.png"><img src="docs/imgs/yongli2.png"/></a>

## 数据库
数据库表描述
| 数据库表名称 | 数据库表描述 |
| --- | --- |
| attachment | 附件表，统一管理封面，头像和视频 |
| comment | 评论表，视频的评论 |
| member | 会员表，app的会员 |
| member\_collect | 收藏表，会员收藏的视频 |
| member\_follow | 关注表，会员关注 |
| patition | 分区表，视频分区 |
| video | 视频表 |
| video\_tag | 视频标签关联表 |
| video\_tags | 视频标签表 |

attachment 附件表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| date | date | YES | 否 | 提交时间 |
| path | varchar(255) | YES | 否 | 路径 |
| temp | bit(1) | NO | 否 | 是否临时的 |
| type | varchar(255) | YES | 否 | Mime类型 |
| userId | int(11) | YES | 否 | 上传的用户id |
| filename | varchar(255) | YES | 否 | 文件名 |

comment 评论表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| content | varchar(255) | YES | 否 | 内容 |
| postTime | datetime | YES | 否 | 回复时间 |
| member\_id | int(11) | YES | 否 | 会员id |
| video\_id | int(11) | YES | 否 | 视频id |

member 会员表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| address | varchar(255) | YES | 否 | 地址 |
| avatar | varchar(255) | YES | 否 | 头像 |
| birthDate | date | YES | 否 | 生日 |
| descript | varchar(255) | YES | 否 | 签名描述 |
| fans | int(11) | YES | 否 | 粉丝数 |
| level | int(11) | YES | 否 | 等级 |
| regDate | date | YES | 否 | 注册日期 |
| sex | enum(&#39;UNKNOW&#39;,&#39;FEMALE&#39;,&#39;MALE&#39;) | YES | 否 | 性别enum |
| videos | int(11) | YES | 否 | 投稿视频数量 |
| follows | int(11) | YES | 否 | 关注的人数 |
| experience | int(11) | YES | 否 | 经验 |

member\_collect 用户收藏表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| member\_id | int(11) | NO | 否 | 会员id |
| video\_id | int(11) | NO | 否 |   |

member\_follow 用户关注表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| member\_id | int(11) | NO | 否 | 用户id |
| up\_id | int(11) | NO | 否 | 关注的用户id |

menu 菜单表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| pid | int(11) | YES | 否 | 父菜单id |
| text | varchar(40) | YES | 否 | 显示的名称 |
| status | enum(&#39;CLOSE&#39;,&#39;OPEN&#39;) | YES | 否 | 状态 |
| seq | smallint(6) | YES | 否 | 排序 |
| icon | varchar(100) | YES | 否 | 图标 |
| resource\_id | int(11) | YES | 否 | 关联的资源id |

patition 分区表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| name | varchar(255) | YES | 否 | 分区名 |

reply 回复表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| content | varchar(255) | YES | 否 | 内容 |
| postTime | datetime | YES | 否 | 提交时间 |
| comment\_id | int(11) | YES | 否 | 评论id |
| member\_id | int(11) | YES | 否 | 会员id |



video 视频表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| auditTime | datetime | YES | 否 | 审核通过时间 |
| click | int(11) | YES | 否 | 点击数量 |
| collect | int(11) | YES | 否 | 收藏数量 |
| cover | varchar(255) | YES | 否 | 封面路径 |
| createTime | datetime | YES | 否 | 创建时间 |
| descript | varchar(255) | YES | 否 | 视频描述 |
| path | varchar(255) | YES | 否 | 路径 |
| play | int(11) | YES | 否 | 播放数量 |
| totalTime | int(11) | YES | 否 | 视频总时间 |
| updateTime | datetime | YES | 否 | 更新时间 |
| videoState | enum(&#39;Auditing&#39;,&#39;Uncommitted&#39;,&#39;ReEdit&#39;,&#39;AuditFailure&#39;,&#39;Audited&#39;) | YES | 否 | 视频状态 |
| member\_id | int(11) | YES | 否 | 上传的会员id |
| partition\_id | int(11) | YES | 否 | 分区id |
| title | varchar(255) | YES | 否 | 标题 |
| attachment\_id | int(11) | YES | 否 | 附件id |
| open\_state | enum(&#39;OPEN&#39;,&#39;CLOSE&#39;) | YES | 否 | 是否对外公开 |

video\_tag 视频标签关联表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| video\_id | int(11) | NO | 否 | 视频id |
| tag\_id | int(11) | NO | 否 | 标签id |

video\_tags 视频标签表

| 字段 | 类型 | 是否为空 | 主键 | 注释 |
| --- | --- | --- | --- | --- |
| id | int(11) | NO | 是 | Id |
| createTime | datetime | YES | 否 | 创建时间 |
| name | varchar(255) | YES | 否 | 标签名 |

会员与视频模块之间的关系，视频可以有多个评论，多个标签，对应只有一个附件保存视频MP4路径。用户可以发表多个评论，关注多个用户，收藏多个视频

## todo 待完成任务:
- 参数验证
- 系统日志
- 增加js插件 密码equal to的功能 
- ~~优化search_~~
- 优化菜单jstree
- 用户验证
- 不同登录类型选择不同的shiro realm
- favicon
- ~~字体~~
- springmvc 访问频率验证 ,ehcache缓存
- 字典管理，附件管理
- 实现自定义注解

## 解决了的问题

- 修改上传插件dmuploader，支持上传速度显示。
- 整合Cropper和dmuploader，裁剪后ajax上传图片(ie9+)。
- Spring下在controller读取properties文件
- mybatis,springMVC,freemarker中对enum的使用
- freemarker模板与springmvc配置,引用相对路径模板文件
- springmvc使用@ModelAttribute,RedirectAttributes.addFlashAttribute重定向,url可不带重定向参数
- jstree,checkbox插件的用法,折叠全部,打开所有选择的节点
- freemarker使用shiro的标签

## 参考博客
[点此](reference.md)

## 关于我
[b站主页](http://space.bilibili.com/1604165/#!/)  
<a href="docs/imgs/weixin.jpg"><img style="height:300px;width:300px" src="docs/imgs/weixin.jpg"/></a>