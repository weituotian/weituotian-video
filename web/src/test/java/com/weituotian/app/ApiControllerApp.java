package com.weituotian.app;

import com.weituotian.base.BaseTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Created by ange on 2017/3/23.
 */
public class ApiControllerApp extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testUserVideos() throws Exception {
        //准备参数

        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/member/videos/")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("userId", "104")
                .param("page", "1")
                .param("pageSize", "10")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }

    @Test
    public void testUserInfo() throws Exception{
        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/member/info/104")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }

    @Test
    public void testUserStars() throws Exception{
        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/member/stars")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("userId", "104")
                .param("page", "1")
                .param("pageSize", "10")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }

    @Test
    public void testUserCollects() throws Exception{
        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/member/collects")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("userId", "104")
                .param("page", "1")
                .param("pageSize", "10")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }

    @Test
    public void testComments() throws Exception{
        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/video/av/comments")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("videoId", "2")
                .param("page", "1")
                .param("pageSize", "10")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }

    @Test
    public void addComment() throws Exception{
        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/member/my/addcomment")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("videoId", "2")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
    }
}
