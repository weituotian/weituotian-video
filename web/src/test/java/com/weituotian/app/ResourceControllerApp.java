package com.weituotian.app;


import com.weituotian.base.BaseTest;
import com.weituotian.system.entity.Resource;
import com.weituotian.system.service.IResourceService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;


public class ResourceControllerApp extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testAutoCreate() throws Exception {
        //准备参数

        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/resource/list")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("controller", "menu")
                .param("descript", "菜单")
                .param("moudleName", "系统管理")
                .param("controllerName", "菜单管理")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
        //被shiro重定向
    }
}
