package com.weituotian.app;

import com.weituotian.base.BaseTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Created by ange on 2017/3/16.
 */
public class LoginControllerApp extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testLogin() throws Exception {
        //准备参数

        //发送请求
        ResultActions resultActions = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/login")
                .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_JSON)
                .param("username", "weituotian")
                .param("password", "1q2w3e4r")
        );
        MvcResult mvcResult = resultActions.andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        logger.info("=====客户端获得反馈数据:" + result);
        //被shiro重定向
    }

}
