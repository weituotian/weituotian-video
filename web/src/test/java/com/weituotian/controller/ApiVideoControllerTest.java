package com.weituotian.controller;

import com.weituotian.base.BaseTest;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.service.IVideoService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * Created by ange on 2017/3/23.
 */
public class ApiVideoControllerTest extends BaseTest {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IVideoService videoService;

    @Test
    public void testList() {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(1, 10);
        pageInfo.setCondition(new HashMap<String, Object>());
        videoService.findPageMemberVo(104, pageInfo);
    }

    @Test
    public void testListCriterial(){
        PageInfo<Video> pageInfo = new PageInfo<>(1, 10);
        pageInfo.setCondition(new HashMap<String, Object>());
        videoService.findPageMember(104, pageInfo);
    }
}
