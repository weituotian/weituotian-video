package com.weituotian.controller;


import com.weituotian.base.BaseTest;
import com.weituotian.system.entity.Resource;
import com.weituotian.system.dao.impl.ResourceDao;
import com.weituotian.system.service.IResourceService;
import com.weituotian.system.service.impl.ResourceService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional

public class ResourceControllerTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @javax.annotation.Resource
    private IResourceService resourceService;


    @Test
    public void testPersist() {
        logger.info("--------testPersist---------");

        String moudleName = "系统管理";
        String controllerName = "菜单管理";
        String controller = "menu";
        String descript = "菜单";

        Resource resource = resourceService.createDefaultResource(moudleName, controllerName);
        resource.setUrl("/" + controller + "/list");
        resource.setName(descript + "列表");
        resourceService.add(resource);

        Resource resource1 = resourceService.createDefaultResource(moudleName, controllerName);
        resource1.setUrl("/" + controller + "/add");
        resource1.setName(descript + "新建");
        resourceService.add(resource1);

        Resource resource2 = resourceService.createDefaultResource(moudleName, controllerName);
        resource2.setUrl("/" + controller + "/doadd");
        resource2.setName(descript + "处理新建");
        resourceService.add(resource2);

        Resource resource3 = resourceService.createDefaultResource(moudleName, controllerName);
        resource3.setUrl("/" + controller + "/edit");
        resource3.setName(descript + "编辑");
        resourceService.add(resource3);

        Resource resource4 = resourceService.createDefaultResource(moudleName, controllerName);
        resource4.setUrl("/" + controller + "/doedit");
        resource4.setName(descript + "处理编辑");
        resourceService.add(resource4);

        Resource resource5 = resourceService.createDefaultResource(moudleName, controllerName);
        resource5.setUrl("/" + controller + "/delete");
        resource5.setName(descript + "删除");
        resourceService.add(resource5);


        logger.info("--------testPersist end---------");
    }
}
