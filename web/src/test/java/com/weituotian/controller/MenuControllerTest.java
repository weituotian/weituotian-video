package com.weituotian.controller;

import com.weituotian.base.BaseTest;
import com.weituotian.system.service.IMenuService;
import com.weituotian.system.service.impl.UserService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * Created by ange on 2017/2/23.
 */
public class MenuControllerTest extends BaseTest {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IMenuService menuService;

    @Test
    public void testFindAll() {
        menuService.findAll();
    }
}
