package com.weituotian.controller;

import com.weituotian.base.BaseTest;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.api.entity.vo.AppMember;
import com.weituotian.moudle.front.entity.CommentVo;
import com.weituotian.moudle.front.service.ICommentService;
import com.weituotian.moudle.front.service.IFrontVideoService;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.service.IMemberService;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-03-25.
 */
public class MemberControllerTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ICommentService commentService;

    @Resource
    private IMemberService memberService;

    @Resource
    private IFrontVideoService frontVideoService;

    @Before
    public void setup() {
        logger.info("--------------setup了------------------");
    }


    @Test
    public void testAddComment() {

        commentService.addComment(104, "testAddComment2", 2);

    }

    @Test
    public void getcomments() {
        logger.info("*******************************start test**------------------");
        PageInfo<CommentVo> pageInfo = new PageInfo<>(1, 10);
        List<CommentVo> commentVos = commentService.getCommentVos(2, pageInfo);
        int max = pageInfo.getTotal() - pageInfo.getFrom();
        for (CommentVo commentVo : commentVos) {
            commentVo.setUserAvatar("" + commentVo.getUserAvatar());
            commentVo.setFloor(max);
            max--;
        }
        logger.info("comments:{}", commentVos);
    }

    @Test
    public void testCollect() {
        logger.info("--------------开始testCollect------------------");
        Integer size = memberService.addCollect(104, 2);
        logger.info("获得大小size:{}", size);

//        memberService.cancelCollect(104, 2);
    }

    @Test
    public void testCancelCollect() {
        logger.info("--------------开始testCancelCollect------------------");
        Integer size = memberService.cancelCollect(104, 2);
        logger.info("获得大小size:{}", size);

//        memberService.cancelCollect(104, 2);
    }

    @Test
    public void testGetCollects() {

        logger.info("--------------开始testGetCollects------------------");
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(1, 10);
        memberService.getCollects(104, pageInfo);
        logger.info("获得collects:{}", pageInfo);

    }

    @Test
    public void testStar() {
        logger.info("--------------开始testStar------------------");
        memberService.starMember(104, 105);
    }

    @Test
    public void testCancelStar() {
        logger.info("--------------开始testStar------------------");
        memberService.cancelStar(104, 105);
    }

    @Test
    public void testGetStars() {

        logger.info("--------------开始testGetStars------------------");
        PageInfo<AppMember> pageInfo = new PageInfo<>(1, 10);
        memberService.getStars(104, pageInfo);
        logger.info("获得stars:{}", pageInfo);

    }

    @Test
    public void testGetPartitionVideos() {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(1, 4);
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("partition", 1);
        pageInfo.setCondition(conditions);
        frontVideoService.findLastest(pageInfo);
        logger.info("列表:{}", pageInfo.getList());
    }
}
