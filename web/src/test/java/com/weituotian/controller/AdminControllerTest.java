package com.weituotian.controller;

import com.weituotian.base.BaseTest;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.admin.service.IAdminCommentService;
import com.weituotian.moudle.admin.service.IAdminVideoService;
import com.weituotian.moudle.admin.service.impl.AdminCommentService;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ange on 2017/4/3.
 */
public class AdminControllerTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IAdminVideoService adminVideoService;

    @Autowired
    private IAdminCommentService commentService;

    @Test
    public void testIndex() {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(1, 10);
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("partition", "1");
        pageInfo.setCondition(conditions);
        adminVideoService.findPage(pageInfo);
        logger.info("结果:{}", pageInfo);
    }

    @Test
    public void testDeleteVideos() {
        Integer[] ids = new Integer[]{3};
        adminVideoService.delete(ids);
    }

    @Test
    public void  testCommentIndex(){
        PageInfo<AdminCommentVo> pageInfo = new PageInfo<>(1, 10);
        Map<String, Object> conditions = new HashMap<>();
        pageInfo.setCondition(conditions);
        commentService.findPageVo(pageInfo);
        logger.info("结果:{}", pageInfo);
    }
}
