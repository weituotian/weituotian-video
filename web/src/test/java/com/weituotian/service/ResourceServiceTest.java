package com.weituotian.service;


import com.weituotian.base.BaseTest;
import com.weituotian.system.dao.impl.ResourceDao;
import com.weituotian.system.entity.Resource;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.service.impl.ResourceService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

//import javax.annotation.Resource;
import java.util.List;

@Transactional

public class ResourceServiceTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @javax.annotation.Resource
    private ResourceDao resourceDao;

    //    @Resource
//    private ResourceMapper resourceMapper
    @Test
    public void testMenu() {
        /*List<com.weituotian.system.model.Resource> resources = resourceMapper.getAllMenus();
        System.out.println(toJson(resources));
        resources = resourceMapper.getAllMenus();
        System.out.println(toJson(resources));*/
    }

    @Test
    public void testResourceTree() {
        /*logger.debug(ArrayUtils.toString(resourceMapper.getResourceJsTree(0)));*/
    }

    @Test
    public void testResourceSqls() {
        logger.debug("--------testResourceSqls---------");
        List<com.weituotian.system.entity.Resource> resources = resourceDao.findResourcesByUserId(17);
        logger.debug(String.valueOf(resources));
        logger.debug("--------testResourceSqls end---------");

    }

    @Test
    public void testPersistOne() {
        logger.debug("--------testPersistOne---------");


        String moudleName = "系统管理1";
        String controllerName = "菜单管理";
        String controller = "menu";
        String descript = "菜单";

        Resource resource = createDefaultResource(moudleName, controllerName);
        resource.setUrl("/" + controller + "/list");
        resource.setName(descript + "列表");
        resourceDao.persist(resource);


        resourceDao.getSession().getTransaction().commit();


        logger.debug("--------testPersistOne end---------");
    }

    @Test
    public void testPersist() {

        //解决办法，esource的主键由int改为integerR，增加@GeneratedValue(strategy=GenerationType.AUTO)
        //在controller层解决

        logger.debug("--------testPersist---------");

        String moudleName = "系统管理";
        String controllerName = "菜单管理";
        String controller = "menu";
        String descript = "菜单";

        Resource resource = createDefaultResource(moudleName, controllerName);
        resource.setUrl("/" + controller + "/list");
        resource.setName(descript + "列表");
        resourceDao.persist(resource);

        Resource resource1 = createDefaultResource(moudleName, controllerName);
        resource1.setUrl("/" + controller + "/add");
        resource1.setName(descript + "新建");
        resourceDao.persist(resource1);


        Resource resource2 = createDefaultResource(moudleName, controllerName);
        resource2.setUrl("/" + controller + "/doadd");
        resource2.setName(descript + "处理新建");
        resourceDao.persist(resource2);

        Resource resource3 = createDefaultResource(moudleName, controllerName);
        resource3.setUrl("/" + controller + "/edit");
        resource3.setName(descript + "编辑");
        resourceDao.persist(resource3);

        Resource resource4 = createDefaultResource(moudleName, controllerName);
        resource4.setUrl("/" + controller + "/doedit");
        resource4.setName(descript + "处理编辑");
        resourceDao.persist(resource4);

        Resource resource5 = createDefaultResource(moudleName, controllerName);
        resource5.setUrl("/" + controller + "/delete");
        resource5.setName(descript + "删除");
        resourceDao.persist(resource5);

        resourceDao.getSession().getTransaction().commit();

        logger.debug("--------testPersist end---------");
    }

    private Resource createDefaultResource(String moudleName, String controllerName) {
        Resource resource = new Resource();
        resource.setStatus(OpenState.OPEN);
        resource.setId(null);
        resource.setMoudle(moudleName);
        resource.setController(controllerName);
        resource.setMethod("");
        return resource;
    }
}
