package com.weituotian.service;

import com.weituotian.base.BaseTest;
import com.weituotian.system.dao.IRoleDao;
import com.weituotian.system.entity.Role;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ange on 2016/9/16.
 */
@Transactional
public class RoleServiceTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private IRoleDao roleDao;


    @Test
    public void testFindRolesByUserId() {
        List<Role> roles = roleDao.findRolesByUserId(104);
        logger.info("{}", roles);
    }
}
