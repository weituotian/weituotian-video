package com.weituotian.service;

import com.weituotian.base.BaseTest;
import com.weituotian.moudle.member.dao.ICommentDao;
import com.weituotian.moudle.member.dao.IMemberDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.Comment;
import com.weituotian.moudle.member.entity.Reply;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017-03-25.
 */
public class MemberServiceTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ICommentDao commentDao;

    @Resource
    private IVideoDao videoDao;

    @Resource
    private IMemberDao memberDao;

    @Before
    public void setup() {
        logger.info("----------------------setup----------------------");
    }

    @Test
    @Transactional
    public void testFind() {
        logger.info("----------------------测试开始----------------------");
        Comment comment = commentDao.findOne(1);
        List<Reply> replies = comment.getReplies();
        logger.info("----------------------输出开始----------------------");
        logger.info("replies:{}", replies);
    }

    @Test
    @Transactional
    public void testAddComment() {
        logger.info("----------------------测试开始----------------------");

    }

    @Test
    @Transactional
    public void testCollect() {
        boolean result = memberDao.checkCollect(104, 3);
        logger.info("测试结果:result:{}", result);
    }

    @Test
    @Transactional
    public void testStar() {
        boolean result = memberDao.checkStar(104, 105);
        logger.info("测试结果:result:{}", result);
    }
}
