package com.weituotian.service;

import com.weituotian.base.BaseTest;
import com.weituotian.system.dao.IMenuDao;
import com.weituotian.system.entity.Menu;
import org.hibernate.Session;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by ange on 2017/2/3.
 */
public class MenuServiceTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private IMenuDao menuDao;

    @Test
    @Transactional
    public void testFindMenu() {
        logger.debug("--------testFindMenu---------");
        logger.debug("--------testFindMenu---------");
        logger.debug("--------testFindMenu---------");
        Menu menu = menuDao.findOne(74);
        menu.setParent(menuDao.findOne(22));
        menuDao.persist(menu);
        logger.debug(String.valueOf(menu));
        menuDao.getSession().getTransaction().commit();

    }

    @Test
    public void testNoTransactional() {
        logger.debug("--------testNoTransactional---------");
        logger.debug("--------testNoTransactional---------");
        logger.debug("--------testNoTransactional---------");
        Session session = menuDao.openSession();
        Menu menu = session.find(Menu.class, 1);
        session.close();

        //抛出错误
        //org.hibernate.LazyInitializationException:
        // failed to lazily initialize a collection of role: com.weituotian.system.entity.Menu.children, could not initialize proxy - no Session
        System.out.println(menu.getChildren());
    }
}
