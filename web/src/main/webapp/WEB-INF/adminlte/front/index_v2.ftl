<#include "../common/htmlwrap.ftl">
<#assign assetsPath=springMacroRequestContext.contextPath+"/static/front/">

<html>
<head>
    <title>
        韦驮天视频
    </title>
    <meta charSet="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="${assetsPath}css/main.css">
    <link rel="stylesheet" type="text/css" href="${assetsPath}css/devices.css">
    <!--统一中文字体-->
    <style>
        body {
            font-family: "Microsoft YaHei", Arial, Helvetica, sans-serif, "宋体";
        }
    </style>
</head>
<body>
<header class="header">
    <div class="container-lrg flex">
        <div class="col-4">
            <div>
                <a class="logo">
                    weituotian
                </a>
            </div>
            <div>
                <a class="nav-link" href="http://weibo.com/2127382490/profile">
                    微博
                </a>
            </div>
        </div>
        <div class="col-8">
            <h1 class="heading">
                韦驮天视频,
                <div>带给你分享的乐趣</div>
            </h1>
            <h2 id="qrcode" class="paragraph">
                现在开始下载安卓客户端<span style="color: #bbff99">(点击扫码)</span>
            </h2>
            <div class="ctas">
                <a class="ctas-button" href="${apk_url}">
                    <img src="${assetsPath}img/androidlogo.svg">
                    <span id="download">
                下载apk
              </span>
                </a>
            </div>
        </div>
    </div>
    <div class="container-lrg">
        <div class="centerdevices col-12">
            <div class="androids">
                <div class="android">
                    <div class="mask">
                        <img class="mask-img" src="${assetsPath}img/mobileapp.svg">
                    </div>
                </div>
                <div class="android android__2">
                    <div class="mask">
                        <img class="mask-img" src="${assetsPath}img/mobileapp.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="feature3">

    <div class="container-lrg flex">
    <#list pageinfo.list as item>
        <div class="col-3">
            <h3 class="subheading">
            ${item.title}
            </h3>
            <p class="paragraph">
                <img src="${item.cover}" class="img-responsive" style="width: 180px"
                     alt="${item.title}"/>
            </p>
        </div>
    </#list>
        <!--<div class="col-4">
            <b class="emoji">
                🚀
            </b>
            <h3 class="subheading">
                Gorgeous Logotypes
            </h3>
            <p class="paragraph">
                We've hand travelled the depths of the internet to bring you gorgeous logotypes. Featuring the beautiful
                fonts of Connary Fagen.
            </p>
        </div>-->
    </div>
</div>
<div class="footer">
    <div class="container-sml flex text-center">
        <div class="col-12">
            <h3 class="heading">
                探索韦驮天视频的世界
            </h3>
            <div class="ctas">
                <a id="btn_upload" href="${common.reg_url}" class="ctas-button">
                    注册
                </a>
                <a id="btn_signin" href="${common.login_url}" class="ctas-button-2">
                    登录
                </a>
            </div>
        </div>
    </div>
    <div class="container-lrg footer-nav flex">
        <div class="col-3 vertical">
            <a class="logo">
                韦驮天
            </a>
            <a class="nav-link2">
                ©2017-2018 韦驮天公司
            </a>
        </div>
        <div class="col-3 vertical">
            <a class="nav-link2">
                关于
            </a>
        <#--<a class="nav-link2">
            Features
        </a>
        <a class="nav-link2">
            Pricing
        </a>-->
        </div>
        <div class="col-3 vertical">
            <a href="http://weibo.com/2127382490/profile" class="nav-link2">
                微博
            </a>
        <#--<a class="nav-link2">
            Facebook
        </a>
        <a class="nav-link2">
            Contact
        </a>-->
        </div>
        <div class="col-3 vertical">
            <a class="nav-link2">
                广东中山
            </a>
        </div>
    </div>
</div>
<!-- jQuery 2.2.3 -->
<script src="${common.staticPath}plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="${common.staticPath}plugins/layer/layer.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#qrcode").click(function () {


            $.ajax({
                url: "${qrcode_url}",
                type: "GET",
                datatype: "json",
                //timeout: 3000,
                data: {}
            }).done(function (data, textStatus) {
                var url = data;
                layer.open({
                    type: 1,
                    offset: 't',
                    title: "扫码下载",
//            area: '500px;',
                    closeBtn: 0,
                    shadeClose: true,
//            skin: 'yourclass',
                    content: '<img  style="width: 300px;height: 300px;" src="' + url + '">',
                    success: function (layero) {

                    }
                });
            }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
            });


        });
    })
</script>
</body>
</html>