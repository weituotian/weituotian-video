<#include "../common/htmlwrap.ftl">
<@html>


    <@header title=pageName>

    <!--<link rel="stylesheet" href="${common.staticPath}bootstrap/css/bootstrap-theme.min.css">-->
    <style>
        body {
            padding-top: 70px;
            padding-bottom: 30px;
        }
    </style>

    </@header>

<body>

<!--导航条-->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">${pageName}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <@shiro.authenticated>
                    <li>
                        <a id="btn_upload" href="${common.admin_url}" class="">
                            后台
                        </a>
                    </li>
                    <li>
                        <a id="btn_signin" href="${common.logout_url}" class="">
                            退出
                        </a>
                    </li>
                </@shiro.authenticated>
                <@shiro.guest>
                    <li>
                        <a id="btn_upload" href="${common.reg_url}" class="">
                            注册
                        </a>
                    </li>
                    <li>
                        <a id="btn_signin" href="${common.login_url}" class="">
                            登录
                        </a>
                    </li>
                </@shiro.guest>
            </ul>
            <form class="navbar-form">
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
</nav>

<div class="container">
    <!-- title : Popular on YouTube&ndash;&gt;
    <div class="row-fluid margin-bottom">
        <span class="glyphicon glyphicon-facetime-video font-size-big"></span>
        Movies
    </div>
    <!-- /title : Movies -->

    <div class="row-fluid">

        <#list pageinfo.list as item>
            <div class="col-md-3 col-sm-3 col-xs-10 col-xs-offset-1 text-center">
                <div class="col-sm-12"><img src="${item.cover}" class="img-responsive" style="width: 180px"
                                            alt="${item.title}"/></div>
                <div class="col-sm-12 text-center">${item.title}</div>
            </div>
        </#list>

    </div>
</div>

<h1>欢迎来到${pageName}</h1>

    <@footer>

    </@footer>
</body>



</@html>
