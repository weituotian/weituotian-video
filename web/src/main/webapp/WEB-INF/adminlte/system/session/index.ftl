<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="${pageName}">

    </@header>

<body>

    <@content_header title='${pageName}'></@content_header>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                    <form id="search_form" class="form-inline" role="form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search_name" id="search_name"
                                   value="${(pageinfo.condition['search_name'])!""}"
                                   placeholder="">
                        </div>
                        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
                    </form>
                </div>
            </div>
            <div class="box">
                <!--<div class="box-header">
                    <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="toolbar">
                        <div class="btn-group">
                            <a class="btn btn-default lr-delete" onclick="delete_selection();"><i
                                    class="fa fa-remove"></i>&nbsp;删除</a>
                            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-remove"></i>&nbsp;全选</a>
                            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                                    class="fa fa-remove"></i>&nbsp;反选</a>
                        </div>
                    </div>
                    <table id="table"
                           data-toggle="table"
                           data-mobile-responsive="true"

                           data-row-style="rowStyle2"
                           data-toolbar="#toolbar"

                           data-show-refresh="true"
                           data-show-columns="true"

                    <#--点击一行任意地方选择该行-->
                           data-click-to-select="true"

                    >
                        <thead>
                        <tr>
                            <th data-field="is_checked" data-checkbox="true"></th>
                            <th data-field="id" data-sortable="true">ID</th>
                            <th>身份</th>
                            <th>状态</th>
                            <th>开始时间</th>
                            <th>最后访问时间</th>
                            <th>超时</th>
                            <th>主机</th>
                            <th data-click-to-select="false">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                            <#list sessions as item>
                            <tr>
                                <td></td>
                                <td>${item.id}</td>
                                <td>
                                    <#if item.login>
                                    ${item.loginName}
                                        <#if item.id=curSession.id>
                                            <span style="color: red;">(我)</span>
                                        </#if>
                                    <#else>
                                    </#if>
                                </td>
                                <td>${item.status}</td>
                                <td>
                                ${item.startTimestamp?datetime!"未知时间"}
                                </td>
                                <td>
                                ${item.lastAccessTime?datetime!"未知时间"}
                                </td>
                                <td>${item.timeout}ms</td>
                                <td>${item.host}</td>
                                <td>
                                    <div class="btn-group">
                                        <@shiro.hasPermission name="/session/kick">
                                            <#if curSession.id!=item.id>
                                                <a class="btn btn-default btn_kick"
                                                   data-sid="${item.id}"
                                                   data-toggle="tooltip" data-placement="left" title="踢出用户${item.id}">
                                                    踢出
                                                </a>
                                            </#if>
                                        </@shiro.hasPermission>
                                    </div>
                                </td>
                            </tr>
                            </#list>
                        </tbody>
                    </table>
                    <div id="pagination">

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>

<#--<@section_body>-->

<#--</@section_body>-->

    <@footer_list>

    <!--bootstrap表格相关-->
    <#--<@tableCommon delete_url=delete_url></@tableCommon>-->

    <!--分页js-->
    <#--<@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>-->

    <script type="text/javascript">
        $(document).ready(function () {
            var $btn_deletes = $('a.btn_kick');

            //按钮
            $btn_deletes.click(function () {
                //本按钮
                var $this = $(this);

                $.ajax({
                    url: "${common.kick_url}",
                    type: "POST",
                    datatype: "json",
                    data: {
                        sid: $this.attr("data-sid")
                    },
                    success: function (data, textStatus) {
                        var index = layer.alert(data.msg, {icon: 3, title: '提示'}, function (index) {
                            //do something
                            if (data.success) {
                                window.location.reload(true);
                            } else {
                                layer.close(index);
                            }
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log(XMLHttpRequest + textStatus + errorThrown);
                    }
                });
            });

        });
    </script>

    </@footer_list>

</body>
</@html>