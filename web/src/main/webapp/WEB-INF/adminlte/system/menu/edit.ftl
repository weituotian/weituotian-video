<#include "../../common/htmlwrap.ftl">

<@html>

    <@header title="${pageName}">

    <#--jstree style-->
    <link rel="stylesheet" href="${common.staticPath}plugins/jstree/dist/themes/default/style.min.css">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">
                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <#if action=='edit'>
                            <!--资源id-->
                            <input name="id" value="${menu.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>
                        </#if>

                        <!--名字-->
                        <div class="form-group">
                            <label for="input_text" class="col-sm-3 control-label">菜单显示名字</label>
                            <div class="col-sm-9">
                                <input name="text" value="${menu.text!""}"
                                       class="form-control" id="input_text"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>


                        <!--父菜单-->
                        <div class="form-group">
                            <label for="input_pid" class="col-sm-3 control-label">父菜单</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span id="pmenuename">${menu.parent.text!""}</span>
                                    <input id="input_pid" name="pid" value="${menu.parent.id!""}"
                                           class="form-control"
                                           placeholder=""
                                           type="hidden"
                                           valType="num"
                                           required>
                                    <button type="button" onclick="selectParent()" class="btn btn-info">选择父菜单</button>
                                </div>
                            </div>
                        </div>

                        <!--挂接资源-->
                        <div class="form-group">
                            <label for="input_resource_id" class="col-sm-3 control-label">挂接资源</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span id="presourcename">${menu.resource.name!""}${'('+menu.resource.url!""+')'}</span>
                                    <input id="input_resource_id" name="resource_id" value="${menu.resource.id!""}"
                                           class="form-control"
                                           placeholder=""
                                           type="hidden"
                                           required>
                                    <button type="button" onclick="selectResouece()" class="btn btn-info">挂接资源</button>
                                    <button type="button" onclick="clearResource()" class="btn btn-danger">清除</button>
                                </div>
                            </div>
                        </div>


                        <!--状态-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">状态</label>
                            <div class="checkbox-list col-sm-9">
                                <#list openStatus as val>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${val.getValue()}"
                                            <#if val.getValue()==menu.status.value>
                                               checked
                                            </#if>
                                        >${val.getTitle()}</label>
                                </#list>
                            <#--<label class="checkbox-inline">
                                <input type="checkbox" value="option1"> 多选框 1 </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="option2"> 多选框 2 </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="option3" disabled> 禁用 </label>-->
                            </div>
                        </div>


                        <!--排序-->
                        <div class="form-group">
                            <label for="input_seq" class="col-sm-3 control-label">排序</label>
                            <div class="col-sm-9">
                                <input name="seq" value="${menu.seq!1}"
                                       class="form-control" id="input_seq"
                                       placeholder=""
                                       type="number"
                                       valType="number"
                                       required>
                            </div>
                        </div>


                        <!--图标-->
                        <div class="form-group">
                            <label for="input_icon" class="col-sm-3 control-label">图标</label>
                            <div class="col-sm-9">
                                <div class="form-inline">
                                    <input name="icon" value="${menu.icon!""}"
                                           class="form-control" id="input_icon"
                                           placeholder=""
                                           type="text"
                                           valType=""
                                           style="min-width: 200px;"
                                    >
                                    <span id="icon_preview" class="${menu.icon!""}"></span>
                                    <button type="button" class="btn btn-primary" onclick="openSelectIcon();">选择图标
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        <#elseif action=="add">
                            <button type="submit" class="btn btn-info pull-right">新增</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">选择父菜单</h4>
            </div>
            <div class="modal-body">
                <!--选择树-->
                <div id="menu_tree">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <#--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">挂接资源</h4>
            </div>
            <div class="modal-body">
                <!--选择树-->
                <div id="res_tree">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <#--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>

    <@footer_edit>

    <!--转换成树的支持-->
    <script src="${common.staticPath}plugins/gtree/tree.js"></script>
    <script src="${common.staticPath}plugins/gtree/dimension.tree.js"></script>
    <!--显示js tree-->
    <script src="${common.staticPath}plugins/jstree/dist/jstree.min.js"></script>

    <!--挂接资源-->
    <script type="text/javascript">

        function validform() {
            return $('#myform').Valid();
        }

        moudleList=[];
        /**
         * 挂接资源
         */
        function selectResouece() {
            var $modal = $('#myModal2');

            if (moudleList.length<=0) {
                var treeUrl = "${resources_url}";
                $('#res_tree').jstree({
                    'plugins': ["wholerow", "types"],
                    'core': {
                        "themes": {
                            "responsive": false
                        },
                        'data': function (node, callback) {
                            console.log("jstree.core.data.function:");
                            console.log(node);
                            $.ajax({
                                url: treeUrl,
                                type: "POST",
                                datatype: "json",
                                //timeout: 3000,
                                data: {
                                    "pid": node.id
                                }
                            }).done($.proxy(function (data, status, xhr) {


                                if (data.success) {

                                    var list= data.obj;

                                    $.each(list, function (n, value) {
                                        var moudleName = value.moudle;
                                        var controllerName = value.controller;
                                        $.AddJsTreeNode(moudleList, moudleName, controllerName, value);
                                    });

                                    callback(moudleList);

                                } else {
                                    layer.alert(data.msg);
                                }

                            }, this)).fail($.proxy(function (XMLHttpRequest, textStatus, errorThrown) {
                                layer.alert(errorThrown);
                            }, this));
                        }
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder icon-state-warning icon-lg"
                        },
                        "file": {
                            "icon": "fa fa-file icon-state-warning icon-lg"
                        }
                    }
                }).bind('select_node.jstree', function (event, selected) {  //绑定的点击事件
                    console.log("select_node.jstree");
//                console.log(selected);
                    var id = selected.node.original.id;

                    if (isMoudleOrControllerNode(moudleList,id)) {
//                        layer.alert("请选择可以绑定url的资源");
                    }else {
                        $("#input_resource_id").val(id);
                        $("#presourcename").text(selected.node.text);
                        $modal.modal("hide");
                    }

                });
            }

            $modal.modal("show");

        }

        function clearResource() {
            $("#input_resource_id").val(null);
            $("#presourcename").text("");
        }

        /**
         * 选择父菜单
         */
        function selectParent() {
            /*layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
//                maxWidth: 512,
//                area: '516px',
//                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#res_tree'),
                success: function (index, layero) {
                    if ($(window).width() <= 768) {
                        layer.full(layero);
                    }
                }
            });*/
            $('#myModal').modal("show");
        }

    </script>

    <!--表单-->
    <!--<script>
        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                if (!$form.Valid()) {
                    event.preventDefault();
                }
            });
        });

    </script>-->

    <!--表单-->
    <script>
        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                event.preventDefault();
                //验证表单
                if (!$form.Valid()) {
                    return;
                }
                //发送ajax请求
                $.ajax({
                    url: $form.attr("action"),
                    type: "POST",
                    datatype: "json",
                    //timeout: 3000,
                    data: $form.serialize()
                }).done(function (data, textStatus) {
                    layerMsg(data.msg, data.success, function () {
                        if (data.success) {
                            close_iframe_or_refresh();
                        }
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
                });
            });
        });

    </script>

    <!--显示菜单树-->
    <script>

        var icon_layer_index;

        function setIcon(icon) {
            $("#input_icon").val(icon);
            $("#icon_preview").attr("class", icon);
            layer.close(icon_layer_index);
        }

        function openSelectIcon() {
            icon_layer_index = layer.open({
                type: 2,
                title: '选择图标',
                shadeClose: true,
                shade: false,
                scrollbar: false,
//                maxmin: true, //开启最大化最小化按钮
                area: ['80%', '60%'],
                content: '${icon_url}'
            });
        }

        $(document).ready(function () {

            //如果有错误信息,就显示出来
            var msg = '${msg!""}';
            if (msg != '') {
                layer.alert(msg);
            }

            //初始化模态框选择
            var $modal = $('#myModal');

            var treeUrl = "${tree_url}";

            //jstree数中一项有text,icon和chilren就足够了
            $('#menu_tree').jstree({
                'plugins': ["wholerow", "types"],
                'core': {
                    "themes": {
                        "responsive": false
                    },
                    'data': function (node, callback) {
                        console.log("jstree.core.data.function:");
                        console.log(node);
                        $.ajax({
                            url: treeUrl,
                            type: "POST",
                            datatype: "json",
                            //timeout: 3000,
                            data: {
                                "pid": node.id
                            }
                        }).done($.proxy(function (data, status, xhr) {
                            if (data.success) {
                                callback(data.obj);
                            } else {
                                layer.alert(data.msg);
                            }
                        }, this)).fail($.proxy(function (XMLHttpRequest, textStatus, errorThrown) {
                            layer.alert(errorThrown);
                        }, this));
                    }
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                }
            }).bind('select_node.jstree', function (event, selected) {  //绑定的点击事件
                console.log("select_node.jstree");
//                console.log(selected);
                $("#input_pid").val(selected.node.original.id);
                $("#pmenuename").text(selected.node.text);

                $modal.modal("hide");
            });

        });
    </script>

    </@footer_edit>

</body>

</@html>