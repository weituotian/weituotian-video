<#include "../../common/htmlwrap.ftl">

<@html>

    <@header title="欢迎页面">
    </@header>


<body class="hold-transition skin-blue sidebar-mini">

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>


<!-- Main content -->
<section class="content">

    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">我的粉丝</span>
                    <span class="info-box-number">${member.fans}
                        <small>个</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">我的关注</span>
                    <span class="info-box-number">${member.follows}
                        <small>个</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                <a class="page-action" title="我的视频" href="${member_my_video_url}" data-pageId="${member_my_video_url}">
                    <div class="info-box-content">
                        <span class="info-box-text">我的视频</span>
                        <span class="info-box-number">${videosCount}
                            <small>个</small></span>
                    </div>
                </a>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">视频评论</span>
                    <span class="info-box-number">${commentsCount}
                        <small>个</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>


    </div>
    <!-- /.row -->

</section>
<!-- /.content -->


    <@footer>


    </@footer>

</body>

</@html>