<#include "../../common/htmlwrap.ftl">

<@html>

    <@header title="视频管理首页">

    <!--选项卡右键菜单css-->
    <link rel="stylesheet" href="${common.staticPath}plugins/ContextJS/context.bootstrap.css">

    <style type="text/css">
        html {
            overflow: hidden;
        }
    </style>

    </@header>

<body class="hold-transition skin-blue sidebar-mini fixed">

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>W</b>V</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Weituotian</b>Video</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="${common.staticPath}img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><@shiro.principal property="email"/></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="${common.staticPath}img/user2-160x160.jpg" class="img-circle"
                                     alt="User Image">
                                <p>
                                    <@shiro.principal property="loginname"/>
                                    <small><@shiro.principal property="email"/></small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                        <#--<li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                        </li>-->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">信息</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" onclick="logout();" class="btn btn-default btn-flat">登出</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="${common.staticPath}img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><@shiro.principal property="loginname"/></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" id="input_search" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="button" id="search-btn" class="btn btn-flat" onclick="search_menu();"><i
                        class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!--<li class="header">MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                        <li><a href="../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>Layout Options</span>
                        <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                        <li><a href="layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                        <li><a href="layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                        <li><a href="layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="widgets.html">
                        <i class="fa fa-th"></i> <span>Widgets</span>
                        <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Charts</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                        <li><a href="charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                        <li><a href="charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                        <li><a href="charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>UI Elements</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                        <li><a href="UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                        <li><a href="UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                        <li><a href="UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                        <li><a href="UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                        <li><a href="UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Forms</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                        <li><a href="forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                        <li><a href="forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Tables</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                        <li><a href="tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
                    </ul>
                </li>
                <li>
                    <a href="calendar.html">
                        <i class="fa fa-calendar"></i> <span>Calendar</span>
                        <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
                    </a>
                </li>
                <li>
                    <a href="mailbox/mailbox.html">
                        <i class="fa fa-envelope"></i> <span>Mailbox</span>
                        <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Examples</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                        <li><a href="examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                        <li><a href="examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                        <li><a href="examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                        <li><a href="examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                        <li><a href="examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                        <li><a href="examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                        <li><a href="examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                        <li><a href="examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Multilevel</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Level One
                                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                                <li>
                                    <a href="#"><i class="fa fa-circle-o"></i> Level Two
                                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                    </ul>
                </li>
                <li><a href="../documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
                <li class="header">LABELS</li>
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="content-wrapper" style="min-height: 421px;">
        <!--bootstrap tab风格 多标签页-->
        <div class="content-tabs">
            <button class="roll-nav roll-left tabLeft" onclick="scrollTabLeft()">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs menuTabs tab-ui-menu" id="tab-menu">
                <div class="page-tabs-content" style="margin-left: 0px;">

                </div>
            </nav>
            <button class="roll-nav roll-right tabRight" onclick="scrollTabRight()">
                <i class="fa fa-forward" style="margin-left: 3px;"></i>
            </button>
            <div class="btn-group roll-nav roll-right">
                <button class="dropdown tabClose" data-toggle="dropdown">
                    页签操作<i class="fa fa-caret-down" style="padding-left: 3px;"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" style="min-width: 128px;">
                    <li><a class="tabReload" href="javascript:refreshTab();">刷新当前</a></li>
                    <li><a class="tabCloseCurrent" href="javascript:closeCurrentTab();">关闭当前</a></li>
                    <li><a class="tabCloseAll" href="javascript:closeOtherTabs(true);">全部关闭</a></li>
                    <li><a class="tabCloseOther" href="javascript:closeOtherTabs();">除此之外全部关闭</a></li>
                </ul>
            </div>
            <button class="roll-nav roll-right fullscreen" onclick="App.handleFullScreen()"><i
                    class="fa fa-arrows-alt"></i></button>
        </div>
        <div class="content-iframe " style="background-color: #ffffff; ">
            <div class="tab-content " id="tab-content">

            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.8
        </div>
        <strong>Copyright &copy; 2017-2018 <a href="">韦驮天</a>.</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                <p>New phone +1(800)555-1234</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                <p>nora@example.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                <p>Execution time 5 seconds</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Resume
                                <span class="label label-success pull-right">95%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Laravel Integration
                                <span class="label label-warning pull-right">50%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Back End Framework
                                <span class="label label-primary pull-right">68%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Allow mail redirect
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Expose author name in posts
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Show me as online
                            <input type="checkbox" class="pull-right" checked>
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Turn off notifications
                            <input type="checkbox" class="pull-right">
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Delete chat history
                            <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>

</body>

    <@footer>

    <!-- Slimscroll -->
    <script src="${common.staticPath}plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="${common.staticPath}plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="${common.staticPath}js/app.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="${common.staticPath}js/demo.js"></script>
    <!--tabs-->
    <script src="${common.staticPath}js/app_iframe.js"></script>

    <!--右键菜单-->
    <script src="${common.staticPath}plugins/ContextJS/context.js"></script>

    <!--转换成树的支持-->
    <script src="${common.staticPath}plugins/gtree/tree.js"></script>

    <!--初始化菜单和菜单控制-->
    <script type="text/javascript">
        /**
         * 本地搜索菜单
         */
        function search_menu() {
            //要搜索的变量
            var text = $('#input_search').val();

            var $ul = $('.sidebar-menu');
            $ul.find("a.nav-link").each(function () {
                var $a = $(this);
                //判断是否含有要搜索的字符串
                if ($a.children("span.menu-text").text().indexOf(text) >= 0) {

                    //如果a标签的父级是隐藏的就展开
                    $ul = $a.parents("ul");
                    if ($ul.is(":hidden")) {
                        $a.parents("ul").prev().click();
                    }

                    //点击该菜单
                    $a.click();

                    return false;
                }
            });

        }

        $(function () {
//        console.log(window.location);

            App.setbasePath("${common.staticPath}");
            App.setGlobalImgPath("img/");

            addTabs({
                id: '10008',
                title: '欢迎页',
                close: false,
                url: '${welcome_url}',
                urlType: "absolute"
            });

            App.fixIframeCotent();

            /*addTabs({
             id: '10009',
             title: '404',
             close: true,
             url: 'UI/buttons_iframe2.html'
             });*/

            /*
             <li class="treeview">
             <a href="#">
             <i class="fa fa-edit"></i> <span>Forms</span>
             <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
             </span>
             </a>
             <ul class="treeview-menu">
             <li><a href="forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
             <li><a href="forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
             <li><a href="forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
             </ul>
             </li>
             */
            /*var menus = [
                {
                    id: "9000",
                    text: "header",
                    icon: "",
                    isHeader: true
                },
                {
                    id: "9001",
                    text: "UI Elements",
                    icon: "fa fa-laptop",
                    children: [
                        {
                            id: "90011",
                            text: "buttons",
                            icon: "fa fa-circle-o",
                            url: "UI/buttons_iframe.html",
                            targetType: "iframe-tab"
                        },
                        {
                            id: "90012",
                            text: "icons",
                            url: "UI/icons_iframe.html",
                            targetType: "iframe-tab",
                            icon: "fa fa-circle-o"
                        },
                        {
                            id: "90013",
                            text: "general",
                            url: "UI/general_iframe.html",
                            targetType: "iframe-tab",
                            icon: "fa fa-circle-o"
                        }
                    ]
                },
                {
                    id: "9002",
                    text: "Forms",
                    icon: "fa fa-edit",
                    children: [
                        {
                            id: "90021",
                            text: "advanced",
                            url: "forms/advanced_iframe.html",
                            targetType: "iframe-tab",
                            icon: "fa fa-circle-o"
                        },
                        {
                            id: "90022",
                            text: "general",
                            url: "forms/general_iframe.html",
                            targetType: "iframe-tab",
                            icon: "fa fa-circle-o"
                        },
                        {
                            id: "90023",
                            text: "editors",
                            url: "forms/editors_iframe.html",
                            targetType: "iframe-tab",
                            icon: "fa fa-circle-o"
                        }
                    ]
                }
            ];*/
            $.getJSON("${menu_url}", {}, function (result) {
                var data = result.obj;

                //获取失败就提示
                if (!result.success) {
                    layer.msg(data.msg, {
                        icon: 1,
                        time: 2000
                    }, function () {
//                        window.location.reload();
                    });
                }


                for (var i = 0; i < data.length; i++) {
//                    data[i]['text'] = data[i]['name'];
                    data[i]['url'] = data[i]['resourceUrl'];
//                    data[i]['targetType'] = "iframe-tab";
                }

                var menuTree = $.GTree({
                    data: data,
                    options: {
                        root_id_field: "-1",
                        child_field: "children",
                        seq: true//排序
                    }
                });

                //basepath: /video

                var $ul = $('.sidebar-menu');
//                console.log(menuTree);
                $ul.sidebarMenu({data: menuTree});

                //自己实现菜单被点击事件
                $ul.on("click", "li.treeview a.nav-link", function (event) {
                    var $a = $(this);
                    event.preventDefault();
                    if ($a.next().size() == 0) {//如果size>0,就认为它是可以展开的
                        addTabs({
                            id: $a.attr("href"),
                            title: $a.attr("title"),
                            close: true,
                            url: $a.attr("href"),
                            urlType: "absolute"
                        });
                    }

                });

                //修正侧边栏,使菜单可以滑动
                $.AdminLTE.layout.fixSidebar();
            });

        });
    </script>

    <!---->
    <!--选项卡右键菜单-->
    <script type="text/javascript">
        $(document).ready(function () {
            context.init({
                preventDoubleContext: false,//不禁用原始右键菜单
                compress: true//元素更少的padding
            });

            function findTabElement(target) {
                var $ele = $(target);
                if (!$ele.is("a")) {
                    $ele = $ele.parents("a.menu_tab");
                }
                return $ele;
            }

            //选项卡的菜单
            context.attach('.page-tabs-content', [
//            {header: 'Options'},
                {
                    text: '刷新',
                    action: function (e, $selector, rightClickEvent) {
                        //e是点击菜单的事件
                        //$selector就是＄（".page-tabs-content")
                        //rightClickEvent就是右键打开菜单的事件

                        var pageId = getPageId(findTabElement(rightClickEvent.target));
                        refreshTabById(pageId);

                    }
                },
                {
                    text: "在新窗口打开",
                    action: function (e, $selector, rightClickEvent) {

                        var pageId = getPageId(findTabElement(rightClickEvent.target));
                        var url = getTabUrlById(pageId);
                        window.open(url);

                    }
                }
//            {text: 'Open in new Window', href: '#'},
//            {divider: true},
//            {text: 'Copy', href: '#'},
//            {text: 'Dafuq!?', href: '#'}
            ]);
        });
    </script>

    <!--登出-->
    <script type="text/javascript">
        <#--登出-->
        function logout() {
            $.ajax({
                url: "${logout_url}",
                type: "POST",
                datatype: "json",
                data: {}
            }).done(function (data, textStatus) {
                layer.msg(data.msg, {
                    icon: 1,
                    time: 3000 //2秒关闭（如果不配置，默认是3秒）
                }, function () {
                    window.location.reload();
                });
            }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
            });
        }

    </script>
    </@footer>
</@html>