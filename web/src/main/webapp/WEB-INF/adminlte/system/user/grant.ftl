<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">
    <style>
        ul {
            list-style: none;
        }

        /*ul li{
            display: inline;
        }*/
    </style>
    </@header>

<body>

<section class="content">

    <div class="row">
        <form role="form" id="myform" class="form-horizontal">
            <input type="hidden" name="userId" value="${user.id}">
            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">当前编辑用户:${user.loginname}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <ul>
                            <#list roleOptions as option>
                                <li>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="roleIds[]"
                                               value="${option.value}" ${option.selected}>
                                    ${option.name}
                                    </label>
                                </li>
                            </#list>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="parent.close_layer();" class="btn btn-default">关闭</button>
                        <button type="button" id="btn_grant" class="btn btn-default">保存</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    <@footer>
    <script>
        $(document).ready(function () {
            var $form = $("#myform");
            //保存
            $("#btn_grant").click(function () {
                $.ajax({
                    url: "${dogrant_url}",
                    type: "POST",
                    datatype: "json",
                    data: $form.serialize()
                }).done(function (data) {
                    var index = parent.layer.alert(data.msg, function () {
                        if (data.success) {
                            parent.close_layer();
                        }
                        parent.layer.close(index);
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert(XMLHttpRequest + textStatus + errorThrown);
                });
            });

        });

    </script>
    </@footer>

</body>
</@html>