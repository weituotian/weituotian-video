<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="${pageName}">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">
                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <#if action=='edit'>
                            <!--角色id-->
                            <input name="id" value="${user.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>

                            <input type="hidden" name="userType" value="${user.userType.getValue()}">

                        </#if>

                        <!--登录名-->
                        <div class="form-group">
                            <label for="input_loginname" class="col-sm-3 control-label">登录名</label>
                            <div class="col-sm-9">
                                <input name="loginname" value="${user.loginname!""}"
                                       class="form-control" id="input_loginname"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>

                        <!--密码-->
                        <div class="form-group">
                            <label for="input_password" class="col-sm-3 control-label">密码</label>
                            <div class="col-sm-9">
                                <input name="password" value=""
                                       class="form-control" id="input_password"
                                       type="password"
                                       valType="mm"
                                    <#if action=='edit'>
                                       placeholder="不修改为留空"
                                    <#elseif action=="add">
                                       required
                                    </#if>
                                >
                            </div>
                        </div>

                        <!--重复密码-->
                        <div class="form-group">
                            <label for="input_password2" class="col-sm-3 control-label">重复密码</label>
                            <div class="col-sm-9">
                                <input name="password2" value=""
                                       class="form-control" id="input_password2"
                                       type="password"
                                       valType="mm"
                                    <#if action=='edit'>
                                       placeholder="不修改为留空"
                                    <#elseif action=="add">
                                       required
                                    </#if>
                                >
                            </div>
                        </div>

                        <!--邮箱-->
                        <div class="form-group">
                            <label for="input_email" class="col-sm-3 control-label">邮箱</label>
                            <div class="col-sm-9">
                                <input name="email" value="${user.email!""}"
                                       class="form-control" id="input_email"
                                       placeholder=""
                                       type="text"
                                       valType="email"
                                       required>
                            </div>
                        </div>


                        <!--用户类型-->
                    <#--<div class="form-group">

                        <label class="col-sm-3 control-label">用户类型</label>

                        <div class="checkbox-list col-sm-9">
                            <#list userTypes as val>
                            &lt;#&ndash;val代表每一个enum&ndash;&gt;
                                <label class="checkbox-inline">
                                    <input type="radio" name="userType" value="${val.getValue()}"
                                        <#if val.getValue()==user.userType.getValue()>
                                           checked
                                        </#if>
                                    >${val.getTitle()}</label>
                            </#list>
                        </div>
                    </div>-->

                        <!--状态-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">状态</label>
                            <div class="checkbox-list col-sm-9">
                                <#list openStatus as val>
                                <#--val代表每一个enum-->
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${val.getValue()}"
                                            <#if val.getValue()==user.status.getValue()>
                                               checked
                                            </#if>
                                        >${val.getTitle()}</label>
                                </#list>
                            <#--以下是map方式-->
                            <#--<#list openStatus?keys as key>
                                <label class="checkbox-inline">
                                    <input type="radio" name="status" value="${key}"
                                        <#if (user.status.getValue()!1)==key>
                                           checked
                                        </#if>
                                    >
                                ${openStatus?values[key]}
                                </label>
                            </#list>-->
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        <#elseif action=="add">
                            <button type="submit" class="btn btn-info pull-right">新增</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    <@footer_edit>

    <!--表单-->
    <script>
        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                event.preventDefault();
                //验证表单
                if (!$form.Valid()) {
                    return;
                }
                if ($('#input_password').val() != $('#input_password2').val()) {
                    layer.alert("两次输入的密码不相同");
                    return;
                }

                //发送ajax请求
                $.ajax({
                    url: $form.attr("action"),
                    type: "POST",
                    datatype: "json",
                    //timeout: 3000,
                    data: $form.serialize()
                }).done(function (data, textStatus) {
                    layerMsg(data.msg, data.success, function () {
                        if (data.success) {
                            close_iframe_or_refresh();
                        }
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
                });
            });
        });

    </script>

    </@footer_edit>

</body>

</@html>