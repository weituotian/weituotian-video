<#include "../../common/htmlwrap.ftl">

<@html>

    <@header title="${pageName}">

    </@header>

<body>
<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">
                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <#if action=='edit'>
                            <!--角色id-->
                            <input name="id" value="${role.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>
                        </#if>

                        <!--名字-->
                        <div class="form-group">
                            <label for="input_name" class="col-sm-3 control-label">角色名</label>
                            <div class="col-sm-9">
                                <input name="name" value="${role.name!""}"
                                       class="form-control" id="input_name"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>

                        <!--状态-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">状态</label>
                            <div class="checkbox-list col-sm-9">
                                <#list openStatus as val>
                                <#--val代表每一个enum-->
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${val.getValue()}"
                                            <#if val.getValue()==role.status.getValue()>
                                               checked
                                            </#if>
                                        >${val.getTitle()}</label>
                                </#list>
                                <#--<#list openStatus?keys as key>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${key}"
                                            <#if role.status.getValue()==key+"">
                                               checked
                                            </#if>
                                        >
                                    ${openStatus?values[key]}
                                    </label>
                                </#list>-->
                            </div>
                        </div>

                        <!--排序-->
                        <div class="form-group">
                            <label for="input_seq" class="col-sm-3 control-label">排序</label>
                            <div class="col-sm-9">
                                <input name="seq" value="${role.seq!1}"
                                       class="form-control" id="input_seq"
                                       placeholder=""
                                       type="number"
                                       valType="number"
                                       required>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        <#elseif action=="add">
                            <button type="submit" class="btn btn-info pull-right">新增</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    <@footer_edit>

    <!--表单-->
    <script>
        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                event.preventDefault();
                //验证表单
                if (!$form.Valid()) {
                    return;
                }
                //发送ajax请求
                $.ajax({
                    url: $form.attr("action"),
                    type: "POST",
                    datatype: "json",
                    //timeout: 3000,
                    data: $form.serialize()
                }).done(function (data, textStatus) {
                    /*var index = layer.alert(data.msg, function () {
                        if (data.success) {
                            if (!close_iframe()) {
                                window.location.reload(true);
                            }
                        }
                        layer.close(index);
                    });*/
                    layerMsg(data.msg, data.success, function () {
                        if (data.success) {
                            close_iframe_or_refresh();
                        }
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
                });
            });
        });

    </script>

    </@footer_edit>


</body>


</@html>