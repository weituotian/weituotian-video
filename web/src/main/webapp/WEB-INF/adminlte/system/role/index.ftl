<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    <#--jstree style-->
    <link rel="stylesheet" href="${common.staticPath}plugins/jstree/dist/themes/default/style.min.css">

    </@header>
<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="角色名">
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <#--<a class="btn btn-default lr-replace"><i class="fa fa-refresh"></i>&nbsp;刷新</a>-->
            <a class="btn btn-default lr-add page-action" title="新增角色" data-pageId="${add_url}" href="${add_url}"><i
                    class="fa fa-plus"></i>&nbsp;新增</a>
        <#--<a class="btn btn-default lr-edit" onclick="toedit()"><i class="fa fa-pencil-square-o"></i>&nbsp;编辑</a>-->
            <a class="btn btn-default lr-delete" onclick="delete_selection();"><i
                    class="fa fa-remove"></i>&nbsp;删除</a>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-remove"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
        <#--<a class="btn btn-default lr-viewlog"><i class="fa fa-detail"></i>&nbsp;查看任务日志</a>-->
        <#--<a class="btn btn-default lr-start"><i class="fa fa-plus"></i>&nbsp;启动</a>-->
        <#--<a class="btn btn-default lr-stop"><i class="fa fa-trash-o"></i>&nbsp;停止</a>-->
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"
           data-click-to-select="true"
    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true">id</th>
            <th>角色名</th>
            <th>排序</th>
            <th>状态</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.seq}</td>
                <td>
                    <#if "OPEN"==item.status.value>
                        <span class="label label-sm label-success"> 开 </span>
                    <#elseif "CLOSE"==item.status.value >
                        <span class="label label-sm label-warning"> 关 </span>
                    </#if>
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="roleedit_${item.id}"
                           href="${edit_url}?id=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="修改角色${item.id}">
                            修改
                        </a>
                        <a class="btn btn-default"
                           onclick="grant(${item.id});"
                           data-toggle="tooltip" data-placement="bottom" title="为角色${item.id}分配资源">
                            分配资源
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">
    </div>
    </@section_body>

<!-- 分配资源模态框 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">分配资源</h4>
            </div>
            <div class="modal-body">
                <!--选择树-->
                <div id="res_tree">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="open_selected()">打开全部选择的</button>
                <button type="button" class="btn btn-default" onclick="closeAll()">折叠所有节点</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="btn_grant">保存</button>
            </div>
        </div>
    </div>
</div>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <!--jstree-->
    <!--转换成树的支持-->
    <script src="${common.staticPath}plugins/gtree/tree.js"></script>
    <script src="${common.staticPath}plugins/gtree/dimension.tree.js"></script>
    <!--显示js tree-->
    <script src="${common.staticPath}plugins/jstree/dist/jstree.min.js"></script>
    <!--分配资源-->
    <script>

        //moudleNode>>controlNode>>methodNode


        var curRoleId;
        var moudleList = [];//保存一次ajax请求返回的节点树

        function grant(roleId) {
            var $this = $(this);
            if (curRoleId != roleId) {

                curRoleId = roleId;
                $("#myModalLabel").text($this.attr("title"));

                //发起ajax请求
                $.ajax({
                    url: "${grant_url}",
                    type: "POST",
                    datatype: "json",
                    data: {
                        id: roleId
                    }
                }).done(function (data) {

                    if (data.success) {

                        moudleList = [];

                        //ajax完成,处理结果
                        var list = data.obj;


                        //jstree节点
                        /*var treeNode = function (id, text) {
                            this.id = id;
                            this.text = text;
                            this.children = false;
                        };

                        //jstree节点状态
                        var state = function (opened, disabled, selected) {
                            this.opened = opened || false;  // is the node open
                            this.disabled = disabled || false; // is the node disabled
                            this.selected = selected || false;  // is the node selected
                        };

                        //寻找模块节点
                        function findMoudleNode(moudleList, moudleName) {
                            var node;
                            $.each(moudleList, function (n, value) {
                                if (value.text == moudleName) {
                                    node = value;
                                    return false;
                                }
                            });
                            if (!node) {
                                node = new treeNode(moudleName, moudleName);//新建moudle的node
                                node.state = new state(true);
                                moudleList.push(node);

                            }
                            return node;
                        }

                        //寻找控制器节点
                        function findControllerNode(moudleNode, controllerName) {
                            var controlNode;
                            if (!moudleNode.children) {
                                //初始化数组
                                moudleNode.children = [];
                            } else {
                                //遍历寻找
                                $.each(moudleNode.children, function (n, value) {
                                    if (value.text == controllerName) {
                                        controlNode = value;
                                        return false;
                                    }
                                });
                            }
                            if (!controlNode) {
                                controlNode = new treeNode(controllerName, controllerName);//新建control的node
                                controlNode.state = new state();
                                moudleNode.children.push(controlNode);

                            }
                            return controlNode;
                        }

                        //增加方法节点
                        function addMethod(controllerNode, obj) {
                            var methodNode = new treeNode(obj.id, obj.name + "(" + obj.url + ")");
                            methodNode.state = new state(false, false, obj.checked != "");

                            if (!controllerNode.children) {
                                //初始化数组
                                controllerNode.children = [];
                            }
                            controllerNode.children.push(methodNode);
                        }

                        moudleList=[];
                        $.each(list, function (n, value) {
                            var moudleName = value.moudle;
                            var controllerName = value.controller;

                            var moudleNode = findMoudleNode(moudleList, moudleName);
                            var controllerNode = findControllerNode(moudleNode, controllerName);
                            addMethod(controllerNode, value);

                            //                var objList = [];
                            /!*if (!objList[moudleName]) {
                                objList[moudleName] = [];
                            }
                            if (!objList[moudleName][controllerName]) {
                                objList[moudleName][controllerName] = [];
                            }
                            objList[moudleName][controllerName].push(value) ;*!/

                        });
                        console.log(moudleList);*/

                        $.each(list, function (n, value) {
                            var moudleName = value.moudle;
                            var controllerName = value.controller;
                            $.AddJsTreeNode(moudleList, moudleName, controllerName, value);
                        });

                        var $tree = $("#res_tree");
                        //重新创建jstree
                        $tree.jstree('destroy').jstree({
                            'plugins': ["wholerow", "checkbox", "types"],
                            'core': {
                                "themes": {
                                    "responsive": false
                                },
                                'data': moudleList,
                                'expand_selected_onload': false//选择的不自动展开
                            },
                            "types": {
                                "default": {
                                    "icon": "fa fa-folder icon-state-warning icon-lg"
                                },
                                "file": {
                                    "icon": "fa fa-file icon-state-warning icon-lg"
                                }
                            }
                            /*,"checkbox": {
                                "three_state": false,//三态
                                "cascade": "undetermined"//是否级联选择
                            }*/
                        });

                        $('#myModal').modal("show");


                    } else {
                        layer.alert(data.msg);
                    }

                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert(XMLHttpRequest + textStatus + errorThrown);
                });

            } else {

                //不用再次ajax请求
                $('#myModal').modal("show");


            }

        }

        function closeAll() {
            var $tree = $("#res_tree");
            $tree.jstree('close_all');
        }

        function open_selected() {
            var $tree = $("#res_tree");
            var checkedIds = $tree.jstree("get_checked", 'full');//jstree当前选择节点数组
            var parentIds = [];//父节点数组
            $.each(checkedIds, function (index, node) {
                //node.parent代表选择节点的父节点
                if (parentIds.indexOf(node.parent) == -1) {
                    parentIds.push(node.parent);
                }
            });
            $tree.jstree('open_node', parentIds);
        }


        /**
         * 获得jstree现在选择的节点
         */
        /*function getJsTreeIds() {
            //方法1
            var ids = $("#res_tree").jstree("get_checked");
            var filterIds = [];
            $.each(ids, function (n, id) {
                if (!isMoudleOrControllerNode(id)) {
                    filterIds.push(id);
                }
            });
            return filterIds;
        }*/


        function getJsTreeIds2() {
            //方法2
            var ids = $("#res_tree").jstree("get_checked");

            var moudleIdList=[];
            var controlIdList=[];
            $.each(moudleList,function (n, moudleNode) {
                moudleIdList.push(moudleNode.id);
                $.each(moudleNode.children,function (n, controllNode) {
                    controlIdList.push(controllNode.id);
                });
            });

            var filterIds = [];
            $.each(ids, function (n, id) {
                if (moudleIdList.indexOf(id) == -1 && controlIdList.indexOf(id) == -1) {
                    filterIds.push(id);
                }
            });
            return filterIds;
        }


        $(document).ready(function () {

            //保存分配资源
            $("#btn_grant").click(function () {
                var ids = getJsTreeIds2();
                $.ajax({
                    url: "${dogrant_url}",
                    type: "POST",
                    datatype: "json",
                    data: {
                        roleId: curRoleId,
                        resIds: ids
                    }
                }).done(function (data) {
                    var index = layer.alert(data.msg, function () {
                        if (data.success) {
                            $('#myModal').modal("hide");
                            curRoleId = -1;//下次打开会重新加载数据
                        }
                        layer.close(index);
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert(XMLHttpRequest + textStatus + errorThrown);
                });
            });

        });
    </script>

    </@footer_list>

</body>

</@html>