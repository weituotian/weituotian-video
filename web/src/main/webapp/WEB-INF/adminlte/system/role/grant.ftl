<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="角色分配资源">

    </@header>

<body>

<section class="content">
    <div id="res_tree">

    </div>
</section>


    <@footer>
    <!--转换成树的支持-->
    <script src="${common.staticPath}plugins/gtree/tree.js"></script>
    <!--显示js tree-->
    <script src="${common.staticPath}plugins/jstree/dist/jstree.min.js"></script>
    <script type="text/javascript">
        var json = '${list}';
        var list = $.parseJSON(json);

        //name字段的值复制到text字段中
        for (var i = 0; i < list.length; i++) {
            list[i]['text'] = list[i]['name'];
        }

        //转化为jstree识别的json格式
        var menuTree = $.GTree({
            data: list,
            options: {
                child_field: "children"
            }
        });

        $('#res_tree').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes": {
                    "responsive": false
                },
                'data': menuTree
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    </script>

    </@footer>

</body>


</@html>