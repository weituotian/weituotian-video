<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="名称">
        </div>
        <div class="form-group">
            <select class="form-control" name="search_moudle">
                <option value="">选择模块</option>
                <#list moudles as moudle>
                    <option value="${moudle}"
                        <#if ((pageinfo.condition['moudle'])!"")==moudle>
                            selected
                        </#if>
                    >${moudle}</option>
                </#list>
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" name="search_controller">
                <option value="">选择控制器</option>
                <#list controllers as controller>
                    <option value="${controller}"
                        <#if ((pageinfo.condition['controller'])!"")==controller>
                            selected
                        </#if>
                    >${controller}</option>
                </#list>
            </select>
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <#--<a class="btn btn-default lr-replace"><i class="fa fa-refresh"></i>&nbsp;刷新</a>-->
            <a class="btn btn-default lr-add page-action" title="新增资源" data-pageId="${add_url}" href="${add_url}"><i
                    class="fa fa-plus"></i>&nbsp;新增</a>
        <#--<a class="btn btn-default lr-edit" onclick="toedit()"><i class="fa fa-pencil-square-o"></i>&nbsp;编辑</a>-->
            <a class="btn btn-default lr-delete" onclick="delete_selection();"><i
                    class="fa fa-remove"></i>&nbsp;删除</a>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-remove"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
            <a class="btn btn-default lr-viewlog" onclick="autoCreate()"><i class="fa fa-detail"></i>&nbsp;一键创建增删改查</a>
        <#--<a class="btn btn-default lr-start"><i class="fa fa-plus"></i>&nbsp;启动</a>-->
        <#--<a class="btn btn-default lr-stop"><i class="fa fa-trash-o"></i>&nbsp;停止</a>-->
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true">id</th>
            <th>名称</th>
            <th>模块</th>
            <th>控制器</th>
            <th>url</th>
            <th>状态</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.moudle}</td>
                <td>${item.controller}</td>
                <td>${item.url}</td>
                <td>
                    <#if "OPEN"==item.status.value>
                        <span class="label label-sm label-success"> 开 </span>
                    </#if>
                    <#if "CLOSE"==item.status.value>
                        <span class="label label-sm label-warning"> 关 </span>
                    </#if>
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="resourcetypeedit_${item.id}"
                           href="${edit_url}?id=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="修改资源${item.id}">
                            修改
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>

<!-- 一键创建模态框 -->
<div class="modal fade" id="autoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${autocreate_url}" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">一键创建</h4>
                </div>
                <div class="modal-body">
                    <!--模块名称-->
                    <div class="form-group">
                        <label for="input_moudleName" class="col-sm-3 control-label">模块名称</label>
                        <div class="col-sm-9">
                            <input name="moudleName"
                                   class="form-control" id="input_moudleName"
                                   placeholder=""
                                   type="text"
                                   valType=""
                                   required>
                            <select class="form-control" id="auto_select_moudle">
                                <#list moudles as moudle>
                                    <option value="${moudle}">${moudle}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <!--控制器url-->
                    <div class="form-group">
                        <label for="input_controllerName" class="col-sm-3 control-label">控制器名称</label>
                        <div class="col-sm-9">
                            <input name="controllerName"
                                   class="form-control" id="input_controllerName"
                                   placeholder=""
                                   type="text"
                                   valType=""
                                   required>
                            <select class="form-control" id="auto_select_controller">
                                <#list controllers as controller>
                                    <option value="${controller}">${controller}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <!--控制器名称-->
                    <div class="form-group">
                        <label for="input_controller" class="col-sm-3 control-label">控制器url</label>
                        <div class="col-sm-9">
                            <input name="controller"
                                   class="form-control" id="input_controller"
                                   placeholder="/xx/list"
                                   type="text"
                                   valType=""
                                   required>
                        </div>
                    </div>

                    <!--名称前缀-->
                    <div class="form-group">
                        <label for="input_descript" class="col-sm-3 control-label">名称前缀</label>
                        <div class="col-sm-9">
                            <input name="descript"
                                   class="form-control" id="input_descript"
                                   placeholder="xx列表,xx新建"
                                   type="text"
                                   valType=""
                                   required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-primary" id="btn_grant">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <script type="text/javascript">
        function autoCreate() {
            $("#autoModal").modal("show");
        }

        $(document).ready(function () {

            //一键创建资源对话框的首页
            $("#auto_select_moudle").click(function () {
                var $this = $(this);
                $("#input_moudleName").val($this.val());
            });

            $("#auto_select_controller").change(function () {
                var $this = $(this);
                $("#input_controllerName").val($this.val());
            });

        });
    </script>
    </@footer_list>

</body>

</@html>