<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="${pageName}">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">
                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <#if action=='edit'>
                            <!--角色id-->
                            <input name="id" value="${resource.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>
                        </#if>

                        <!--名称-->
                        <div class="form-group">
                            <label for="input_name" class="col-sm-3 control-label">名称</label>
                            <div class="col-sm-9">
                                <input name="name" value="${resource.name!""}"
                                       class="form-control" id="input_name"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>

                        <!--模块-->
                        <div class="form-group">
                            <label for="input_moudle" class="col-sm-3 control-label">模块</label>
                            <div class="col-sm-6">
                                <input name="moudle" value="${resource.moudle!""}"
                                       class="form-control" id="input_moudle"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="auto_select_moudle">
                                    <#list moudles as moudle>
                                        <option value="${moudle}">${moudle}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>

                        <!--控制器-->
                        <div class="form-group">
                            <label for="input_controller" class="col-sm-3 control-label">控制器</label>
                            <div class="col-sm-6">
                                <input name="controller" value="${resource.controller!""}"
                                       class="form-control" id="input_controller"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="auto_select_controller">
                                    <#list controllers as controller>
                                        <option value="${controller}">${controller}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>

                        <!--方法-->
                        <div class="form-group">
                            <label for="input_method" class="col-sm-3 control-label">方法</label>
                            <div class="col-sm-9">
                                <input name="method" value="${resource.method!""}"
                                       class="form-control" id="input_method"
                                       placeholder="选填"
                                       type="text"
                                       valType=""
                                >
                            </div>
                        </div>

                        <!--url-->
                        <div class="form-group">
                            <label for="input_method" class="col-sm-3 control-label">url</label>
                            <div class="col-sm-9">
                                <input name="url" value="${resource.url!""}"
                                       class="form-control" id="input_method"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>

                        <!--状态-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">状态</label>
                            <div class="checkbox-list col-sm-9">
                                <#list openStatus as val>
                                <#--val代表每一个enum-->
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${val.getValue()}"
                                            <#if val.getValue()==resource.status.value>
                                               checked
                                            </#if>
                                        >${val.getTitle()}</label>
                                </#list>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        <#elseif action=="add">
                            <button type="submit" class="btn btn-info pull-right">新增</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    <@footer_edit>

    <!--表单-->
    <script>
        $(document).ready(function () {
            //一键创建资源对话框的首页
            $("#auto_select_moudle").click(function () {
                var $this = $(this);
                $("#input_moudle").val($this.val());
            });

            $("#auto_select_controller").change(function () {
                var $this = $(this);
                $("#input_controller").val($this.val());
            });
        });

        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                event.preventDefault();
                //验证表单
                if (!$form.Valid()) {
                    return;
                }

                //发送ajax请求
                $.ajax({
                    url: $form.attr("action"),
                    type: "POST",
                    datatype: "json",
                    //timeout: 3000,
                    data: $form.serialize()
                }).done(function (data, textStatus) {
                    layerMsg(data.msg, data.success, function () {
                        if (data.success) {
                            close_iframe_or_refresh();
                        }
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
                });
            });
        });

    </script>

    </@footer_edit>

</body>

</@html>