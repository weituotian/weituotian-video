<#include "../common/htmlwrap.ftl">

<@html>

    <@header title="404">

    </@header>

<body class="skin-blue">

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        404 Error Page
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">404 error</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

            <p>
                找不到你想要的页面啦
                这个时候, 你可以 <a href="">返回首页</a> or try using the search form.
            </p>

            <#--<form class="search-form">-->
                <#--<div class="input-group">-->
                    <#--<input type="text" name="search" class="form-control" placeholder="Search">-->

                    <#--<div class="input-group-btn">-->
                        <#--<button type="submit" name="submit" class="btn btn-warning btn-flat"><i-->
                                <#--class="fa fa-search"></i>-->
                        <#--</button>-->
                    <#--</div>-->
                <#--</div>-->
                <#--<!-- /.input-group &ndash;&gt;-->
            <#--</form>-->
        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</section>
<!-- /.content -->

    <@footer>

    </@footer>
</body>

</@html>
