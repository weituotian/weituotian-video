<#assign basePath=springMacroRequestContext.contextPath>

<#--视频封面-->
<#assign coverPath=springMacroRequestContext.contextPath+memberCover+"/">
<#--用户头像-->
<#assign avatarPath=springMacroRequestContext.contextPath+memberAvatar+"/">

<#assign staticPath=springMacroRequestContext.contextPath+"/static/adminlte/">

<#assign login_url=basePath+"/login">
<#assign logout_url=basePath+"/logout">
<#assign reg_url=basePath+memberPath+"/reg/index">
<#assign admin_url=basePath+adminPath+"/index">
