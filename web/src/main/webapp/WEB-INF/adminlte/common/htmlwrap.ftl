<#macro html>
<!DOCTYPE html>
<html>
    <#nested/>
</html>
</#macro>

<#macro header title="韦驮天视频">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>${title}</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<#--<meta content="width=device-width, initial-scale=1" name="viewport"/>-->
    <meta content="韦驮天视频" name="description"/>
    <meta content="韦驮天" name="author"/>

    <!--pace-->
    <link rel="stylesheet" href="${common.staticPath}plugins/pace/pace-theme-flash.css">
    <script type="text/javascript" src="${common.staticPath}plugins/pace/pace.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="${common.staticPath}bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="${common.staticPath}css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="${common.staticPath}css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="${common.staticPath}css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="${common.staticPath}css/skins/_all-skins.min.css">
    <!--bootstrap table-->
    <link rel="stylesheet" href="${common.staticPath}plugins/bootstrap-table/bootstrap-table.min.css">

    <!--统一中文字体-->
    <style>
        body {
            font-family: "Microsoft YaHei", Arial, Helvetica, sans-serif, "宋体";
        }
    </style>

    <!--http://aimodu.org:7777/admin/index_iframe.html?q=audio&search=#-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="${common.staticPath}plugins/ie9/html5shiv.min.js"></script>
    <script src="${common.staticPath}plugins/ie9/respond.min.js"></script>
    <![endif]-->

    <#nested/>
</head>
</#macro>

<#--脚部-->
<#macro footer>

<!-- jQuery 2.2.3 -->
<script src="${common.staticPath}plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${common.staticPath}bootstrap/js/bootstrap.min.js"></script>

<!--layer-->
<script src="${common.staticPath}plugins/layer/layer.js"></script>

<!--子页面操作父的tab-->
<script src="${common.staticPath}js/pages/pages-common.js"></script>

    <#nested/>

</#macro>

<#--列表页面的脚部-->
<#macro footer_list>

    <@footer>

    <!--用户body换肤功能-->
    <script src="${common.staticPath}js/pageSkin.js"></script>

    </@footer>

<!--bootstrap table-->
<script src="${common.staticPath}plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${common.staticPath}plugins/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>
<script src="${common.staticPath}plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

    <#nested/>

<!--Bootstrap tooltip 暂时不启用-->
<!--<script type="text/javascript">
    $(document).ready(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>-->

</#macro>

<#--编辑页面的脚部-->
<#macro footer_edit>
    <@footer>

    <!--用户body换肤功能-->
    <script src="${common.staticPath}js/pageSkin.js"></script>

    </@footer>

<!--日期-->
<script src="${common.staticPath}plugins/laydate/laydate.js"></script>
<!--验证-->
<link rel="stylesheet" href="${common.staticPath}plugins/validate/validatestyle3.css">
<script src="${common.staticPath}plugins/validate/validator3+.js"></script>
<!-- iCheck -->
<link rel="stylesheet" href="${common.staticPath}plugins/iCheck/square/blue.css">
<script src="${common.staticPath}plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

    <#nested/>
</#macro>

<#--位置-->
<#macro content_header title='页面'>
<section class="content-header">
    <h1>
    ${title}
    <#--<small>Version 2.0</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void (0);" onclick="top.activeTabByPageId('10008');"><i class="fa fa-dashboard"></i> 首页</a>
        </li>
        <li class="active">${title}</li>
    </ol>

</section>
</#macro>

<#--很多页面用到的,外面套个section,box标签-->
<#macro section_body>
<section class="content">
    <div class="row">
        <div class="col-sm-12">

            <div class="box">
                <!--<div class="box-header">
                    <h3 class="box-title">Hover Data Table</h3>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <#nested/>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</section>
</#macro>

<#--bootstrap table的一些常用操作-->
<#macro tableCommon delete_url="">

<script>
    var delete_url = "${delete_url}";
</script>

<!--封装了table的常用操作-->
<script src="${common.staticPath}js/table_common.js"></script>

</#macro>

<#---分页条显示和跳转-->
<#macro pagination startPage=1 totalPages=1>
<script src="${common.staticPath}plugins/pagination/urlParas.js"></script>
<script src="${common.staticPath}plugins/twbs-pagination-master/jquery.twbsPagination.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pagination').twbsPagination({
            hideOnlyOnePage: true,
            totalPages: ${totalPages},
            startPage:${startPage},
            visiblePages: 7,
            first: "首页",
            prev: '上一页',
            next: '下一页',
            last: '尾页',
            initiateStartPageClick: false,
            onPageClick: function (event, page) {
                var curUrl = window.location.href;
                var newUrl = urlParas(curUrl).set({
                    "page": page
                });
                console.log(newUrl);
                window.location.href = newUrl;
            }
        });
    });
</script>

</#macro>

<#-- 参数说明：pageNo当前的页码，totalPage总页数， showPages显示的页码个数，callFunName回调方法名（需在js中自己定义）-->
<#macro page pageNo totalPage showPages callFunName>
<div class="page_list clearfix">
    <#if pageNo!=1>
        <a href="javascript:${callFunName+'('+1+')'};" class="top_page">首页</a>
        <a href="javascript:${callFunName+'('+(pageNo-1)+')'};" class="page_prev">上一页</a>
    </#if>
    <#if pageNo-showPages/2 gt 0>
        <#assign start = pageNo-(showPages-1)/2/>
        <#if showPages gt totalPage>
            <#assign start = 1/>
        </#if>
    <#else>
        <#assign start = 1/>
    </#if>
    <#if totalPage gt showPages>
        <#assign end = (start+showPages-1)/>
        <#if end gt totalPage>
            <#assign start = totalPage-showPages+1/>
            <#assign end = totalPage/>
        </#if>
    <#else>
        <#assign end = totalPage/>
    </#if>
    <#assign pages=start..end/>
    <#list pages as page>
        <#if page==pageNo>
            <a href="javascript:${callFunName+'('+page+')'};" class="current">${page}</a>
        <#else>
            <a href="javascript:${callFunName+'('+page+')'};">${page}</a>
        </#if>
    </#list>
    <#if pageNo!=totalPage>
        <a href="javascript:${callFunName+'('+(pageNo+1)+')'};" class="page_next">下一页</a>
        <a href="javascript:${callFunName+'('+totalPage+')'};" class="end_page">尾页</a>
    </#if>
</div>
</#macro>