<#include "common/htmlwrap.ftl">

<@html>

    <@header title="发生了错误">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='500错误'></@content_header>

<!-- Main content -->
<section class="content">

    <div class="error-page">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> 噢! 发生了一些错误 </h3>

            <p>
                ${msg!""},
                We will work on fixing that right away.
                Meanwhile, you may <a href="">回到首页</a>
            </p>

            <!--<form class="search-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>-->
        </div>
    </div>
    <!-- /.error-page -->

</section>
<!-- /.content -->


    <@footer>

    </@footer>
</body>

</@html>

