<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    <style>
        .coverImage {
            height: 90px;
            width: 90px;
            border: 3px solid rgba(255, 255, 255, 0.2);
        }
    </style>

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="会员名称">
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <@shiro.hasPermission name="/manager/member/delete">
            <a class="btn btn-default lr-delete" onclick="delete_selection();">
                <i class="fa fa-remove"></i>&nbsp;删除
            </a>
        </@shiro.hasPermission>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-icon-check"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
            <a class="btn btn-default lr-start" onclick="set_status();"><i class="fa fa-plus"></i>&nbsp;设置状态</a>
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true" data-visible="false">id</th>
            <th data-field="login_name">登录名</th>
            <th data-field="name">昵称</th>
            <th data-field="avatar">头像</th>
            <th data-field="reg_date">注册日期</th>
            <th data-field="sex">性别</th>
            <th data-field="follow">关注</th>
            <th data-field="fans">粉丝</th>
            <th data-field="status">状态</th>
            <th data-field="stateId" data-visible="false">状态id</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.loginname}</td>
                <td>${item.name}</td>
                <td><img src="${common.avatarPath+item.avatar!""}" class="img-responsive coverImage" alt="Image"></td>
                <td>${item.regDate?datetime!""}</td>
                <td>
                <#list sexEnum as val>
                    <#if val.getValue()==(item.sex.getValue())!'UNKNOW'>
                    ${val.getTitle()}
                    </#if>
                </#list>
                </td>
                <td>${item.follows}</td>
                <td>${item.fans}</td>
                <td>
                    <#if item.status.getValue()=="OPEN">
                    <span class="label label-sm label-success">
                    <#elseif item.status.getValue()=="CLOSE" >
                    <span class="label label-sm label-warning">
                    </#if>
                    ${item.status.getTitle()} </span>
                </td>
                <td>${item.status.getValue()}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="memberedit_${item.id}"
                           href="${edit_url}?memberId=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="编辑会员${item.id}">
                            编辑会员
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">设置状态</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <#list openState as val>
                            <#--val代表每一个enum-->
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="open" value="${val.getValue()}">${val.getTitle()}
                                    </label>
                                </div>
                            </#list>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="status_submit();">提交</button>
            </div>
        </div>
    </div>
</div>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <script type="text/javascript">
        var check_url = '${check_url}';
    </script>

    <script type="text/javascript">

        //提交视频
        function status_submit() {
            if (submitRow != null) {

                //ajax请求
                $.ajax({
                    url: check_url,
                    type: "POST",
                    datatype: "json",
                    // timeout: 3000,
                    data: {
                        memberId: submitRow.id,
                        status: $('input[name=open]:checked').val()
                    },
                    success: function (data, textStatus) {
                        var layer_index = layer.alert(data.msg, function () {
                            if (data.success) {
                                window.location.reload(true);
                            }
                            layer.close(layer_index);
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(XMLHttpRequest + textStatus + errorThrown);
                    }
                });
            }

        }

        var submitRow;

        //打开设置状态模态框
        function set_status() {
            var $table = $('#table');

            var $list = $table.bootstrapTable("getSelections");

            if ($list.length > 0) {

                //选择第一个
                var selectedRow = $list[0];

                submitRow = selectedRow;

                var $modal = $("#myModal");
                var $title = $modal.find("modal-title");
                var title = "设置会员(id:" + selectedRow.id + ",昵称:" + selectedRow.name + ")的状态为";
                $title.text(title);

                $('input[name=open]').each(function () {
                    var $radio = $(this);
                    $radio.prop("checked", false);
                    /*if ($.trim($radio.parent().text()) == selectedRow.status) {
                        $radio.prop("checked", true);
                    }*/
                    if ($radio.val() == selectedRow.stateId) {
                        $radio.prop("checked", true);
                    }
                });

                $modal.modal("show");

            } else {
                layer.alert("请选择一条数据");
            }

        }

        $(document).ready(function () {

        });
    </script>

    </@footer_list>

</body>

</@html>