<#include "../../common/htmlwrap.ftl">

<@html>

    <@header title="视频查看">

    <!-- Add any other renderers you need; see Use Renderers for more information -->
    <link rel="stylesheet" href="${common.staticPath}plugins/mediaelement/mediaelementplayer.min.css"/>

    </@header>
<body>
    <@section_body>

    <video id="myvideo" class=""
           preload="auto"
           controls
           style="width: 100%;height: 100%;">
        <source src="${videoSrc}" type="video/mp4"/>
        <p class="vjs-no-js">观看视频需要启用JavaScript并且升级到支持html5的浏览器</p>
    </video>

    </@section_body>
    <@footer>

    <script src="${common.staticPath}plugins/mediaelement/mediaelement-and-player.min.js"></script>
    <script src="${common.staticPath}plugins/mediaelement/lang/zh-cn.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            mejs.i18n.language('zh-CN'); // Setting German language

            $('#myvideo').mediaelementplayer({
                enableAutosize: true,
                features: ['playpause', 'progress', 'current', 'volume', 'fullscreen'],
                success: function (mediaElement, originalNode) {
                    // do things
                    var playerId = $('#myvideo').closest('.mejs__container').attr('id');
                    var player = mejs.players[playerId];

                    //全屏
                    $(player.fullscreenBtn).click();
                }
            });

            /*
            // To access player after its creation through jQuery use:
            var playerId = $('#myvideo').closest('.mejs__container').attr('id');
            // or $('#mediaplayer').closest('.mejs-container').attr('id') in "legacy" stylesheet

            var player = mejs.players[playerId];

            // With iOS (iPhone), since it defaults always to QuickTime, you access the player directly;
            // i.e., if you wanna exit fullscreen on iPhone using the player, use this:
            var player = $('#mediaplayer')[0];
            player.webkitExitFullScreen();
            */

        });
    </script>

    </@footer>

</body>
</@html>