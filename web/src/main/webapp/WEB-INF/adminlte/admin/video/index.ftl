<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    <style>
        .coverImage {
            height: 90px;
            width: 90px;
            border: 3px solid rgba(255, 255, 255, 0.2);
        }
    </style>

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="视频名称">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="search_member" id="search_member"
                   value="${(pageinfo.condition['member'])!""}"
                   placeholder="上传者名称">
        </div>
        <div class="form-group">
            <select name="search_partition" class="form-control">
                <option value="">选择分区</option>
                <#list partitions as each>
                    <option value="${each.id}"
                        <#if ((pageinfo.condition['partition'])!"")==each.id+"">
                            selected
                        </#if>
                    >${each.name}</option>
                </#list>
            </select>
        </div>
        <div class="form-group">
            <select name="search_status" class="form-control">
                <option value="">选择视频状态</option>
                <#list videoState as val>
                <#--val代表每一个enum-->
                    <option value="${val.getValue()}"
                        <#if val.getValue()==((pageinfo.condition['status'])!"")>
                            selected
                        </#if>
                    >${val.getTitle()}</option>
                </#list>
            </select>
        </div>
    <#--<div class="form-group">
        <select name="search_open" class="form-control">
            <option value="">选择开启状态</option>
            <#list openStatus as val>
            &lt;#&ndash;val代表每一个enum&ndash;&gt;
                <option value="${val.getValue()}"
                    <#if val.getValue()==((pageinfo.condition['open'])!-1)>
                        selected
                    </#if>
                >${val.getTitle()}</option>
            </#list>
        </select>
    </div>-->
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <#--<a class="btn btn-default lr-replace"><i class="fa fa-refresh"></i>&nbsp;刷新</a>-->
        <#--<a class="btn btn-default lr-add page-action" title="新增资源" data-pageId="${add_url}" href="${add_url}"><i
                class="fa fa-plus"></i>&nbsp;新增</a>-->
        <#--<a class="btn btn-default lr-edit" onclick="toedit()"><i class="fa fa-pencil-square-o"></i>&nbsp;编辑</a>-->
            <@shiro.hasPermission name="/manager/video/delete">
                <a class="btn btn-default lr-delete" onclick="delete_selection();">
                    <i class="fa fa-remove"></i>&nbsp;删除
                </a>
            </@shiro.hasPermission>

            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-icon-check"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
            <a class="btn btn-default lr-start" onclick="set_status();"><i class="fa fa-plus"></i>&nbsp;设置状态</a>
        <#--<a class="btn btn-default lr-stop"><i class="fa fa-trash-o"></i>&nbsp;停止</a>-->
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true" data-visible="false">id</th>
            <th data-field="title">标题</th>
            <th data-field="cover">封面</th>
            <th data-field="uploader">上传者</th>
            <th data-field="partition">分区</th>
            <th data-field="time">时间</th>
            <th data-field="status">视频状态</th>
            <th data-field="stateId" data-visible="false">视频状态id</th>
            <th data-field="hot">热度</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td><img src="${common.coverPath+item.cover!""}" class="img-responsive coverImage" alt="Image"></td>
                <td>${item.memberName}</td>
                <td>${item.partitionName}</td>
                <td>${item.createTime?datetime}</td>
                <td>
                    <!--视频状态-->
                    <#if item.videoState.getValue()=="AuditFailure">
                        <span class="label label-sm label-warning"> ${item.videoState.getTitle()} </span>

                    <#elseif item.videoState.getValue()=="Auditing" || item.videoState.getValue()=="ReEdit">
                        <span class="label label-sm label-info"> ${item.videoState.getTitle()} </span>

                    <#else >
                        <span class="label label-sm label-success"> ${item.videoState.getTitle()} </span>

                    </#if>
                </td>
                <td>${item.videoState.getValue()}</td><!--这一列隐藏-->
                <td><i class="fa fa-play-circle">${item.play!0}<br><i class="fa fa-star">${item.collect!0}</td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-info"
                                onclick="openVideo(${item.id})"
                                data-toggle="tooltip" data-placement="bottom" title="查看视频${item.id}">
                            查看视频
                        </button>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">设置状态</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <#list videoState as val>
                            <#--val代表每一个enum-->
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="status" value="${val.getValue()}">${val.getTitle()}
                                    </label>
                                </div>
                            </#list>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="status_submit();">提交</button>
            </div>
        </div>
    </div>
</div>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <script type="text/javascript">
        var check_url = '${check_url}';
    </script>

    <script type="text/javascript">

        //提交视频
        function status_submit() {
            if (submitRow != null) {

                //ajax请求
                $.ajax({
                    url: check_url,
                    type: "POST",
                    datatype: "json",
                    // timeout: 3000,
                    data: {
                        videoId: submitRow.id,
                        status: $('input[name=status]:checked').val()
                    },
                    success: function (data, textStatus) {
                        var layer_index = layer.alert(data.msg, function () {
                            if (data.success) {
                                window.location.reload(true);
                            }
                            layer.close(layer_index);
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(XMLHttpRequest + textStatus + errorThrown);
                    }
                });
            }

        }

        var submitRow;

        //打开设置状态模态框
        function set_status() {
            var $table = $('#table');

            var $list = $table.bootstrapTable("getSelections");

            if ($list.length > 0) {

                //选择第一个
                var selectedRow = $list[0];

                submitRow = selectedRow;

                var $modal = $("#myModal");
                var $title = $modal.find("modal-title");
                var title = "设置视频(id:" + selectedRow.id + ",标题:" + selectedRow.title + ")的状态为";
                $title.text(title);

                $('input[name=status]').each(function () {
                    var $radio = $(this);
                    $radio.prop("checked", false);
                    /*if ($.trim($radio.parent().text()) == selectedRow.status) {
                        $radio.prop("checked", true);
                    }*/
                    if ($radio.val() == selectedRow.stateId) {
                        $radio.prop("checked", true);
                    }
                });

                $modal.modal("show");

            } else {
                layer.alert("请选择一条或者多条数据");
            }

        }

        //打开视频播放窗口
        function openVideo(videoId) {
            var $this = $(this);
            var layer_index = layer.open({
                type: 2,
                title: $this.attr("title"),
                shadeClose: true,
                shade: false,
                scrollbar: false,
                maxmin: true, //开启最大化最小化按钮
//                area: ["640px", "360px"],
                area: ['80%', "60%"],
                content: '${view_url}?videoId=' + videoId
                /*success:function (layero, index) {
                    layer.iframeAuto(index);
                }*/
            })
        }

        $(document).ready(function () {

        });
    </script>

    </@footer_list>

</body>

</@html>