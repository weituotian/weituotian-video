<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    <style>
        .coverImage {
            width: 54px;
            border: 3px solid rgba(255, 255, 255, 0.2);
        }
    </style>

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="评论">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="search_member" id="search_member"
                   value="${(pageinfo.condition['member'])!""}"
                   placeholder="上传者名称">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="search_video" id="search_video"
                   value="${(pageinfo.condition['video'])!""}"
                   placeholder="视频名称">
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
            <@shiro.hasPermission name="/manager/comment/delete">
                <a class="btn btn-default lr-delete" onclick="delete_selection();">
                    <i class="fa fa-remove"></i>&nbsp;删除
                </a>
            </@shiro.hasPermission>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-icon-check"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true" data-visible="false">id</th>
            <th data-field="user">评论者</th>
            <th data-field="content">内容</th>
            <th data-field="time">时间</th>
            <th data-field="video">视频</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>
                    <a class="page-action" href="${member_edit_url}?memberId=${item.userId}" title="编辑用户${item.userId}">
                        <img src="${common.avatarPath+item.userAvatar!""}" class="img-responsive coverImage" alt="Image">${item.userName}</td>
                </a>
                <td>${item.content}</td>
                <td>${item.postTime?datetime}</td>
                <td>
                    <a class="page-action" href="${video_index_url}?search_name=${item.videoName}" title="查看视频${item.userId}">
                    <img src="${common.coverPath+item.videoCover!""}" class="img-responsive coverImage" alt="Image" >
                    ${item.videoName}
                    </a>
                    </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="editComment_${item.id}"
                           href="${edit_url}?id=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="编辑评论${item.id}">
                            编辑评论
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">设置状态</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <#list videoState as val>
                            <#--val代表每一个enum-->
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="status" value="${val.getValue()}">${val.getTitle()}
                                    </label>
                                </div>
                            </#list>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="status_submit();">提交</button>
            </div>
        </div>
    </div>
</div>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>


    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>

    </@footer_list>

</body>

</@html>