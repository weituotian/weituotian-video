<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="${pageName}">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">

                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->

                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">

                        <#if action=='edit'>
                            <!--评论id-->
                            <input name="cid" value="${comment.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>
                        </#if>

                        <!--内容-->
                        <div class="form-group">
                            <label for="input_content" class="col-sm-3 control-label">内容</label>
                            <div class="col-sm-9">
                                <textarea name="content" class="form-control">${comment.content!""}</textarea>
                                <#--<input name="content" value="${comment.content!""}"
                                       class="form-control" id="input_content"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>-->
                            </div>
                        </div>

                        <!--时间-->
                        <div class="form-group">
                            <label for="input_posttime" class="col-sm-3 control-label">时间</label>
                            <div class="col-sm-9">
                                <input name="time" value="${(comment.postTime?datetime)!""}"
                                       id="input_posttime"
                                       class="form-control"
                                       type="text"
                                       valType="date"
                                       onclick="laydate({elem: '#input_posttime',format: 'YYYY-MM-DD hh:mm:ss'})"
                                >
                            </div>
                        </div>

                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    <@footer_edit>

    <!--表单-->
    <script>

        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                event.preventDefault();
                //验证表单
                if (!$form.Valid()) {
                    return;
                }

                //发送ajax请求
                $.ajax({
                    url: $form.attr("action"),
                    type: "POST",
                    datatype: "json",
                    //timeout: 3000,
                    data: $form.serialize()
                }).done(function (data, textStatus) {
                    layerMsg(data.msg, data.success, function () {
                        if (data.success) {
                            close_iframe_or_refresh();
                        }
                    });
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ajax error", XMLHttpRequest, textStatus, errorThrown);
                });
            });
        });

    </script>

    </@footer_edit>

</body>

</@html>