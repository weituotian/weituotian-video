<#include "common/htmlwrap.ftl">

<@html>

<#--<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>韦驮天视频 | 登录</title>
    <!-- Tell the browser to be responsive to screen width &ndash;&gt;
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 &ndash;&gt;
    <link rel="stylesheet" href="${common.staticPath}bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome &ndash;&gt;
    <link rel="stylesheet" href="${common.staticPath}css/font-awesome.min.css">
    <!-- Ionicons &ndash;&gt;
    <link rel="stylesheet" href="${common.staticPath}css/ionicons.min.css">
    <!-- Theme style &ndash;&gt;
    <link rel="stylesheet" href="${common.staticPath}css/AdminLTE.min.css">
    <!-- iCheck &ndash;&gt;
    <link rel="stylesheet" href="${common.staticPath}plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries &ndash;&gt;
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// &ndash;&gt;
    <!--[if lt IE 9]>
    <script src="${common.staticPath}plugins/ie9/html5shiv.min.js"></script>
    <script src="${common.staticPath}plugins/ie9/respond.min.js"></script>
    <![endif]&ndash;&gt;
</head>-->

    <@header title="登录">
    <link rel="stylesheet" href="${common.staticPath}plugins/iCheck/square/blue.css">
    </@header>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>韦驮天</b>视频</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">登录并开始使用</p>

        <form id="login_form" action="../../index2.html" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="用户名">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="密码">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" value="true"> 记住我
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="btn-login" class="btn btn-primary btn-block btn-flat">登录</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <!--<div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                Google+</a>
        </div>-->
        <!-- /.social-auth-links -->

        <a href="#">忘记密码</a><br>
        <a href="${common.reg_url}" class="text-center">注册</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="${common.staticPath}plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${common.staticPath}bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${common.staticPath}plugins/iCheck/icheck.min.js"></script>

<script src="${common.staticPath}plugins/layer/layer.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        function submitForm() {
            //使用HTML表单来初始化一个FormData对象
            //var data = new FormData($('#myform').get(0));

            var load_index = layer.msg("登录中...", {icon: 3, time: 0});

            $.ajax({
                url: "${common.basePath}/login",
                type: "POST",
                datatype: "json",
                data: $("#login_form").serialize(),
//                processData: false,  // 告诉jQuery不要去处理发送的数据
//                contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                success: function (data, textStatus) {

                    layer.close(load_index);

                    if (data.success) {
                        window.location.href = data.obj;
                    } else {
//                        layer.msg("asd");
                        layer.alert(data.msg, {icon: 3, title: '提示'});
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest + textStatus + errorThrown);
                }
            });
        }

        $('#btn-login').click(function (event) {
            submitForm();
            return false;
        });

        if (self != top) {//top.window.location.href!=window.location.href
            top.window.location.href = window.location.href;
        }
    })

</script>

<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            })
        }, 500)
    });
</script>

</body>

</@html>