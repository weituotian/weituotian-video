<#include "../../common/htmlwrap.ftl">
<@html>

    <@header title="会员注册">

    </@header>


<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <a href=""><b>韦驮天</b>视频</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">新会员注册</p>

        <form id="reg-form" action="${doreg_url}" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="loginName" class="form-control" placeholder="登录名">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="name" class="form-control" placeholder="昵称">
                <span class="glyphicon glyphicon-text-width form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="邮箱">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="密码">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="重复密码">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> 我同意 <a href="#">注册条款</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id="btn-reg" class="btn btn-primary btn-block btn-flat">注册</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <!--<div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
                Google+</a>
        </div>-->

        <a href="${common.login_url}" class="text-center">已有账户,去登录</a>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="${common.staticPath}plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${common.staticPath}bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${common.staticPath}plugins/iCheck/icheck.min.js"></script>
<!--layer-->
<script src="${common.staticPath}plugins/layer/layer.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        function submitForm() {
            var $form = $("#reg-form");

            $.ajax({
                url: $form.attr("action"),
                type: "POST",
                datatype: "json",
                data: $form.serialize(),
                success: function (data, textStatus) {

                    if (data.success) {
                        window.location.href = data.obj;
                    } else {
                        layer.alert(data.msg, {icon: 3, title: '提示'});
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest + textStatus + errorThrown);
                }
            });
        }

        $('#btn-reg').click(function (event) {
            submitForm();
            return false;
        });
    })

</script>

<script>
    $(function () {


        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>

</@html>