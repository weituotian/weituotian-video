<#include "../../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    <style>
        .coverImage {
            height: 90px;
            width: 90px;
            border: 3px solid rgba(255, 255, 255, 0.2);
        }
    </style>

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['name'])!""}"
                   placeholder="名称">
        </div>
        <div class="form-group">
            <select name="search_partition" class="form-control">
                <option value="">选择分区</option>
                <#list partitions as each>
                    <option value="${each.id}"
                        <#if ((pageinfo.condition['partition'])!"-1")==each.id+"">
                            selected
                        </#if>
                    >${each.name}</option>
                </#list>
            </select>
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <#--<a class="btn btn-default lr-replace"><i class="fa fa-refresh"></i>&nbsp;刷新</a>-->
        <#--<a class="btn btn-default lr-add page-action" title="新增资源" data-pageId="${add_url}" href="${add_url}"><i
                class="fa fa-plus"></i>&nbsp;新增</a>-->
        <#--<a class="btn btn-default lr-edit" onclick="toedit()"><i class="fa fa-pencil-square-o"></i>&nbsp;编辑</a>-->
            <a class="btn btn-default lr-delete" onclick="delete_selection();"><i
                    class="fa fa-remove"></i>&nbsp;删除</a>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-icon-check"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
        <#--<a class="btn btn-default lr-start"><i class="fa fa-plus"></i>&nbsp;启动</a>-->
        <#--<a class="btn btn-default lr-stop"><i class="fa fa-trash-o"></i>&nbsp;停止</a>-->
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true" data-visible="false">id</th>
            <th data-field="title">标题</th>
            <th>封面</th>
            <th>时间</th>
            <th>分区</th>
            <th>状态</th>
            <th>热度</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td><img src="${common.coverPath+item.cover!""}" class="img-responsive coverImage" alt="Image"></td>
                <td>${item.createTime?datetime}</td>
                <td>${item.partitionName}</td>
                <td>${item.videoState.getTitle()}</td>
                <td><i class="fa fa-play-circle">${item.play!0}<br><i class="fa fa-star">${item.collect!0}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="resourcetypeedit_${item.id}"
                           href="${edit_url}?id=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="编辑视频${item.id}">
                            编辑
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>


    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <script type="text/javascript">

        $(document).ready(function () {

        });
    </script>

    </@footer_list>

</body>

</@html>