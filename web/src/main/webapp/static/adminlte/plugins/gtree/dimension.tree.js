/**
 * Created by ange on 2017/2/5.
 */
(function ($) {

    //jstree节点
    var treeNode = function (id, text) {
        this.id = id;
        this.text = text;
        this.children = false;
    };

    //jstree节点状态
    var state = function (opened, disabled, selected) {
        this.opened = opened || false;  // is the node open
        this.disabled = disabled || false; // is the node disabled
        this.selected = selected || false;  // is the node selected
    };

    //寻找模块节点
    function findMoudleNode(moudleList, moudleName) {
        var node;
        $.each(moudleList, function (n, value) {
            if (value.text == moudleName) {
                node = value;
                return false;
            }
        });
        if (!node) {
            node = new treeNode("moudle_" + moudleName, moudleName);//新建moudle的node
            node.state = new state(true);
            moudleList.push(node);

        }
        return node;
    }

    //寻找控制器节点
    function findControllerNode(moudleNode, controllerName) {
        var controlNode;
        if (!moudleNode.children) {
            //初始化数组
            moudleNode.children = [];
        } else {
            //遍历寻找
            $.each(moudleNode.children, function (n, value) {
                if (value.text == controllerName) {
                    controlNode = value;
                    return false;
                }
            });
        }
        if (!controlNode) {
            controlNode = new treeNode("controller_"+controllerName, controllerName);//新建control的node
            controlNode.state = new state();
            moudleNode.children.push(controlNode);

        }
        return controlNode;
    }

    //增加方法节点
    function addMethod(controllerNode, obj) {
        var methodNode = new treeNode(obj.id, obj.name + "(" + obj.url + ")");
        methodNode.state = new state(false, false, obj.checked != "");

        if (!controllerNode.children) {
            //初始化数组
            controllerNode.children = [];
        }
        controllerNode.children.push(methodNode);
    }

    $.AddJsTreeNode = function (moudleList, moudleName, controllerName, obj) {
        var moudleNode = findMoudleNode(moudleList, moudleName);
        var controllerNode = findControllerNode(moudleNode, controllerName);
        addMethod(controllerNode, obj);
    }

})(jQuery);

function isMoudleOrControllerNode(moudleList, id) {
    var flag = false;
    $.each(moudleList, function (n, moudleNode) {
        if (moudleNode.id == id) {
            //是控制器节点
            flag = true;
            return false;//退出循环
        } else {
            //遍历寻找该moudleNode的children也就是controlNode的list
            $.each(moudleNode.children, function (n, controlNode) {
                if (controlNode.id == id) {
                    flag = true;
                    return false;//退出循环
                }
            });
        }
    });
    return flag;
}

function isMoudleOrControllerNode2(moudleList, id) {

    var moudleIdList = [];
    var controlIdList = [];
    $.each(moudleList, function (n, moudleNode) {
        moudleIdList.push(moudleNode.id);
        $.each(moudleNode.children, function (n, controllNode) {
            controlIdList.push(controllNode.id);
        });
    });

    return moudleIdList.indexOf(id) == -1 || controlIdList.indexOf(id) == -1;
}