<?php
/**
 * Created by PhpStorm.
 * User: 嘉辉
 * Date: 2017-02-23
 * Time: 10:19
 */

$re = array("success" => true, "msg" => "");

//设置上传目录
$path = 'data/uploads/';

if (!is_dir($path)) {//不存在文件夹创建
    mkdir($path, 0777, true);
}

$year = date('Y', time());//文件按年份存储
$path .= $year . '/';

if (!is_dir($path)) {//不存在文件夹创建
    mkdir($path, 0777, true);
}

require_once "FileUpload.php";

$up = new FileUpload();
//设置属性(上传的位置， 大小， 类型， 名是是否要随机生成)
$up->set("path", $path);
$up->set("maxsize", 2000000);
//$up->set("allowtype", array("gif", "png", "jpg", "jpeg"));
$up->set("israndname", true);

//使用对象中的upload方法， 就可以上传文件， 方法需要传一个上传表单的名子 pic, 如果成功返回true, 失败返回false
if ($up->upload("file")) {

    //获取上传后文件名子
    $file_name = $up->getFileName();

    $re['msg'] = "上传成功";
    $re['url'] = $path . $file_name;


} else {

    //获取上传失败以后的错误提示
    $message = $up->getErrorMsg();

    $re['success'] = false;
    $re['msg'] = $message;
}

echo json_encode($re);


