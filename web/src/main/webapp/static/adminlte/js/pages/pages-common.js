function getCurPageId() {
    if (top.getPageId) {
        return top.getPageId(window.frameElement);
    }
    return null;
    /*var $iframe = $(window.frameElement);
     return $iframe.attr("data-id");*/
}

var myPageId = getCurPageId();

/**
 * 关闭当前iframe
 */
function close_iframe() {
    if (top.closeTabByPageId) {
        top.closeTabByPageId(myPageId);
        return true;
    }
    return false;
}

function close_iframe_or_refresh() {
    if (!close_iframe()) {
        window.location.reload(true);
    }
}

/**
 *
 * 方便子页面操作iframe tab
 * 需要jquery
 *
 * Created by ange on 2017/1/25.
 */
(function ($) {

    $(document).ready(function () {

        //所有有page-action的类都可以打开新tab
        $("body").on("click", ".page-action", function (event) {
            var $this = $(this);

            if (top.addTabs) {
                event.preventDefault();
                top.addTabs({
                    id: $this.attr("data-pageId"),
                    title: $this.attr("title") || $this.attr("data-original-title"),//data-original-title兼容bootstrap tip
                    close: true,
                    url: $this.attr("href") || $this.attr("data-href") || "页面",
                    urlType: "absolute"
                });
            }

        });

        //修改当前iframe的标题
        // top.editTabTitle(myPageId, document.title);

    });

})(jQuery);

/**
 * 封装layer.msg函数实现消息提示
 * @param msg
 * @param success
 * @param callback
 */
function layerMsg(msg, success, callback) {
    var icon = 2;
    if (success) {
        icon = 1;
    }
    layer.msg(msg, {
        icon: icon,
        time: 2000 //2秒关闭（如果不配置，默认是3秒）
    }, function () {
        callback && callback();
    });
}



