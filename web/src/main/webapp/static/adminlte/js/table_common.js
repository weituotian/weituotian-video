/**
 * 编辑页面表格的一些公用操作
 * 约定
 * 表格的id为table
 * 引入此js的时候要声明 var delete_url="${delete_url}";
 *
 * Created by ange on 2017/1/23.
 */

/**
 * 全选
 */
function selectAll() {
    var $table = $('#table');
//            $table.bootstrapTable("checkAll");
    $table.find("input[name=btSelectItem]").each(function () {
        var $input = $(this);
        if (!$input.prop("checked")) {
            $input.click();
        }
    });
}

/**
 * 反选
 */
function reverse_selection() {
    var $table = $('#table');
    $table.find("input[name=btSelectItem]").each(function () {
        var $input = $(this);
        $input.click();
    });
}

/**
 * 删除所选
 */
function delete_selection() {
    var $table = $('#table');

    var $list = $table.bootstrapTable("getSelections");

    var ids_arr = [];

    $.each($list, function (n, value) {
        ids_arr.push(value.id);
    });

    if (ids_arr.length > 0) {

        //ajax请求
        $.ajax({
            url: delete_url,
            type: "POST",
            datatype: "json",
            // timeout: 3000,
            data: {
                ids: ids_arr
            },
            success: function (data, textStatus) {
                var layer_index = layer.alert(data.msg, function () {
                    if (data.success) {
                        window.location.reload(true);
                    }
                    layer.close(layer_index);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest + textStatus + errorThrown);
            }
        });

    } else {
        layer.alert("请选择一条或者多条数据");
    }
}

$(function () {
    var $table = $('#table');

    //刷新按钮被点击
    $table.on("refresh.bs.table", function (event, row, $element) {
        window.location.reload(true);
    });

});

function rowStyle2(row, index) {
    var classes = ['success', 'info', 'warning', 'danger'];//'active',
    if (index % 2 === 0) {//&& index / 2 < classes.length)
        return {
            classes: classes[(index / 2) % classes.length]
        };
    }
    return {};
}