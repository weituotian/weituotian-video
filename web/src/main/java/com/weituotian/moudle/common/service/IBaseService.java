package com.weituotian.moudle.common.service;

import com.weituotian.core.jpa.domain.SimpleDtoMapper;
import com.weituotian.core.utils.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * 公共基础服务,包含增删改查
 * Created by ange on 2017/3/10.
 */
public interface IBaseService<T, PK extends Serializable> {
    /**
     * 增加
     *
     * @return 是否成功
     */
    void add(T t);

    /**
     * 更新
     *
     * @param t 持久bean
     * @return 是否成功
     */
    T update(T t);

    /**
     * 通过vo来更新
     * @param vo vo
     * @param id id
     * @param <V> vo类
     * @return
     */
    <V> T updateByDto(V vo, PK id);

    /**
     * 通过id找到该bean
     *
     * @param id id
     * @return bean
     */
    T findById(PK id);

    /**
     * 获得全部
     *
     * @return list
     */
    List<T> findAll();

    /**
     * 根据id删除bean
     *
     * @param id id
     */
    void deleteById(PK id);

    void deleteBatch(PK[] ids);

    /**
     * 根据id数组删除条目
     *
     * @param ids id数组
     */
    void deleteByIds(PK[] ids);
}
