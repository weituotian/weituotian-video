package com.weituotian.moudle.common.service.impl;

import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.utils.DtoUtils;
import com.weituotian.moudle.common.service.IBaseService;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.service.IAdminBaseService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;

/**
 * 基础服务
 * Created by ange on 2017/3/11.
 */
@Transactional
public abstract class BaseService<T, PK extends Serializable> implements IBaseService<T, PK> {

    public abstract IBaseDao<T, PK> getBaseDao();

    /**
     * 增加
     *
     * @param t
     * @return 是否成功
     */
    @Override
    public void add(T t) {
        this.getBaseDao().persist(t);
    }

    /**
     * 更新
     *
     * @param t 持久bean
     * @return 是否成功
     */
    @Override
    public T update(T t) {
        return this.getBaseDao().merge(t);
    }

    /**
     * 通过vo来更新
     *
     * @param vo vo
     * @param id id
     * @return
     */
    @Override
    public <V> T updateByDto(V vo, PK id) {
        IBaseDao<T, PK> baseDao = getBaseDao();

        T entity = baseDao.findOne(id);

        DtoUtils.copyProperties(vo, entity);

        baseDao.merge(entity);

        return entity;
    }

    /**
     * 通过id找到该bean
     *
     * @param id id
     * @return bean
     */
    @Override
    public T findById(PK id) {
        return this.getBaseDao().findOne(id);
    }


    /**
     * 获得全部
     *
     * @return list
     */
    @Override
    public List<T> findAll() {
        return getBaseDao().findAll();
    }

    /**
     * 根据id删除bean
     *
     * @param id id
     */
    @Override
    public void deleteById(PK id) {
        try {
            this.getBaseDao().delete(id);
        } catch (Exception e) {
            throw new ServiceException("删除失败!");
        }
    }

    @Override
    public void deleteBatch(PK[] ids) {
        Assert.notNull(ids);
        try {
            this.getBaseDao().deleteBatch(ids);
        } catch (PersistenceException e) {
            throw new ServiceException("要删除的实体有关联的数据存在,请先删除关联的数据");
        }
    }

    /**
     * 根据id数组删除条目
     *
     * @param ids id数组
     */
    @Override
    public void deleteByIds(PK[] ids) {
        for (PK id : ids) {
            deleteById(id);
        }
        /*try {
        } catch (Exception e) {
            throw new ServiceException("删除失败!");
        }*/
    }
}
