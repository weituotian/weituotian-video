package com.weituotian.moudle.api.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.api.entity.vo.AppMember;
import com.weituotian.moudle.front.service.IFrontVideoService;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.service.IMemberService;
import com.weituotian.moudle.member.service.IVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ange on 2017/3/23.
 */
@Controller("apiMemberController")
@RequestMapping("${apiPath}/member")
public class ApiMemberController extends BaseController {

    private final IMemberService memberService;

    private final IVideoService videoService;

    private final IFrontVideoService frontVideoService;

    @Autowired
    public ApiMemberController(IMemberService memberService, IVideoService videoService, IFrontVideoService frontVideoService) {
        this.memberService = memberService;
        this.videoService = videoService;
        this.frontVideoService = frontVideoService;
    }

    /**
     * 获得会员的详细信息
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/info/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object getUserInfo(@PathVariable("id") Integer userId) {
        AppMember member = null;
        try {
            member = memberService.appFindById(userId);
            member.setAvatar(getAvatarPath() + member.getAvatar());
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("没有找到这个用户");
        }
        Result result = renderSuccess("获取成功");
        result.setObj(member);
        return result;
    }

    /**
     * 获得会员的所有视频
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/videos", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object getUserVideo(Integer userId, Integer page, Integer pageSize) {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(page, pageSize);
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("member", userId);
        pageInfo.setCondition(conditions);

        frontVideoService.findLastest(pageInfo);

        request.getServerName();
        //封面相对路径转绝对路径
        for (VideoListVo vo : pageInfo.getList()) {
            vo.setCover(getCoverPath() + vo.getCover());
        }

        Result result = renderSuccess("获取成功");
        result.setObj(pageInfo);
        return result;
    }

    /**
     * 获得用户的关注列表
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/stars", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object getUserStars(Integer userId, Integer page, Integer pageSize) {
        PageInfo<AppMember> pageInfo = new PageInfo<>(page, pageSize);

        memberService.getStars(userId, pageInfo);

        request.getServerName();
        //封面相对路径转绝对路径
        for (AppMember appMember : pageInfo.getList()) {
            appMember.setAvatar(getAvatarPath() + appMember.getAvatar());
        }

        Result result = renderSuccess("获取成功");
        result.setObj(pageInfo);
        return result;
    }

    /**
     * 获得用户的关注列表
     *
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/collects", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object getUserCollects(Integer userId, Integer page, Integer pageSize) {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(page, pageSize);

        memberService.getCollects(userId, pageInfo);

        request.getServerName();
        //封面相对路径转绝对路径
        for (VideoListVo vo : pageInfo.getList()) {
            vo.setCover(getCoverPath() + vo.getCover());
        }

        Result result = renderSuccess("获取成功");
        result.setObj(pageInfo);
        return result;
    }
}
