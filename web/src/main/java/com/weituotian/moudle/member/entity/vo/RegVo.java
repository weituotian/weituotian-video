package com.weituotian.moudle.member.entity.vo;

/**
 * 注册vo
 * Created by ange on 2017/2/22.
 */
public class RegVo {
    private String loginName;
    private String password;
    private String email;
    private String name;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
