package com.weituotian.moudle.member.dao;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.system.dao.IBaseDao;

import java.util.List;

/**
 * Created by ange on 2017/2/22.
 */
public interface IMemberDao extends IBaseDao<Member, Integer> {

    Member findByEmail(String email);

    boolean checkCollect(Integer userid, Integer videoId);

    List<Video> getCollects(Integer userId, PageInfo pageInfo);

    boolean checkStar(Integer cur, Integer target);

    List<Member> getStars(Integer userId, PageInfo pageInfo);

    Integer getNewlyMemberCount();


}
