package com.weituotian.moudle.member.entity.vo;

import com.weituotian.moudle.member.entity.enums.SexEnum;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 我的资料vo
 * Created by ange on 2017/2/28.
 */
public class MemberVo {

    //头像
    private String avatar;

    //描述
    private String descript;

    //性别
    private SexEnum sex;

    //生日
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date birthDate;

    private String name;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
