package com.weituotian.moudle.member.service.impl;

import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.jpa.domain.SimpleSpecification;
import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.DtoUtils;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.api.entity.vo.AppMember;
import com.weituotian.moudle.member.dao.IMemberDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.service.IMemberService;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.service.impl.AdminBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 会员服务
 * Created by ange on 2017/2/22.
 */
@Service("memberService")
@Transactional
public class MemberService extends AdminBaseService<Member, Integer> implements IMemberService {

    private final IMemberDao memberDao;

    private final IVideoDao videoDao;

    @Autowired
    public MemberService(IMemberDao memberDao, IVideoDao videoDao) {
        this.memberDao = memberDao;
        this.videoDao = videoDao;
    }

    @Override
    public IBaseDao<Member, Integer> getBaseDao() {
        return memberDao;
    }

    @Override
    public Specification<Member> pageSpecification(PageInfo<?> pageInfo) {
        return new SimpleSpecification<Member>() {
            @Override
            public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return null;
            }
        };
    }

    @Override
    public AppMember appFindById(Integer id) {
        Member member = memberDao.findOne(id);
        if (member == null) {
            throw new ServiceException("没有这个用户");
        }
        AppMember appMember = new AppMember();
        DtoUtils.copyProperties(member, appMember);
        return appMember;
    }

    @Override
    public Member findByEmail(String email) {
        return memberDao.findByEmail(email);
    }

    public void check(Integer memberId, OpenState state) {
        Member member = memberDao.findOne(memberId);
        if (member == null) {
            throw new ServiceException("没有找到用户");
        }
        member.setStatus(state);
    }

    public Integer addCollect(Integer userId, Integer videoId) {
        Member member = memberDao.findOne(userId);
        Video video = videoDao.findOne(videoId);
        member.getVideoCollect().add(video);
        videoDao.flush();
        Integer size = video.getCollect() + 1;
        video.setCollect(size);//更新收藏数量
        return size;
    }

    public Integer cancelCollect(Integer userId, Integer videoId) {
        Member member = memberDao.findOne(userId);
        Video video = videoDao.findOne(videoId);
        member.getVideoCollect().remove(video);
        videoDao.flush();
        Integer size = video.getCollect() - 1;
        video.setCollect(size);//更新收藏数量
        return size;
    }

    public boolean checkCollect(Integer userId, Integer videoId) {
        return memberDao.checkCollect(userId, videoId);
    }

    public void getCollects(Integer userId, PageInfo<VideoListVo> pageInfo) {
        List<Video> videos = memberDao.getCollects(userId, pageInfo);
        List<VideoListVo> vos = new ArrayList<>();
        for (Video video1 : videos) {
            VideoListVo vo = new VideoListVo();

            DtoUtils.copyProperties(video1, vo);

            vo.setPartitionName(video1.getPartition().getName());
            vo.setMemberName(video1.getMember().getName());

            vos.add(vo);
        }
        pageInfo.setList(vos);
    }

    public void starMember(Integer cur, Integer target) {
        if (cur.equals(target)) {
            throw new ServiceException("自己不能关注自己");
        }
        Member curMember = memberDao.findOne(cur);
        Member targetMember = memberDao.findOne(target);

        //获得关注发起人
        curMember.getMemberFollows().add(targetMember);
        Integer followSize = curMember.getMemberFollows().size();
        curMember.setFollows(followSize);
        memberDao.flush();

        //更新被关注人
        Integer fanSize = targetMember.getMembersFans().size();
        targetMember.setFans(fanSize);
    }

    public void cancelStar(Integer cur, Integer target) {
        Member curMember = memberDao.findOne(cur);
        Member targetMember = memberDao.findOne(target);
        //获得关注发起人
        curMember.getMemberFollows().remove(targetMember);
        Integer followSize = curMember.getMemberFollows().size();
        curMember.setFollows(followSize);
        memberDao.flush();

        //更新被关注人
        Integer fanSize = targetMember.getMembersFans().size();
        targetMember.setFans(fanSize);
    }

    public boolean checkStar(Integer cur, Integer target) {
        return memberDao.checkStar(cur, target);
    }

    public void getStars(Integer userId, PageInfo<AppMember> pageInfo) {
        List<Member> members = memberDao.getStars(userId, pageInfo);
        List<AppMember> vos = new ArrayList<>();
        for (Member member : members) {
            AppMember appMember = new AppMember();
            DtoUtils.copyProperties(member, appMember);
            vos.add(appMember);
        }
        pageInfo.setList(vos);
    }
}
