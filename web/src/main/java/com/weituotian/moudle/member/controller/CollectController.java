package com.weituotian.moudle.member.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.ShiroUtil;
import com.weituotian.moudle.member.service.IMemberService;
import com.weituotian.system.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ange on 2017/3/26.
 */
@Controller("CollectController")
@RequestMapping("${memberPath}/collect")
public class CollectController extends BaseController {

    private final IMemberService memberService;

    @Autowired
    public CollectController(IMemberService memberService) {
        this.memberService = memberService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object collect(Integer videoId) {
        Integer count;
        try {
            User currentUser = ShiroUtil.getCurrentUser();
            count = memberService.addCollect(currentUser.getId(), videoId);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("操作失败");
        }
        Result result = renderSuccess("收藏成功");
        result.setObj(count);
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object cancel(Integer videoId) {
        Integer count;
        try {
            User currentUser = ShiroUtil.getCurrentUser();
            count = memberService.cancelCollect(currentUser.getId(), videoId);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("操作失败");
        }
        Result result = renderSuccess("取消成功");
        result.setObj(count);
        return result;
    }

    @RequestMapping(value = "check", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object checkCollect(Integer videoId) {
        User currentUser = ShiroUtil.getCurrentUser();
        boolean isCollect = false;
        try {
            isCollect = memberService.checkCollect(currentUser.getId(), videoId);
        } catch (Exception e) {
            e.printStackTrace();
            return "服务器错误";
        }
        Result result = renderSuccess("成功");
        result.setObj(isCollect);
        return result;
    }
}
