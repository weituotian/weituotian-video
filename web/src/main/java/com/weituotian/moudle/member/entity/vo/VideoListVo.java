package com.weituotian.moudle.member.entity.vo;

import com.weituotian.core.utils.DtoUtils;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.enums.VideoState;

import java.util.Date;

/**
 * 视频列表Vo
 * Created by ange on 2017/3/8.
 */
public class VideoListVo {

    private int id;

    //封面
    private String cover;

    private String memberName;

    private String partitionName;

    private Integer attachmentId;

    //视频标题
    private String title;

    private VideoState videoState;

    //点击量
    private Integer click;

    //播放量
    private Integer play;

    //收藏量
    private Integer collect;

    //视频简介
    private String descript;

    private Date createTime;

    private Integer totalTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPartitionName() {
        return partitionName;
    }

    public void setPartitionName(String partitionName) {
        this.partitionName = partitionName;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public VideoState getVideoState() {
        return videoState;
    }

    public void setVideoState(VideoState videoState) {
        this.videoState = videoState;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public Integer getPlay() {
        return play;
    }

    public void setPlay(Integer play) {
        this.play = play;
    }

    public Integer getCollect() {
        return collect;
    }

    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    @Override
    public String toString() {
        return "VideoListVo{" +
                "id=" + id +
                ", cover='" + cover + '\'' +
                ", memberName='" + memberName + '\'' +
                ", partitionName='" + partitionName + '\'' +
                ", attachmentId=" + attachmentId +
                ", title='" + title + '\'' +
                ", videoState=" + videoState +
                ", click=" + click +
                ", play=" + play +
                ", collect=" + collect +
                ", descript='" + descript + '\'' +
                ", createTime=" + createTime +
                ", totalTime=" + totalTime +
                '}';
    }
}
