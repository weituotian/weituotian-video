package com.weituotian.moudle.member.service;

import com.weituotian.moudle.member.entity.Partition;
import com.weituotian.system.service.IAdminBaseService;

/**
 * 会员视频服务
 * Created by ange on 2017/2/22.
 */
public interface IPartitionService extends IAdminBaseService<Partition,Integer> {

}
