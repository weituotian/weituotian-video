package com.weituotian.moudle.member.dao.mapper;

import com.weituotian.core.jpa.domain.SimpleDtoMapper;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.front.entity.CommentVo;
import com.weituotian.moudle.member.entity.Comment;
import com.weituotian.moudle.member.entity.enums.SexEnum;

/**
 * Created by ange on 2017/4/10.
 */
public class AdminCommentVoMapper extends SimpleDtoMapper<Comment,AdminCommentVo>{

    @Override
    public AdminCommentVo toDto(Comment comment) {
        AdminCommentVo adminCommentVo= new AdminCommentVo();

        adminCommentVo.setId(comment.getId());

        adminCommentVo.setFloor(0);
        adminCommentVo.setPostTime(comment.getPostTime());
        adminCommentVo.setContent(comment.getContent());

        adminCommentVo.setUserId(0);
        adminCommentVo.setUserName("");
        adminCommentVo.setUserAvatar("");
        adminCommentVo.setUserSex(SexEnum.UNKNOW);

        adminCommentVo.setVideoId(0);
        adminCommentVo.setVideoName("");
        adminCommentVo.setVideoCover("");

        return adminCommentVo;
    }
}
