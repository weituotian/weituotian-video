package com.weituotian.moudle.member.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.api.entity.vo.AppMember;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.service.IAdminBaseService;

import java.util.List;

/**
 * 会员服务
 * Created by ange on 2017/2/22.
 */
public interface IMemberService extends IAdminBaseService<Member,Integer> {

    AppMember appFindById(Integer id);

    Member findByEmail(String email);

    /**
     * 改变用户当前状态，开启或者关闭
     * @param memberId 用户id
     * @param state 状态
     */
    void check(Integer memberId, OpenState state);
    /**
     * 用户增加视频收藏
     * @param userId 用户id
     * @param videoId 视频id
     * @return
     */
    Integer addCollect(Integer userId, Integer videoId);

    /**
     * 用户取消视频收藏
     * @param userId
     * @param videoId
     * @return
     */
    Integer cancelCollect(Integer userId, Integer videoId);

    /**
     * 检查用户是否收藏了某个视频
     * @param userId
     * @param videoId
     * @return
     */
    boolean checkCollect(Integer userId, Integer videoId);

    /**
     * 获得用户的所有收藏
     * @param userId
     * @param pageInfo
     * @return
     */
    void getCollects(Integer userId, PageInfo<VideoListVo> pageInfo);

    /**
     * 关注用户
     * @param cur 当前用户id
     * @param target 目标用户id
     */
    void starMember(Integer cur, Integer target);

    /**
     * 取消关注用户
     * @param cur
     * @param target
     */
    void cancelStar(Integer cur, Integer target);

    /**
     * 检查是否关注了用户
     * @param cur
     * @param target
     * @return
     */
    boolean checkStar(Integer cur, Integer target);

    /**
     * 分页获取某个用户的关注列表
     * @param userId 当前用户id
     * @param pageInfo 分页类
     * @return
     */
    void getStars(Integer userId,PageInfo<AppMember> pageInfo);
}
