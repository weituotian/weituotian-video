package com.weituotian.moudle.member.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * 上传的附件实体
 * Created by ange on 2017/3/4.
 */
@Entity
@Table(name = "attachment")
public class Attachment {

    @Id
    /*@GeneratedValue(strategy = GenerationType.TABLE, generator = "tablegen1")
    @TableGenerator(name = "tablegen1", table = "myidtable", pkColumnName = "pkcolumn1", valueColumnName = "valuecolumn1", pkColumnValue = "pkcolumnvalue1", allocationSize = 1)*/
    @GeneratedValue(generator = "paymentableGenerator")
    @GenericGenerator(name = "paymentableGenerator", strategy = "native")
    private int id;

    private boolean temp;

    private String path;

    private String filename;

    private Date date;

    //所属用于
    private Integer userId;

    //文件类型
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTemp() {
        return temp;
    }

    public void setTemp(boolean temp) {
        this.temp = temp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
