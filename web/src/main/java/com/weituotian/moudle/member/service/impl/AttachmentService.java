package com.weituotian.moudle.member.service.impl;

import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.dao.IAttachmentDao;
import com.weituotian.moudle.member.entity.Attachment;
import com.weituotian.moudle.member.service.IAttachmentService;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.entity.User;
import com.weituotian.system.service.impl.AdminBaseService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;

/**
 * 会员服务
 * Created by ange on 2017/2/22.
 */
@Service("attachmentService")
@Transactional
public class AttachmentService extends AdminBaseService<Attachment, Integer> implements IAttachmentService {

    private final IAttachmentDao attachmentDao;

    @Autowired
    public AttachmentService(IAttachmentDao attachmentDao) {
        this.attachmentDao = attachmentDao;
    }

    @Override
    public IBaseDao<Attachment, Integer> getBaseDao() {
        return attachmentDao;
    }

    @Override
    public Specification<Attachment> pageSpecification(PageInfo<?> pageInfo) {
        return null;
    }

    @Override
    public Attachment addTemp(MultipartFile file, File targetFile) {
        //写入附件表
        Attachment attachment = new Attachment();
        attachment.setDate(new Date());
        attachment.setPath(targetFile.getAbsolutePath());
        attachment.setFilename(targetFile.getName());
        attachment.setTemp(true);
        attachment.setType(file.getContentType());

        User user = (User) SecurityUtils.getSubject().getPrincipal();
        attachment.setUserId(user.getId());

        this.add(attachment);

        return attachment;
    }

}
