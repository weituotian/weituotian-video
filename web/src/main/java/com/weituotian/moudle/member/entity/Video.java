package com.weituotian.moudle.member.entity;

import com.weituotian.core.convert.EnumConverter;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.system.entity.enums.OpenState;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 视频
 * Created by ange on 2017/3/5.
 */
@Entity
@Table(name = "video")
public class Video {

    private int id;

    private Member member;

    private Partition partition;

    //视频标签
    private List<VideoTag> tags;

    //评论
    private List<Comment> comments;

    //封面
    private String cover;

    //视频路径
    private String path;

    private Attachment attachment;

    //视频标题
    private String title;

    //视频简介
    private String descript;

    //视频状态
    private VideoState videoState;

    private OpenState openState;

    //点击量
    private Integer click;

    //播放量
    private Integer play;

    //总时间
    private Integer totalTime;

    //收藏量
    private Integer collect;

    private Date createTime;

    private Date updateTime;

    private Date auditTime;

    @Id
    @GeneratedValue(generator = "paymentableGenerator")
    @GenericGenerator(name = "paymentableGenerator", strategy = "native")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_video_member"))
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "partition_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_video_partition"))
    public Partition getPartition() {
        return partition;
    }

    public void setPartition(Partition partition) {
        this.partition = partition;
    }

    @ManyToMany
    @JoinTable(name = "video_tag", joinColumns = @JoinColumn(name = "video_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id", nullable = false))
    public List<VideoTag> getTags() {
        return tags;
    }

    public void setTags(List<VideoTag> tags) {
        this.tags = tags;
    }

    @OneToMany(mappedBy = "video",cascade = CascadeType.REMOVE)//会删除对应的评论
    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 视频附件
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attachment_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_video_attachment"))
    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    @Enumerated(EnumType.STRING)
    public VideoState getVideoState() {
        return videoState;
    }

    public void setVideoState(VideoState videoState) {
        this.videoState = videoState;
    }

    @Basic
    @Column(name = "open_state")
    @Enumerated(EnumType.STRING)
    public OpenState getOpenState() {
        return openState;
    }

    public void setOpenState(OpenState openState) {
        this.openState = openState;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public Integer getPlay() {
        return play;
    }

    public void setPlay(Integer play) {
        this.play = play;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getCollect() {
        return collect;
    }

    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }
}
