package com.weituotian.moudle.member.entity;

import com.weituotian.moudle.member.entity.enums.SexEnum;
import com.weituotian.system.entity.User;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 注册会员
 * Created by ange on 2017/2/17.
 */
@Entity
@Table(name = "member")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Member extends User {

    //头像
    private String avatar;

    //经验
    private Integer experience;

    //描述
    private String descript;

    //性别
    private SexEnum sex;

    //关注了多少人
    private Integer follows;

    //粉丝数
    private Integer fans;

    //投稿视频数量
    private Integer videos;

    //生日
    private Date birthDate;

    //注册日期
    private Date regDate;

    //地址
    private String address;

    //这个会员关注的人
    private Set<Member> memberFollows;

    //这个会员的粉丝
    private Set<Member> membersFans;

    //收藏的视频
    private Set<Video> videoCollect;

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer level) {
        this.experience = level;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Integer getFollows() {
        return follows;
    }

    public void setFollows(Integer follow) {
        this.follows = follow;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public Integer getVideos() {
        return videos;
    }

    public void setVideos(Integer videos) {
        this.videos = videos;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    @ManyToMany
    @JoinTable(name = "member_follow", joinColumns = @JoinColumn(name = "member_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "up_id", referencedColumnName = "id", nullable = false))
    public Set<Member> getMemberFollows() {
        return memberFollows;
    }

    public void setMemberFollows(Set<Member> memberFollow) {
        this.memberFollows = memberFollow;
    }

    @ManyToMany(mappedBy = "memberFollows")
    public Set<Member> getMembersFans() {
        return membersFans;
    }

    public void setMembersFans(Set<Member> membersFans) {
        this.membersFans = membersFans;
    }

    @ManyToMany
    @JoinTable(name = "member_collect", joinColumns = @JoinColumn(name = "member_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "video_id", referencedColumnName = "id", nullable = false))
    public Set<Video> getVideoCollect() {
        return videoCollect;
    }

    public void setVideoCollect(Set<Video> videoCollect) {
        this.videoCollect = videoCollect;
    }
}
