package com.weituotian.moudle.member.entity.vo;

/**
 * 视频vo
 * Created by ange on 2017/3/7.
 */
public class VideoVo {

    //封面
    private String cover;

    private Integer partitionId;

    private Integer attachmentId;

    //视频标题
    private String title;

    //视频简介
    private String descript;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }
}
