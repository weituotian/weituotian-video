package com.weituotian.moudle.member.dao;

import com.weituotian.moudle.member.entity.Attachment;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.system.dao.IBaseDao;

/**
 * Created by ange on 2017/2/22.
 */
public interface IAttachmentDao extends IBaseDao<Attachment, Integer> {

}
