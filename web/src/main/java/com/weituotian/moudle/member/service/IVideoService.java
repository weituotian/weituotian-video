package com.weituotian.moudle.member.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.entity.vo.VideoVo;
import com.weituotian.system.service.IAdminBaseService;

/**
 * 会员视频服务
 * Created by ange on 2017/2/22.
 */
public interface IVideoService extends IAdminBaseService<Video, Integer> {

    void addByDto(VideoVo videoVo);

    void updateByDto(VideoVo videoVo, Integer id);

    /**
     * 通过id找到视频，已经加载了它关联的实体了
     * @param id 视频id
     * @return 视频实体
     */
    Video findOneFetch(Integer id);

    /**
     * [会员后台专用]
     * 分页,会员的视频
     * 使用criterial的方式
     * @param pageInfo 分页信息
     */
    void findPageMember(Integer userId, PageInfo<Video> pageInfo);

    /**
     * [会员后台专用]
     * 分页,会员的视频,返回videolistvo
     * 使用拼接jpql的方式实现
     *
     * @param pageInfo 分页信息
     */
    void findPageMemberVo(Integer userId, PageInfo<VideoListVo> pageInfo);

}
