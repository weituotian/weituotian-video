package com.weituotian.moudle.member.dao;

import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.member.dao.mapper.AdminCommentVoMapper;
import com.weituotian.moudle.member.entity.*;
import com.weituotian.system.dao.IBaseDao;
import org.hibernate.Session;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-03-25.
 */
public interface ICommentDao extends IBaseDao<Comment, Integer> {

    List<Comment> getComments(Integer videoId, PageInfo pageInfo);

    List<AdminCommentVo> findPage(PageInfo<AdminCommentVo> pageInfo, Specification<Comment> spec);
}
