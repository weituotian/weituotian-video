package com.weituotian.moudle.member.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.ShiroUtil;
import com.weituotian.moudle.member.service.IMemberService;
import com.weituotian.system.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ange on 2017/3/27.
 */
@Controller("FollowController")
@RequestMapping("${memberPath}/follow")
public class FollowController extends BaseController {

    private final IMemberService memberService;

    @Autowired
    public FollowController(IMemberService memberService) {
        this.memberService = memberService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object collect(Integer target) {
        try {
            User currentUser = ShiroUtil.getCurrentUser();
            memberService.starMember(currentUser.getId(), target);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("操作失败");
        }
        return renderSuccess("关注成功");
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object cancel(Integer target) {
        try {
            User currentUser = ShiroUtil.getCurrentUser();
            memberService.cancelStar(currentUser.getId(), target);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("操作失败");
        }
        return renderSuccess("取消关注成功");
    }

    @RequestMapping(value = "check", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object checkCollect(Integer target) {
        User currentUser = ShiroUtil.getCurrentUser();
        boolean isStar;
        try {
            isStar = memberService.checkStar(currentUser.getId(), target);
        } catch (Exception e) {
            e.printStackTrace();
            return "服务器错误";
        }
        Result result = renderSuccess("查询成功");
        result.setObj(isStar);
        return result;
    }

}
