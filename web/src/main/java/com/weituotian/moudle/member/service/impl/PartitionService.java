package com.weituotian.moudle.member.service.impl;

import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.dao.IPartitionDao;
import com.weituotian.moudle.member.entity.Partition;
import com.weituotian.moudle.member.service.IPartitionService;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.service.impl.AdminBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 会员视频服务
 * Created by ange on 2017/2/22.
 */
@Service("partitionService")
@Transactional
public class PartitionService extends AdminBaseService<Partition, Integer> implements IPartitionService {

    private final IPartitionDao partitionDao;

    @Autowired
    public PartitionService(IPartitionDao partitionDao) {
        this.partitionDao = partitionDao;
    }

    @Override
    public IBaseDao<Partition, Integer> getBaseDao() {
        return partitionDao;
    }

    @Override
    public Specification<Partition> pageSpecification(PageInfo<?> pageInfo) {
        return null;
    }
}
