package com.weituotian.moudle.member.dao.impl;

import com.weituotian.moudle.member.dao.IPartitionDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.Partition;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.system.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * 视频分区Dao
 * Created by ange on 2017/2/22.
 */
@Repository
public class PartitionDao extends BaseDao<Partition, Integer> implements IPartitionDao {

    @Override
    protected String getEntityName() {
        return "Partition";
    }

    @Override
    protected String getIdFieldName() {
        return "id";
    }
}
