package com.weituotian.moudle.member.dao;

import com.weituotian.core.jpa.domain.JPQLParamsSetter;
import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.system.dao.IBaseDao;

import java.util.List;

/**
 * 视频Dao
 * Created by ange on 2017/2/22.
 */
public interface IVideoDao extends IBaseDao<Video, Integer> {

    Video findOneFetch(Integer id);

    List<VideoListVo> findPageMember(Integer memberId, String where1);

    List<VideoListVo> findPageMember(Integer memberId, String where1, PageInfo<?> pageInfo, JPQLParamsSetter setter);

    /**
     * [前台专用]
     * 分页,查找所有视频
     * @return
     */
    List<VideoListVo> findLastest(PageInfo<VideoListVo> pageInfo);

    List<VideoListVo> findLastest2(PageInfo<VideoListVo> pageInfo, Specification<Video> spec);

    /**
     * 根据附件id找到该视频
     *
     * @param attachmentId
     * @return
     */
    List<Video> findByAttachmentId(Integer attachmentId);

    /**
     * 根据标题找到该视频
     *
     * @param title
     * @return
     */
    List<Video> findByTitle(String title);

    /**
     * 获得用户上传的视频数
     * @param memberId 会员id
     * @return
     */
    Integer getMemberVideosCount(Integer memberId);

    /**
     * 获得用户上传的视频的所有评论总数
     * @param memberId
     * @return
     */
    Integer getMemberVideoCommentsCount(Integer memberId);

    /**
     * 获得最新上传的视频数量
     * @return
     */
    Integer getNewlyVideoCount();
}
