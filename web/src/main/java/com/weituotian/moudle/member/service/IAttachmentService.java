package com.weituotian.moudle.member.service;

import com.weituotian.moudle.member.entity.Attachment;
import com.weituotian.system.service.IAdminBaseService;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * 会员服务
 * Created by ange on 2017/2/22.
 */
public interface IAttachmentService extends IAdminBaseService<Attachment,Integer> {

    Attachment addTemp(MultipartFile file, File targetFile);

}
