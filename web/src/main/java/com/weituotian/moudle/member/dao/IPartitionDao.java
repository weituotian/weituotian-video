package com.weituotian.moudle.member.dao;

import com.weituotian.moudle.member.entity.Partition;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.system.dao.IBaseDao;

/**
 * 视频分区Dao
 * Created by ange on 2017/2/22.
 */
public interface IPartitionDao extends IBaseDao<Partition, Integer> {

}
