package com.weituotian.moudle.member.dao.impl;

import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.member.dao.ICommentDao;
import com.weituotian.moudle.member.dao.mapper.AdminCommentVoMapper;
import com.weituotian.moudle.member.entity.*;
import com.weituotian.system.dao.impl.BaseDao;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-03-25.
 */
@Repository
public class CommentDao extends BaseDao<Comment, Integer> implements ICommentDao {

    @Override
    protected String getEntityName() {
        return "Comment";
    }

    @Override
    protected String getIdFieldName() {
        return "id";
    }


    public List<Comment> getComments(Integer videoId, PageInfo pageInfo) {
        /*Session session = getSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Comment> query = builder.createQuery(Comment.class);

        Root<Comment> root = query.from(Comment.class);

        query.select(root);*/

        String jpql = "select comment from Comment comment left join fetch comment.member where comment.video.id=:id order by comment.postTime desc";
        Query query = getSession().createQuery(jpql);
        query.setParameter("id", videoId);

        //分页
        query.setFirstResult(pageInfo.getFrom());
        query.setMaxResults(pageInfo.getPagesize());

        //总数
        String count = "select count(comment) from Comment comment where comment.video.id=:id";
        Query query1 = getSession().createQuery(count).setParameter("id", videoId);
        Long total = (Long) query1.getSingleResult();
        pageInfo.setTotal(total.intValue());


        return query.getResultList();
    }

    public List<AdminCommentVo> findPage(PageInfo<AdminCommentVo> pageInfo, Specification<Comment> spec) {
        Session session = getSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
//        CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
        CriteriaQuery<Tuple> cq = cb.createTupleQuery();

        Root<Comment> comment = cq.from(Comment.class);

        /* where 条件 */
        Predicate predicate = spec.toPredicate(comment, cq, cb);
        cq.where(predicate);

        /* order 排序 */
        Map<String, String> sort = pageInfo.getOrderby();
        if (sort != null) {
            List<Order> orders = toOrders(sort, comment, cb);
            spec.onOrder(orders, comment, cq, cb);
            cq.orderBy(orders);
        }

        /*-----select-----*/
        Path<Video> video = comment.get(Comment_.video);
        Path<Member> member = comment.get(Comment_.member);

        /*CompoundSelection<Object[]> arrayVideo = cb.array();
        CompoundSelection<Object[]> arrayMember = cb.array();*/

        cq.select(cb.tuple(comment,
                video.get(Video_.id), video.get(Video_.title), video.get(Video_.cover),
                member.get(Member_.id), member.get(Member_.name), member.get(Member_.avatar)));

        TypedQuery<Tuple> query = session.createQuery(cq);

        //分页
        query.setFirstResult(pageInfo.getFrom());
        query.setMaxResults(pageInfo.getPagesize());

        //总数
        Long total = executeCountQuery(getCountQuery(spec));
        pageInfo.setTotal(total.intValue());

        //结果list
//        List<Tuple> result = query.getResultList();
        List<Tuple> result = total > pageInfo.getFrom() ? query.getResultList() : Collections.<Tuple>emptyList();

        /* 转换成vo */
        List<AdminCommentVo> vos = new ArrayList<>();
        AdminCommentVoMapper mapper = new AdminCommentVoMapper();
        for (Tuple tuple : result) {
            Comment comment1 = tuple.get(0, Comment.class);

            AdminCommentVo vo = mapper.toDto(comment1);

            vo.setVideoId((Integer) tuple.get(1));//id
            vo.setVideoName((String) tuple.get(2));//title
            vo.setVideoCover((String) tuple.get(3)); //cover

            vo.setUserId((Integer) tuple.get(4)); //id
            vo.setUserName((String) tuple.get(5));//name
            vo.setUserAvatar((String) tuple.get(6));//avatat

            vos.add(vo);
        }

        return vos;
    }

}
