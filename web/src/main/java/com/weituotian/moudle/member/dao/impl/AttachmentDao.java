package com.weituotian.moudle.member.dao.impl;

import com.weituotian.moudle.member.dao.IAttachmentDao;
import com.weituotian.moudle.member.entity.Attachment;
import com.weituotian.system.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;

import java.io.File;


/**
 * Created by ange on 2017/2/22.
 */
@Repository
public class AttachmentDao extends BaseDao<Attachment, Integer> implements IAttachmentDao {

    @Override
    protected String getEntityName() {
        return "Attachment";
    }

    @Override
    protected String getIdFieldName() {
        return "id";
    }

    public void delete(Integer id, String realPath) throws Exception {
        Attachment attachment = findOne(id);
        File targetFile = new File(realPath, attachment.getFilename());
        boolean delete = targetFile.delete();
        if (delete) {
            delete(attachment);
        }else {
            throw new Exception("文件删除失败");
        }
    }

}
