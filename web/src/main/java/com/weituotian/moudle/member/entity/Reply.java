package com.weituotian.moudle.member.entity;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.Date;

/**
 * 回复
 * Created by Administrator on 2017-03-25.
 */
@Entity
@Table(name = "reply")
public class Reply {

    private int id;

    private String content;

    private Date postTime;

    private Member member;

    private Comment comment;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    @ManyToOne
    @JoinColumn(name = "member_id")
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @ManyToOne
//    @BatchSize(size = 1)//comment
    @JoinColumn(name = "comment_id")
    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
