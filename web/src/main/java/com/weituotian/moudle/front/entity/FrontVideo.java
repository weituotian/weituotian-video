package com.weituotian.moudle.front.entity;

import com.weituotian.moudle.member.entity.VideoTag;
import com.weituotian.moudle.member.entity.enums.VideoState;

import java.util.Date;
import java.util.List;

/**
 * Created by ange on 2017/3/24.
 */
public class FrontVideo {

    private int id;

    //封面
    private String cover;

    //视频标题
    private String title;

    //视频简介
    private String descript;

    //点击量
    private Integer click;

    //播放量
    private Integer play;

    //总时间
    private Integer totalTime;

    //收藏量
    private Integer collect;

    private Date createTime;

    //上传者
    private Integer memberId;
    private String memberCover;
    private String memberName;

    private VideoState videoState;

    private String partitionName;

    private String token;

    private List<VideoTag> frontTags;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public Integer getPlay() {
        return play;
    }

    public void setPlay(Integer play) {
        this.play = play;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getCollect() {
        return collect;
    }

    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public VideoState getVideoState() {
        return videoState;
    }

    public void setVideoState(VideoState videoState) {
        this.videoState = videoState;
    }

    public String getPartitionName() {
        return partitionName;
    }

    public void setPartitionName(String partitionName) {
        this.partitionName = partitionName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<VideoTag> getFrontTags() {
        return frontTags;
    }

    public void setFrontTags(List<VideoTag> frontTags) {
        this.frontTags = frontTags;
    }

    public String getMemberCover() {
        return memberCover;
    }

    public void setMemberCover(String memberCover) {
        this.memberCover = memberCover;
    }
}
