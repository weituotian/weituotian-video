package com.weituotian.moudle.front.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.front.service.IFrontVideoService;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * 首页控制器
 * Created by ange on 2017/3/9.
 */
@Controller
public class IndexController extends BaseController {

    private final IFrontVideoService frontVideoService;

    @Autowired
    public IndexController(IFrontVideoService frontVideoService) {
        this.frontVideoService = frontVideoService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        /*model.addAttribute("login_url", contextPath + "/login");
        model.addAttribute("reg_url", contextPath + memberPath + "/reg/index");*/
    }

    @RequestMapping(value = "*", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String hello(HttpServletRequest request) {
        logger.info("/notfount referer{}", request.getServletPath());
        return "hello";
    }

    @RequestMapping("/")
    public String index(Model model) {

        PageInfo<VideoListVo> pageInfo = new PageInfo<>(1, 4);
        pageInfo.setCondition(new HashMap<String, Object>());
        frontVideoService.findLastest(pageInfo);

        //封面相对路径转绝对路径
        for (VideoListVo vo : pageInfo.getList()) {
            vo.setCover(getCoverPath() + vo.getCover());
        }

        model.addAttribute("pageinfo", pageInfo);
        model.addAttribute("pageName", "韦驮天视频");

        String contextPath = getContextPath();
        model.addAttribute("qrcode_url", contextPath + frontPath + "/qrcode/code");
        model.addAttribute("apk_url", contextPath + "/upload" + "/file/WeituotianVideo.apk");
        return "/front/index_v2";
    }

    @RequestMapping("/notfount")
    public String notfount(final HttpServletRequest request) {
        final String referer = request.getHeader("referer");
        logger.info("/notfount referer{}", referer);
        return "/system/notfount";
    }
}
