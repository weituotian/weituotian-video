package com.weituotian.moudle.front.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.moudle.common.service.impl.qrcode.CoderService;
import com.weituotian.moudle.common.service.impl.qrcode.LogoConfig;
import com.weituotian.moudle.common.service.impl.qrcode.ZxingDecoderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.File;

/**
 * 可以生成二维码
 * Created by ange on 2017/4/13.
 */
@Controller("ZxingEncodeController")
@RequestMapping("${frontPath}/qrcode")
public class ZxingEncodeController extends BaseController {
    @Autowired
    public void setLogiconfig(LogoConfig logiconfig) {
        this.logiconfig = logiconfig;
    }

    @Autowired
    public void setZxingDecodeService(ZxingDecoderService zxingDecodeService) {
        this.zxingDecodeService = zxingDecodeService;
    }

    @Autowired
    public void setCoderService(CoderService coderService) {
        this.coderService = coderService;
    }

    private LogoConfig logiconfig;
    private ZxingDecoderService zxingDecodeService;
    private CoderService coderService;

    /**
     * 解码
     *
     * @param realImgPath
     * @param session
     * @return
     */
    @RequestMapping(value = "/decode", method = RequestMethod.GET)
    public ModelAndView zxingdecode(@RequestParam("realImgPath") String realImgPath, HttpSession session) {
        String uploadPath = "/images";
        String realUploadPath = session.getServletContext().getRealPath(uploadPath);
        String imgPath = realUploadPath + "/" + realImgPath;
        String result = zxingDecodeService.zxingdecode(imgPath);

        ModelAndView ret = new ModelAndView();
        ret.addObject("result", result);
        ret.setViewName("zxingdecode");

        return ret;
    }


    /**
     * 生成 http://blog.csdn.net/wenteryan/article/details/51373712
     *
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/code", method = RequestMethod.GET)
    @ResponseBody
    public String watermark(HttpSession session) throws Exception {
        String uploadPath = "/upload/images";
        String imageName = "apkdownload" + ".png";
        String realUploadPath = session.getServletContext().getRealPath(uploadPath);

        File image = new File(realUploadPath + "/" + imageName);
        if (image.exists()) {
            return getContextPath() + uploadPath + "/" + imageName;
        } else {

            // 模拟订单详情
            String contents = getFullContextPath() + "/file/WeituotianVideo.apk";

            int width = 300;
            int height = 300;
            String zxingImage = coderService.encode(contents, width, height, uploadPath, realUploadPath, imageName);
            return getContextPath() + zxingImage;
        }
    }
}
