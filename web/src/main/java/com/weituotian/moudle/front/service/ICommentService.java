package com.weituotian.moudle.front.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.common.service.IBaseService;
import com.weituotian.moudle.front.entity.CommentVo;
import com.weituotian.moudle.member.entity.Comment;

import java.util.List;

/**
 * Created by Administrator on 2017-03-25.
 */
public interface ICommentService extends IBaseService<Comment, Integer> {

    List<CommentVo> getCommentVos(Integer videoId, PageInfo pageInfo);

    void addComment(Integer userId, String content, Integer videoId);

}
