package com.weituotian.moudle.front.quarts;

import com.weituotian.moudle.front.service.IFrontVideoService;
import com.weituotian.moudle.member.service.IVideoService;
import com.weituotian.moudle.member.service.impl.VideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 排行榜任务
 * Created by Administrator on 2017-03-10.
 */
@Component("RankingList")
public class RankingList {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    private final IFrontVideoService frontVideoService;

    @Autowired
    public RankingList(IFrontVideoService frontVideoService) {
        this.frontVideoService = frontVideoService;
    }

    /**
     * 更新首页的视频排行榜
     */
    public void updateIndex() {
        logger.info("update test-----------------------------------------");
    }

    public void test() {
//        videoService.findPage(page);
    }

}
