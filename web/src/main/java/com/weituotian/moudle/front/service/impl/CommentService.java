package com.weituotian.moudle.front.service.impl;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.common.service.impl.BaseService;
import com.weituotian.moudle.front.entity.CommentVo;
import com.weituotian.moudle.front.service.ICommentService;
import com.weituotian.moudle.member.dao.ICommentDao;
import com.weituotian.moudle.member.dao.IMemberDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.Comment;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017-03-25.
 */
@Service
public class CommentService extends BaseService<Comment, Integer> implements ICommentService {

    private final ICommentDao commentDao;

    private final IVideoDao videoDao;


    private final IMemberDao memberDao;

    @Autowired
    public CommentService(ICommentDao commentDao, IVideoDao videoDao, IMemberDao memberDao) {
        this.commentDao = commentDao;
        this.videoDao = videoDao;
        this.memberDao = memberDao;
    }

    @Override
    public IBaseDao<Comment, Integer> getBaseDao() {
        return commentDao;
    }

    public List<CommentVo> getCommentVos(Integer videoId, PageInfo pageInfo) {
        List<Comment> comments = commentDao.getComments(videoId, pageInfo);
        List<CommentVo> vos = new ArrayList<>();
        for (Comment comment : comments) {
            CommentVo commentVo = new CommentVo();
            commentVo.setId(comment.getId());
            commentVo.setContent(comment.getContent());
            commentVo.setPostTime(comment.getPostTime());
            commentVo.setUserAvatar(comment.getMember().getAvatar());
            commentVo.setUserName(comment.getMember().getName());
            commentVo.setUserSex(comment.getMember().getSex());
            vos.add(commentVo);
        }
        return vos;
    }

    @Override
    public void addComment(Integer userId, String content, Integer videoId) {
        Member member = memberDao.findOne(userId);
        Video video = videoDao.findOne(videoId);
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setPostTime(new Date());
        comment.setMember(member);
        comment.setVideo(video);
        commentDao.persist(comment);
    }

}
