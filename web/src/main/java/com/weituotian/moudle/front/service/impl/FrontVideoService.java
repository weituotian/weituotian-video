package com.weituotian.moudle.front.service.impl;

import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.jpa.domain.SimpleSpecification;
import com.weituotian.core.utils.DtoUtils;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.common.service.impl.BaseService;
import com.weituotian.moudle.front.entity.FrontVideo;
import com.weituotian.moudle.front.service.IFrontVideoService;
import com.weituotian.moudle.member.dao.IMemberDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.Member_;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.VideoTag;
import com.weituotian.moudle.member.entity.Video_;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.entity.enums.OpenState;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * Created by ange on 2017/3/10.
 */
@Service("frontVideoService")
@Transactional
public class FrontVideoService extends BaseService<Video, Integer> implements IFrontVideoService {

    private final IVideoDao videoDao;

    @Autowired
    private IMemberDao memberDao;

    @Autowired
    public FrontVideoService(IVideoDao videoDao) {
        this.videoDao = videoDao;
    }

    @Override
    public IBaseDao<Video, Integer> getBaseDao() {
        return videoDao;
    }

    @Override
    public void findLastest(PageInfo<VideoListVo> pageInfo) {
        pageInfo.setList(videoDao.findLastest(pageInfo));
    }


    @Override
    public FrontVideo findFrontVideo(Integer videoId) {
        Video video = videoDao.findOneFetch(videoId);
        if (video == null) {
            throw new ServiceException("不存在该视频");
        }
        FrontVideo frontVideo = new FrontVideo();

        //复制属性
        List<VideoTag> tags = video.getTags();
        if (tags.size() > 0) {
            frontVideo.setFrontTags(tags);
        }

        DtoUtils.copyProperties(video, frontVideo);

        //视频访问token是当前用户sessionid和token拼接而来
        Session session = SecurityUtils.getSubject().getSession();
        String concat = session.getId() + frontVideo.getTitle();
        String token = Base64.getUrlEncoder().encodeToString(concat.getBytes());
        frontVideo.setToken(token);

        frontVideo.setMemberName(video.getMember().getName());
        frontVideo.setMemberCover(video.getMember().getAvatar());
        frontVideo.setMemberId(video.getMember().getId());
        frontVideo.setPartitionName(video.getPartition().getName());

        return frontVideo;
//        Base64.encode(concat.getBytes());
//        frontVideo.setToken();
    }

    @Override
    public String getVideoPath(Integer videoId, String token) {
        Video video = videoDao.findOneFetch(videoId);
        if (video == null) {
            throw new ServiceException("不存在该视频");
        }
        //播放+1
        video.setPlay(video.getPlay() + 1);
        String filename = video.getAttachment().getFilename();
        return filename;
    }
}
