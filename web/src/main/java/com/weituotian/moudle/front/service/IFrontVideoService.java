package com.weituotian.moudle.front.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.common.service.IBaseService;
import com.weituotian.moudle.common.service.impl.BaseService;
import com.weituotian.moudle.front.entity.FrontVideo;
import com.weituotian.moudle.member.entity.Video;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.system.service.IAdminBaseService;

import java.util.List;

/**
 * 前台的视频service
 * Created by ange on 2017/3/10.
 */
public interface IFrontVideoService extends IBaseService<Video, Integer> {

    /**
     * 首页显示的最新视频
     * @return
     */
    void findLastest(PageInfo<VideoListVo> pageInfo);

    /**
     * 返回一个在前台显示的视频的vo
     * @param videoId
     */
    FrontVideo findFrontVideo(Integer videoId);


    String getVideoPath(Integer videoId, String token);
}
