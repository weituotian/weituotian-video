package com.weituotian.moudle.admin.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.admin.service.IAdminCommentService;
import com.weituotian.moudle.member.entity.Comment;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.system.entity.enums.OpenState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * Created by ange on 2017/4/8.
 */
@RequestMapping("${adminPath}/comment")
@Controller("CommentController")
public class CommentController extends BaseController {

    private final IAdminCommentService commentService;

    @Autowired
    public CommentController(IAdminCommentService commentService) {
        this.commentService = commentService;
    }


    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        model.addAttribute("delete_url", contextPath + adminPath + "/comment/delete");
        model.addAttribute("edit_url", contextPath + adminPath + "/comment/edit");
        model.addAttribute("member_edit_url", contextPath + adminPath + "/member/edit");
        model.addAttribute("video_index_url", contextPath + adminPath + "/video/index");
    }

    @RequestMapping("/index")
    public String index(Integer page, Integer pagesize, Model model) {
        PageInfo<AdminCommentVo> pageInfo = new PageInfo<>(page, 10);

        //条件
        pageInfo.setCondition(getSearchMap());

        pageInfo.addSort("postTime", "asc");

        commentService.findPageVo(pageInfo);

        model.addAttribute("openStatus", OpenState.values());
        model.addAttribute("videoState", VideoState.values());
        model.addAttribute("pageName", "管理评论");
        model.addAttribute("pageinfo", pageInfo);
        return "admin/comment/index";
    }


    /**
     * 编辑页面
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Integer id, Model model) {
        Comment comment = commentService.findById(id);

        //返回视图
        model.addAttribute("comment", comment);

        String contextPath = this.getContextPath();
        model.addAttribute("form_url", contextPath + adminPath + "/comment/doedit");

        model.addAttribute("action", "edit");
        model.addAttribute("pageName", "评论编辑");
        return "admin/comment/edit";
    }

    /**
     * 处理编辑
     *
     * @return
     */
    @RequestMapping(value = "/doedit", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doedit(Integer cid, String content, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date time) {
        Comment comment = commentService.findById(cid);
        comment.setContent(content);
        comment.setPostTime(time);
        try {
            commentService.update(comment);
        } catch (ServiceException e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("修改成功！");
    }

    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
            commentService.deleteByIds(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("删除成功!");
    }
}
