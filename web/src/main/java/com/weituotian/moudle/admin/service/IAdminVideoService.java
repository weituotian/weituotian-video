package com.weituotian.moudle.admin.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.moudle.member.entity.vo.VideoListVo;

/**
 * Created by ange on 2017/4/3.
 */
public interface IAdminVideoService {

    /**
     * 管理员管理分页列表
     * @param pageInfo
     */
    void findPage(PageInfo<VideoListVo> pageInfo);

    /**
     * 获得视频地址
     * @param videoId
     * @return
     */
    String getVideoPath(Integer videoId);

    /**
     * 设置视频状态
     * @param videoId
     * @param videoState
     */
    void check(Integer videoId, VideoState videoState);

    void delete(Integer[] ids);
}
