package com.weituotian.moudle.admin.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.moudle.member.entity.enums.SexEnum;
import com.weituotian.moudle.member.entity.vo.MemberVo;
import com.weituotian.moudle.member.service.IMemberService;
import com.weituotian.system.entity.enums.OpenState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 管理会员
 * Created by ange on 2017/4/4.
 */
@RequestMapping("${adminPath}/member")
@Controller("AdminMemberController")
public class AdminMemberController extends BaseController {

    private final IMemberService memberService;

    @Autowired
    public AdminMemberController(IMemberService memberService) {
        this.memberService = memberService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        model.addAttribute("edit_url", contextPath + adminPath + "/member/edit");
        model.addAttribute("check_url", contextPath + adminPath + "/member/check");
        model.addAttribute("avatar_url", contextPath + memberPath + "/my/avaterupload");
        model.addAttribute("delete_url", contextPath + adminPath + "/member/delete");
    }

    /*在vo使用了@datetimeformat的方法，这里不需要了*/
    /*@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);

        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));   //true:允许输入空值，false:不能为空值
    }*/

    @RequestMapping("/index")
    public String index(Integer page, Integer pagesize, Model model) {
        PageInfo<Member> pageInfo = new PageInfo<>(page, 10);

        //条件
        pageInfo.setCondition(getSearchMap());

        memberService.findPage(pageInfo);

        model.addAttribute("openState", OpenState.values());
        model.addAttribute("sexEnum", SexEnum.values());
        model.addAttribute("pageName", "管理会员");
        model.addAttribute("pageinfo", pageInfo);
        return "admin/member/index";
    }

    @RequestMapping("/edit")
    public String edit(Integer memberId, Model model) {

        Member member = memberService.findById(memberId);
        /*if (member.getAvatar() != null) {
            member.setAvatar(getAvatarPath() + member.getAvatar());
        }*/

        model.addAttribute("member", member);

        model.addAttribute("sexEnum", SexEnum.values());

        String contextPath = this.getContextPath();
        model.addAttribute("form_url", contextPath + adminPath + "/member/doedit");

        model.addAttribute("pageName", "修改用户");
        return "member/my/edit";
    }

    @RequestMapping(value = "/doedit", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doedit(MemberVo memberVo, @RequestParam("member_id") Integer memberId) {//,
        try {
            memberService.updateByDto(memberVo, memberId);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("修改成功");
    }

    @RequestMapping(value = "/check", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object check(OpenState status, Integer memberId) {
        try {
            memberService.check(memberId, status);
        } catch (Exception e) {
            e.printStackTrace();
            Result result = renderError("服务器发生了错误");
            if (e instanceof ServiceException) {
                result.setMsg(e.getMessage());
            }
            return result;
        }
        return renderSuccess("设置成功");
    }

    /**
     * 批量删除
     *
     * @param ids ids
     * @return
     */
    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
            memberService.deleteByIds(ids);
//            userService.deleteBatch(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("删除失败，请先确认用户的评论是否删除完成");
        }
        return renderSuccess("删除成功!");
    }

}
