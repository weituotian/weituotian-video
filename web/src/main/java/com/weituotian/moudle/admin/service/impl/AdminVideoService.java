package com.weituotian.moudle.admin.service.impl;

import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.jpa.domain.SimpleSpecification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.service.IAdminVideoService;
import com.weituotian.moudle.member.dao.IAttachmentDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.moudle.member.entity.*;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.system.entity.enums.OpenState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 管理员管理后台服务
 * Created by ange on 2017/4/3.
 */
@Service
@Transactional
public class AdminVideoService implements IAdminVideoService {

    private final IVideoDao videoDao;

    private final IAttachmentDao attachmentDao;

    @Autowired
    public AdminVideoService(IVideoDao videoDao, IAttachmentDao attachmentDao) {
        this.videoDao = videoDao;
        this.attachmentDao = attachmentDao;
    }

    public void findPage(PageInfo<VideoListVo> pageInfo) {
        SimpleSpecification<Video> spec = new SimpleSpecification<Video>() {
            @Override
            public void onOrder(List<Order> orders, Root<Video> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                orders.add(0, cb.asc(root.get(Video_.videoState)));
            }

            @Override
            public Predicate toPredicate(Root<Video> video, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<Predicate>();

                for (Map.Entry<String, Object> entry : pageInfo.getCondition().entrySet()) {
                    String name = entry.getKey();
                    String value = String.valueOf(entry.getValue());
                    if (name.equals("name")) {
                        //名字
                        predicates.add(cb.like(video.get(Video_.title), "%" + value + "%"));
                    } else if (name.equals("partition")) {
                        //分区
                        predicates.add(cb.equal(video.get(Video_.partition).get(Partition_.id), value));
                    } else if (name.equals("open")) {
                        //视频开启状态
                        try {
                            OpenState valueEnum = OpenState.valueOf(value);
                            predicates.add(cb.equal(video.get(Video_.partition).get(Partition_.id), valueEnum));
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    } else if (name.equals("status")) {
                        //视频审核状态
                        try {
                            VideoState videoState = VideoState.valueOf(value);
                            predicates.add(cb.equal(video.get(Video_.videoState), videoState));//审核通过的
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //搜索上传者名字
                        if (name.equals("member")) {
                            predicates.add(cb.like(video.get(Video_.member).get(Member_.name), "%" + value + "%"));
                        }
                    }
                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        };
        pageInfo.setList(videoDao.findLastest2(pageInfo, spec));
    }

    @Override
    public String getVideoPath(Integer videoId) {
        Video video = videoDao.findOneFetch(videoId);
        if (video == null) {
            throw new ServiceException("不存在该视频");
        }
        String name = video.getAttachment().getFilename();
        if (name == null || name.equals("")) {
            throw new ServiceException("视频路径错误了");
        }
        return name;
    }

    public void check(Integer videoId, VideoState videoState) {
        Video video = videoDao.findOne(videoId);
        if (video != null) {
            video.setVideoState(videoState);
        } else {
            throw new ServiceException("找不到可以设置的视频");
        }
    }

    public void delete(Integer[] ids) {
        for (Integer id : ids) {
            Video one = videoDao.findOneFetch(id);

            //对应的附件设置为临时附件
            Attachment attachment = one.getAttachment();
            attachment.setTemp(true);

            //删除视频
            videoDao.delete(id);
        }
    }
}
