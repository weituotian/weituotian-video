package com.weituotian.moudle.admin.service;

import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.entity.vo.AdminCommentVo;
import com.weituotian.moudle.common.service.IBaseService;
import com.weituotian.moudle.member.entity.Comment;
import com.weituotian.system.service.IAdminBaseService;

/**
 * Created by Administrator on 2017-03-25.
 */
public interface IAdminCommentService extends IBaseService<Comment, Integer> {

    /**
     * 管理员管理分页列表
     *
     * @param pageInfo
     */
    void findPageVo(PageInfo<AdminCommentVo> pageInfo);

}
