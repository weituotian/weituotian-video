package com.weituotian.moudle.admin.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.exception.ControllerException;
import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.admin.service.IAdminVideoService;
import com.weituotian.moudle.member.entity.enums.VideoState;
import com.weituotian.moudle.member.entity.vo.VideoListVo;
import com.weituotian.moudle.member.service.IPartitionService;
import com.weituotian.system.entity.enums.OpenState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ange on 2017/4/3.
 */
@RequestMapping("${adminPath}/video")
@Controller("AdminVideoController")
public class AdminVideoController extends BaseController {


    private final IAdminVideoService adminVideoService;

    private final IPartitionService partitionService;

    @Autowired
    public AdminVideoController(IAdminVideoService adminVideoService, IPartitionService partitionService) {
        this.adminVideoService = adminVideoService;
        this.partitionService = partitionService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        model.addAttribute("view_url", contextPath + adminPath + "/video/view");
        model.addAttribute("check_url", contextPath + adminPath + "/video/check");
        model.addAttribute("delete_url", contextPath + adminPath + "/video/delete");
    }

    @RequestMapping("/index")
    public String index(Integer page, Integer pagesize, Model model) {
        PageInfo<VideoListVo> pageInfo = new PageInfo<>(page, 10);

        //条件
        pageInfo.setCondition(getSearchMap());

        pageInfo.addSort("createTime", "asc");

        adminVideoService.findPage(pageInfo);

        model.addAttribute("partitions", partitionService.findAll());
        model.addAttribute("openStatus", OpenState.values());
        model.addAttribute("videoState", VideoState.values());
        model.addAttribute("pageName", "管理视频");
        model.addAttribute("pageinfo", pageInfo);
        return "admin/video/index";
    }

    @RequestMapping("/view")
    public String view(Integer videoId, Model model) throws ControllerException {
        try {
            String filename = adminVideoService.getVideoPath(videoId);
            String path = getVideoPath() + filename;
            model.addAttribute("videoSrc", path);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ControllerException(e.getMessage());
        }

        return "admin/video/view";
    }

    @RequestMapping(value = "/check", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object check(VideoState status, Integer videoId) {
        try {
            adminVideoService.check(videoId, status);
        } catch (Exception e) {
            e.printStackTrace();
            Result result = renderError("服务器发生了错误");
            if (e instanceof ServiceException) {
                result.setMsg(e.getMessage());
            }
            return result;
        }
        return renderSuccess("设置成功");
    }

    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
//            String realPath = request.getSession().getServletContext().getRealPath(videoPath);//在磁盘上的绝对路径
//            adminVideoService
//            roleService.deleteBatch(ids);
            //删除评论测试成功，多对多的标签未测试
            adminVideoService.delete(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("删除成功!");
    }
}
