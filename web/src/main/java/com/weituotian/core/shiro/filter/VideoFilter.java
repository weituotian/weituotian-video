package com.weituotian.core.shiro.filter;

import com.weituotian.core.result.Result;
import com.weituotian.core.utils.JsonUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * 拦截视频mp4,暂时无用
 * Created by ange on 2016/11/14.
 */
public class VideoFilter extends AccessControlFilter {

    public final static String VIDEO_KEY = "video_key";

    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        boolean flag = false;

        response.setContentType("video/mp4");

        Subject subject = getSubject(request, response);

        String token = request.getParameter("token");
        if (token.equals(subject.getSession().getAttribute(VIDEO_KEY))) {
            flag = true;
        }

        return flag;
    }


    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        //返回ajax
        Result result = new Result();
        result.setSuccess(false);
        result.setMsg("无权限");
        JsonUtils.out(response, result);
        return false;
    }

    private void renderError(ServletRequest request, ServletResponse response, String msg) throws IOException {
//        if (ShiroFilterUtils.isAjax(request)) {
        //返回ajax
        Result result = new Result();
        result.setSuccess(false);
        result.setMsg(msg);
        JsonUtils.out(response, result);
        /*} else {
            saveRequestAndRedirectToLogin(request, response);
        }*/
    }
}
