package com.weituotian.core.shiro.filter;

import com.weituotian.core.result.Result;
import com.weituotian.core.utils.JsonUtils;
import com.weituotian.system.entity.User;
import com.weituotian.system.entity.enums.UserType;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 自定义登录filter
 * Created by ange on 2016/11/14.
 */
public class SimpleAuthFilter extends AccessControlFilter {


    @Autowired
    private CookieRememberMeManager cookieRememberMeManager;

    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        Subject subject = getSubject(request, response);
        boolean flag = false;
        if (subject.isAuthenticated()) {
            //已经验证登录
            flag = true;

        } else {

            if (subject.isRemembered()) {
                //使用了记住我功能

                User user = (User) subject.getPrincipal();

                //会员自动登录
                if (user.getUserType() == UserType.MEMBER) {
                    UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginname(), user.getPassword().toCharArray());//md5加密密码
                    token.setRememberMe(true);
                    subject.login(token);
                    return true;
                }

                //提示验证超时
                /*
                try {
                    Session session = subject.getSession(false);
                    session.getStartTimestamp();
                } catch (ExpiredSessionException e) {
                    // 超时

                }
                */
            }
        }
        return flag;
    }


    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        if (ShiroFilterUtils.isAjax(request)) {
            //返回ajax
            Result result = new Result();
            result.setSuccess(false);
            result.setMsg("请登录后操作");
            JsonUtils.out(response, result);
        } else {
            saveRequestAndRedirectToLogin(request, response);
        }
        return false;
    }
}
