package com.weituotian.core.accept;

/**
 * sprimgmvc接受排序的请求
 * Created by ange on 2017/1/12.
 */
public class MutilSort {

    private String sortName;
    private String sortOrder;

    public MutilSort() {
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
