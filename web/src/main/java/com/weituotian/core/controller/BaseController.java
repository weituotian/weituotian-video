package com.weituotian.core.controller;

import com.weituotian.core.result.Result;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 基础控制器
 * Created by Administrator on 2016-09-19.
 */
public abstract class BaseController {
    //日志工具
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected HttpServletRequest request;

    @Value("${adminPath}")
    protected String adminPath;

    @Value("${memberPath}")
    protected String memberPath;

    @Value("${systemPath}")
    protected String systemPath;

    @Value("${frontPath}")
    protected String frontPath;


    //一些上传文件的路径
    @Value("${member.avater}")
    protected String avaterPath;

    @Value("${member.cover}")
    protected String coverPath;

    @Value("${member.video}")
    protected String videoPath;

    protected String getFullContextPath(){
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    protected String getAvatarPath() {
        return getFullContextPath() + avaterPath + "/";
    }

    protected String getCoverPath(){
        return getFullContextPath() + coverPath + "/";
    }

    protected String getVideoPath(){
        return getFullContextPath() + videoPath + "/";
    }

    /**
     * 获得当前url
     *
     * @return
     */
    protected String makeUrl() {
        return request.getRequestURL().toString() + "?" + request.getQueryString();
    }

    public BaseController() {

    }

    /**
     * ajax失败
     *
     * @param msg 失败的消息
     * @return {Object}
     */
    protected Result renderError(String msg) {
        Result result = new Result();
        result.setMsg(msg);
        return result;
    }

    /**
     * ajax成功
     *
     * @param msg 消息
     * @return {Object}
     */
    protected Result renderSuccess(String msg) {
        Result result = new Result();
        result.setSuccess(true);
        result.setMsg(msg);
        return result;
    }

    /**
     * 创建一个新的ajax返回对象
     *
     * @return
     */
    protected Result renderResult() {
        //其它功能待添加
        return new Result();
    }

    /**
     * 重定向到新页面显示信息
     */
    protected String showMessage() {
        return "redirect:/msg";
    }

    /**
     * 获得上下文路径
     *
     * @return
     */
    protected String getContextPath() {
        return request.getContextPath();
    }

    /**
     * 获得本次请求的所有参数,放到一个map里面
     *
     * @return
     */
    protected Map<String, Object> getParamMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        //列举参数名
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            //每一个参数名
            String paramName = (String) paramNames.nextElement();
            //值数组
            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length == 1) {
                //单一字符串
                String paramValue = paramValues[0];
                if (!paramValue.equals("")) {//判断不为空字符串
                    map.put(paramName, paramValue);
                }
            } else {
                //数组
                map.put(paramName, paramValues);
            }
        }
        return map;
    }

    /**
     * 过滤参数map,要求参数是以传入的字符串为开头的
     *
     * @return
     */
    protected Map<String, Object> getFilterMap(String startWith) {
        Map<String, Object> map = getParamMap();
        //iterator 遍历
        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String key = entry.getKey();
            if (!key.startsWith(startWith)) {
                it.remove();        //OK
            }
        }
        return map;
    }

    protected Map<String, Object> getSearchFilterMap(String startWith) {
        Map<String, Object> map = new HashMap<String, Object>();
        //列举参数名
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            //每一个参数名
            String paramName = (String) paramNames.nextElement();
            if (paramName.startsWith(startWith)) {
                String newParamName = StringUtils.substring(paramName, startWith.length());
                //值数组
                String[] paramValues = request.getParameterValues(paramName);
                if (paramValues.length == 1) {
                    //单一字符串
                    String paramValue = paramValues[0];
                    if (!paramValue.equals("")) {//判断不为空字符串
                        map.put(newParamName, paramValue);
                    }
                } else {
                    //数组
                    map.put(newParamName, paramValues);
                }
            }
        }
        return map;
    }

    /**
     * 将参数中所有的参数名为search_开头的都查询出来,放到一个map里面
     *
     * @return
     */
    protected Map<String, Object> getSearchMap() {
        return checkSearchConditionMap(getSearchFilterMap("search_"));
    }

    /**
     * 做一些操作,比如去除空格
     *
     * @param map
     * @return
     */
    protected Map<String, Object> checkSearchConditionMap(Map<String, Object> map) {
        //iterator 遍历
        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();

            String key = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof String[]) {

//                数组暂时不做处理
//                String[] strValues = (String[]) value;

            } else if (value instanceof String) {
                String strValue = (String) value;
                if (StringUtils.isBlank(strValue)) {//纯空格,去掉
                    it.remove();        //OK
                } else {
                    entry.setValue(strValue.trim());//去除多余空格
                }
            }
        }
        return map;
    }
}
