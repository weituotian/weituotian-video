package com.weituotian.core.controller;

/**
 * 用于过滤url参数的key的fielter
 * Created by ange on 2017/2/2.
 */
public interface Namefilter {

    boolean canFilter(String key, String value);

}
