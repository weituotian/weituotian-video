package com.weituotian.core.result;

/**
 * bootstrap table 的返回类
 * Created by ange on 2017/1/11.
 */
public class TableResult {
    private int total;
    private Object rows;

    public TableResult(int total, Object rows) {
        this.total = total;
        this.rows = rows;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Object getRows() {
        return rows;
    }

    public void setRows(Object rows) {
        this.rows = rows;
    }
}
