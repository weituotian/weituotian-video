package com.weituotian.core.result;

import java.util.List;

/**
 * jstree ndoe
 * Created by Administrator on 2017-01-21.
 */
public class JsTreeNode implements java.io.Serializable {

    private String id;

    private String pid;

    private String icon;

    private String text;

    private State state;

    private Integer seq;

    private boolean selected;

    private boolean opened;

    private boolean disabled;

    private Object children;

    public class State {
        public boolean opened = false;//是否开启
        public boolean disabled = false;//是否禁用
        public boolean selected = false;//是否checkbox选中
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Object getChildren() {
        return children;
    }

    public void setChildren(Object children) {
        this.children = children;
    }
}
