package com.weituotian.core.utils;

import com.weituotian.system.entity.User;
import org.apache.shiro.SecurityUtils;

/**
 * shiro utils
 * Created by ange on 2017/3/6.
 */
public class ShiroUtil {

    public static User getCurrentUser() {
        return (User) SecurityUtils.getSubject().getPrincipal();
    }

}
