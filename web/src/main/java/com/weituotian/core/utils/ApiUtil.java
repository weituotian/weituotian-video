package com.weituotian.core.utils;

import com.weituotian.moudle.api.entity.vo.AppUser;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.system.entity.User;

/**
 * Created by ange on 2017/3/30.
 */
public class ApiUtil {

    public static AppUser getAppUser(User user, String avatarPath) {
        //转换为appuser,手机端保存信息
        AppUser appUser = new AppUser();
        appUser.setId(user.getId());
        appUser.setEmail(user.getEmail());
        appUser.setLoginname(user.getLoginname());
        appUser.setName(user.getName());
        appUser.setPassword(user.getPassword());
        appUser.setUserType(user.getUserType());
        if (user instanceof Member) {
            String avatar = ((Member) user).getAvatar();
            appUser.setAvatar(avatarPath + avatar);
        }
        return appUser;
    }
}
