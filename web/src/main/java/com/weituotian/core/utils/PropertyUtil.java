package com.weituotian.core.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * Created by ange on //.
 */
public class PropertyUtil {

    public static Properties loadPropertyFile(String fileName) {
        if (null == fileName || fileName.equals("")) {
            throw new IllegalArgumentException("Properties file path can not be null: " + fileName);
        }

        InputStream inputStream = null;
        Properties properties = null;
        try {
            inputStream = PropertyUtil.class.getClassLoader().getResourceAsStream(fileName);
            properties = new Properties();
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Properties file not found: " + fileName);
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file can not be loading: " + fileName);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

}
