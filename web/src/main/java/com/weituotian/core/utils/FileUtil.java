package com.weituotian.core.utils;

/**
 * 文件工具
 * Created by ange on 2017/3/4.
 */
public class FileUtil {

    public static String getFileSubfix(String fileName) {
//        String name = targetFile.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static String generateNewFileName(String fileName) {
        String suffix = getFileSubfix(fileName);
        String uuid = UUIDUtils.getUUID();
        return uuid + "." + suffix;
    }

    public static String generateNewFileName(String fileName,String ext) {
        String uuid = UUIDUtils.getUUID();
        return uuid + "." + ext;
    }
}
