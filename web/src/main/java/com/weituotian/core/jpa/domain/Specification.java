package com.weituotian.core.jpa.domain;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by ange on 2017/1/30.
 */
public interface Specification<T> {

    enum SelectType {
        list, count
    }

    /**
     * 发生在构建查询list的query之前
     * 允许对root,query,cb做一些额外的操作
     *
     * @param root
     * @param query
     * @param cb
     */
    void beforeSelectList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb);

    void onOrder(List<Order> orders, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb);

    /**
     * Creates a WHERE clause for a query of the referenced entity in form of a {@link Predicate} for the given
     * 返回搜索条件
     * {@link Root} and {@link CriteriaQuery}.
     *
     * @param root
     * @param query
     * @return a {@link Predicate}, must not be {@literal null}.
     */
    Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb);
}
