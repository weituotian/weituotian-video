package com.weituotian.core.jpa.domain;

import javax.persistence.Query;

/**
 * jpql动态生成时的回调
 * Created by ange on 2017/3/8.
 */
public interface JPQLParamsSetter {

    void onSetParams(Query query);

}
