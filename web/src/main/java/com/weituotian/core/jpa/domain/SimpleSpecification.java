package com.weituotian.core.jpa.domain;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * 简单实现 Specification
 * Created by ange on 2017/2/5.
 */
public abstract class SimpleSpecification<T> implements Specification<T>{

    @Override
    public void onOrder(List<Order> orders, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

    }

    @Override
    public void beforeSelectList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

    }

}
