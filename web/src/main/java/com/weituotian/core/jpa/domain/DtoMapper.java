package com.weituotian.core.jpa.domain;

/**
 * dto转换成实体的mapper
 * S指的是源
 * D指的是目标
 * Created by ange on 2017/2/4.
 */
public interface DtoMapper<S,T> {

    T toDto(S s);

}
