package com.weituotian.core.jpa.domain;

import java.util.Collection;

/**
 * 简单实现
 * Created by ange on 2017/2/5.
 */
public abstract class SimpleDtoMapper<S, T> implements DtoMapper<S, T> {

    /*List<T> toDto(List<S> sources) {
        List<T> newList = new ArrayList<T>();
        for (S source : sources) {
            T target = toDto(source);
            newList.add(target);
        }
        return newList;
    }

    Set<T> toDto(Set<S> sources) {
        Set<T> newList = new HashSet<T>();
        for (S source : sources) {
            T target = toDto(source);
            newList.add(target);
        }
        return newList;
    }*/

    public Collection<T> toListDto(Collection<S> sources) {
        try {
            Collection<T> newList = sources.getClass().newInstance();
            for (S source : sources) {
                T target = toDto(source);
                newList.add(target);
            }
            return newList;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 默认转换方法
     *
     * @param targetClazz vo的class
     * @param sources     entity的collection
     * @param <S>         entity
     * @param <T>         vo
     * @return vo的collection
     */
/*    public static <S,T> Collection<T> defaultToListDto(Class<T> targetClazz, Collection<S> sources) {
        try {
            Collection<T> newList = sources.getClass().newInstance();
            //
            for (S source : sources) {
                T target = targetClazz.newInstance();
                DtoUtils.copyProperties(source, target);
                newList.add(target);
            }
            return newList;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }*/
}
