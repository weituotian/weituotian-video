package com.weituotian.system.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.exception.ControllerException;
import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.system.entity.Resource;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.service.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 资源控制器
 * Created by ange on 2017/2/3.
 */
@Controller("resourceController")
@RequestMapping("${systemPath}/resource")
public class ResourceController extends BaseController {

    private final IResourceService resourceService;


    @Autowired
    public ResourceController(IResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        model.addAttribute("list_url", contextPath + systemPath + "/resource/ajaxlist");

        model.addAttribute("edit_url", contextPath + systemPath + "/resource/edit");
        model.addAttribute("add_url", contextPath + systemPath + "/resource/add");
        model.addAttribute("delete_url", contextPath + systemPath + "/resource/delete");

        model.addAttribute("autocreate_url", contextPath + systemPath + "/resource/autocreate");
    }

    /**
     * 资源列表
     *
     * @param page 当前页数
     * @return
     */
    @RequestMapping(value = "/list")
    public String list(Integer page, Integer pagesize, Model model) {
        PageInfo<Resource> pageInfo = new PageInfo<>(page, 10);

        //条件
        pageInfo.setCondition(getSearchMap());

        //排序
        pageInfo.addSort("moudle", "desc");
        pageInfo.addSort("controller", "desc");
//        pageInfo.addSort("method", "desc");
        pageInfo.addSort("id", "desc");
        resourceService.findPage(pageInfo);

        //筛选模块和控制器
        model.addAttribute("moudles", resourceService.findMoudles());
        model.addAttribute("controllers", resourceService.findControllers());


        model.addAttribute("pageName", "资源列表");

        model.addAttribute("pageinfo", pageInfo);

        return "system/resource/index";
    }

    /**
     * 编辑页面
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Integer id, Model model) {
        Resource resource = resourceService.findById(id);

        model.addAttribute("resource", resource);
        model.addAttribute("openStatus", OpenState.values());

        //常用模块和控制器
        model.addAttribute("moudles", resourceService.findMoudles());
        model.addAttribute("controllers", resourceService.findControllers());

        model.addAttribute("form_url", this.getContextPath() + systemPath + "/resource/doedit");
        model.addAttribute("action", "edit");
        model.addAttribute("pageName", "资源编辑");

        return "system/resource/edit";
    }

    /**
     * 处理编辑
     *
     * @return
     */
    @RequestMapping(value = "/doedit", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doedit(Resource resource) {
        try {
            resourceService.update(resource);
        } catch (ServiceException e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("修改成功！");
    }

    /**
     * 增加页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        Resource resource = new Resource();
        resource.setStatus(OpenState.OPEN);//默认开启

        model.addAttribute("resource", resource);
        model.addAttribute("openStatus", OpenState.values());

        //常用模块和控制器
        model.addAttribute("moudles", resourceService.findMoudles());
        model.addAttribute("controllers", resourceService.findControllers());

        model.addAttribute("form_url", this.getContextPath() + systemPath + "/resource/doadd");
        model.addAttribute("action", "add");
        model.addAttribute("pageName", "资源新增");
        return "system/resource/edit";
    }

    /**
     * 处理增加
     *
     * @param resource
     * @return
     */
    @RequestMapping(value = "/doadd", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doadd(Resource resource) {
        try {
            resourceService.add(resource);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("增加成功!");
    }

    /**
     * 批量删除
     *
     * @param ids ids
     * @return
     */
    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
            resourceService.deleteBatch(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("删除成功!");
    }

    /**
     * 一键创建增删查改资源
     *
     * @return
     */
    @RequestMapping(value = "/autocreate")
    public Object autoCreate(String controller, String descript, String moudleName, String controllerName) throws ControllerException {
        try {

            Resource resource = resourceService.createDefaultResource(moudleName, controllerName);
            resource.setUrl("/" + controller + "/list");
            resource.setName(descript + "列表");
            resourceService.add(resource);

            Resource resource1 = resourceService.createDefaultResource(moudleName, controllerName);
            resource1.setUrl("/" + controller + "/add");
            resource1.setName(descript + "新建");
            resourceService.add(resource1);

            Resource resource2 = resourceService.createDefaultResource(moudleName, controllerName);
            resource2.setUrl("/" + controller + "/doadd");
            resource2.setName(descript + "处理新建");
            resourceService.add(resource2);

            Resource resource3 = resourceService.createDefaultResource(moudleName, controllerName);
            resource3.setUrl("/" + controller + "/edit");
            resource3.setName(descript + "编辑");
            resourceService.add(resource3);

            Resource resource4 = resourceService.createDefaultResource(moudleName, controllerName);
            resource4.setUrl("/" + controller + "/doedit");
            resource4.setName(descript + "处理编辑");
            resourceService.add(resource4);

            Resource resource5 = resourceService.createDefaultResource(moudleName, controllerName);
            resource5.setUrl("/" + controller + "/delete");
            resource5.setName(descript + "删除");
            resourceService.add(resource5);

//            resourceService.autoCreateCRUD(controller, descript, moudleName, controllerName);
        } catch (ServiceException e) {
            e.printStackTrace();
            throw new ControllerException(e.getMessage());//交给spring分发到错误页面
            //return renderError(e.getMessage());
        }
        return "redirect:list";
        //return renderSuccess("一键创建成功!");
    }
}
