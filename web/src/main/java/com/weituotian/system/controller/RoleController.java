package com.weituotian.system.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.exception.ServiceException;
import com.weituotian.core.result.Result;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.system.entity.Role;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.entity.vo.ResourceVo;
import com.weituotian.system.service.IResourceService;
import com.weituotian.system.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 角色控制器
 * Created by Administrator on 2016-09-26.
 */
@Controller("roleController")
@RequestMapping("${systemPath}/role")
public class RoleController extends BaseController {

    private final IRoleService roleService;

    private final IResourceService resourceService;

    @Autowired
    public RoleController(IRoleService roleService, IResourceService resourceService) {
        this.roleService = roleService;
        this.resourceService = resourceService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        model.addAttribute("edit_url", contextPath + systemPath + "/role/edit");
        model.addAttribute("add_url", contextPath + systemPath + "/role/add");
        model.addAttribute("delete_url", contextPath + systemPath + "/role/delete");

        model.addAttribute("grant_url", contextPath + systemPath + "/role/grant");
        model.addAttribute("dogrant_url", contextPath + systemPath + "/role/dogrant");
    }

    /**
     * 角色列表
     *
     * @param page 当前页数
     * @return
     */
    @RequestMapping(value = "/list")
    public String list(Integer page, Integer pagesize, Model model) {
        PageInfo<Role> pageInfo = new PageInfo<>(page, 4);

        pageInfo.setCondition(getSearchMap());

        roleService.findPage(pageInfo);


        model.addAttribute("pageinfo", pageInfo);
        model.addAttribute("pageName", "角色列表");
        return "system/role/index";
    }

    /**
     * 编辑页面
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Integer id, Model model) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        model.addAttribute("form_url", this.getContextPath() + systemPath + "/role/doedit");
        model.addAttribute("action", "edit");

        model.addAttribute("pageName", "角色编辑");
//        model.addAttribute("openStatus", roleService.getOpenStatus());
        model.addAttribute("openStatus", OpenState.values());
        return "system/role/edit";
    }

    /**
     * 处理编辑
     *
     * @return
     */
    @RequestMapping(value = "/doedit", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doedit(Role role) {
        try {
            //更新用户,封装到了一个事务中
            roleService.update(role);
        } catch (ServiceException e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("修改成功！");
    }

    /**
     * 增加页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        Role role = new Role();
        role.setStatus(OpenState.OPEN);//默认开启
        role.setSeq(1);

        model.addAttribute("role", role);
        model.addAttribute("form_url", this.getContextPath() + systemPath + "/role/doadd");
        model.addAttribute("action", "add");

        model.addAttribute("pageName", "角色新增");
        model.addAttribute("openStatus", roleService.getOpenStatus());
        return "system/role/edit";
    }

    /**
     * 处理增加
     *
     * @param role
     * @return
     */
    @RequestMapping(value = "/doadd", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doadd(Role role) {
        try {
            roleService.add(role);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError(e.getMessage());
        }
        return renderSuccess("增加成功!");
    }

    /**
     * 角色分配资源,返回ajax列表
     *
     * @return
     */
    @RequestMapping(value = "/grant", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object grant(Integer id, Model model) {
        /*List<Resource> all = resourceService.getAllResourceList();*/

        //构造资源树
        /*List<Node<Resource>> resTree = resourceService.findALLTree(all);//所有的资源列表
        List<Integer> resourceIds = resourceService.findResourceIdListByRoleId(id);
        if (resourceIds.size() > 0) {
            for (Node<Resource> node : resTree) {
                if (resourceIds.contains(node.getItem().getId())) {
                    node.setChecked(true);//设置选中
                }
            }
        }*/
        Result result = new Result();
        result.setMsg("获取成功");
        List<ResourceVo> resourceVos = resourceService.findResourceVoAllByRoleId(id);
//        model.addAttribute("resVos", resourceVos);
        result.setObj(resourceVos);
        result.setSuccess(true);

        /*List<JsTreeNode> res = resourceService.getAllResoureceJsTreeByRole(id);
//        model.addAttribute("list", JSONObject.toJSONString(res));
        for (JsTreeNode jsTreeNode : res) {
            if (jsTreeNode.isSelected()) {
                JsTreeNode.State state = jsTreeNode.new State();
                state.selected = true;
                jsTreeNode.setState(state);
            }
        }
        result.setObj(res);
        result.setSuccess(true);*/

        //角色
//        Role role = roleService.findById(id);

//        model.addAttribute("role", role);
        /*model.addAttribute("resTree", resTree);*/
//        return "role/grant";
        return result;
    }


    /**
     * 处理角色分配资源
     *
     * @return
     */
    @RequestMapping(value = "/dogrant", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object dogrant(Integer roleId, @RequestParam(value = "resIds[]", required = false) Integer[] resIds) {
        try {
            roleService.grant(roleId, resIds);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("分配失败!");
        }
        return renderSuccess("分配成功!");
    }

    /**
     * 批量删除操作
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
            roleService.deleteBatch(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("删除失败!");
        }
        return renderSuccess("删除成功!");
    }

}
