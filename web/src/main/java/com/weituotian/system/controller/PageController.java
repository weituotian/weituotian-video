package com.weituotian.system.controller;

import com.weituotian.core.controller.BaseController;
import com.weituotian.core.utils.ShiroUtil;
import com.weituotian.moudle.member.entity.Member;
import com.weituotian.system.entity.User;
import com.weituotian.system.service.IWelcomeService;
import com.weituotian.system.shiro.ShiroDbRealm;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 首页控制器
 * Created by Administrator on 2016-11-16.
 */
@Controller
@RequestMapping(value = "${systemPath}")
public class PageController extends BaseController {

    final ShiroDbRealm shiroDbRealm;

    private final IWelcomeService welcomeService;

    @Autowired
    public PageController(ShiroDbRealm shiroDbRealm, IWelcomeService welcomeService) {
        this.shiroDbRealm = shiroDbRealm;
        this.welcomeService = welcomeService;
    }

    @ModelAttribute
    public void initUrl(Model model) {
        String contextPath = this.getContextPath();
        if (SecurityUtils.getSubject().getPrincipal() instanceof Member) {
            //以会员的身份登录
            model.addAttribute("welcome_url", contextPath + systemPath + "/welcome_member");

        } else {
            //以管理员的身份登录
            model.addAttribute("welcome_url", contextPath + systemPath + "/welcome");
        }
        model.addAttribute("menu_url", contextPath + systemPath + "/menu/getmenu");
        model.addAttribute("logout_url", contextPath + "/logout");

        model.addAttribute("video_manager_url", contextPath + adminPath + "/video/index");
        model.addAttribute("member_manager_url", contextPath + adminPath + "/member/index");
        model.addAttribute("member_my_video_url", contextPath + memberPath + "/video/index");
    }

    /**
     * 根
     */
    @RequestMapping("/")
    public String tindex() {
        return "system/index";
    }

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index() {
        return "system/index/index";
    }

    /**
     * 管理员的欢迎页面
     *
     * @return
     */
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute("newlyMembers", welcomeService.getNewlyMemberCount());
        model.addAttribute("newlyVideos", welcomeService.getNewlyVideoCount());
        model.addAttribute("pageName", "欢迎");

        return "system/index/welcome";
    }

    /**
     * 会员的欢迎页面
     *
     * @return
     */
    @RequestMapping(value = "/welcome_member", method = RequestMethod.GET)
    public String welcome_member(Model model) {
        User currentUser = ShiroUtil.getCurrentUser();
        Integer userId = currentUser.getId();
        model.addAttribute("member", ((Member) currentUser));
        model.addAttribute("videosCount", welcomeService.getMemberVideosCount(userId));
        model.addAttribute("commentsCount", welcomeService.getMemberCommentsCount(userId));
        model.addAttribute("pageName", "欢迎");
        return "system/index/welcome_member";
    }

    /**
     * 显示信息的页面
     *
     * @return
     */
    @RequestMapping(value = "/msg", method = RequestMethod.GET)
    public String msg(@ModelAttribute("msg") String msg, Model model) {
        model.addAttribute("msg", msg);
        return "/msg";
    }
}
