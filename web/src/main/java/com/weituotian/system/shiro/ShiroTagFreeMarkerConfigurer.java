package com.weituotian.system.shiro;

import com.jagregory.shiro.freemarker.ShiroTags;
import freemarker.template.TemplateException;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;

/**
 * 配置freemarker使用shiro的标签
 * Created by ZXF-PC1 on 2015/6/18.
 */
public class ShiroTagFreeMarkerConfigurer extends FreeMarkerConfigurer {
    @Override
    public void afterPropertiesSet() throws IOException, TemplateException {
        super.afterPropertiesSet();
        //freemarker的shiro标签
        this.getConfiguration().setSharedVariable("shiro", new ShiroTags());
    }
}
