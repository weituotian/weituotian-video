package com.weituotian.system.dao;

import com.weituotian.system.entity.User;

public interface IUserDao extends IBaseDao<User, Integer> {

    /**
     * 通过登录名找到用户
     * @param loginName 登录名
     * @return
     */
    User findByLoginName(String loginName);

    /**
     * 通过email找到用户
     * @param email
     * @return
     */
    User findByEmail(String email);

    /**
     * 通过name找到用户
     * @param name
     * @return
     */
    User findByName(String name);

    /**
     * 登录的时候通过登录名,名,邮箱找到用户
     * @param name
     * @return
     */
    User loginFindBy(String name);
}
