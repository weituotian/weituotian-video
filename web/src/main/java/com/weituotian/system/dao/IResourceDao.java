package com.weituotian.system.dao;

import com.weituotian.system.entity.Resource;

import java.util.List;
import java.util.Set;

public interface IResourceDao extends IBaseDao<Resource, Integer> {

    /**
     * 通过用户id获得该用户拥有的所有资源
     * @param userId
     * @return
     */
    List<Resource> findResourcesByUserId(Integer userId);

    /**
     * 根据角色id查询角色所拥有的资源列表
     * @param userId
     * @return
     */
    List<Resource> findResourcesByRoleId(Integer userId);

    /**
     * 从资源实体列表中提取出各个资源的url,组成Set集合
     *
     * @param resources 资源list
     * @return
     */
    Set<String> getResourcesUrlSet(List<Resource> resources);

    /**
     * 找到资源表中定义的所有模块
     * @return
     */
    List<String> findMoudles();

    /**
     * 找到资源表中定义的所有控制器
     * @return
     */
    List<String> findControllers();

    /**
     * 找到资源表中属于指定模块的所有控制器名称
     *
     * @return
     */
    List<String> findControllersByMoudleId(String moudle);
}
