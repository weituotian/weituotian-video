package com.weituotian.system.dao;

import com.weituotian.core.result.Option;
import com.weituotian.system.entity.Role;

import java.util.List;
import java.util.Set;

public interface IRoleDao extends IBaseDao<Role, Integer> {

    /**
     * 从角色实体列表中提取出各个角色的名字,组成Set集合
     *
     * @param roles 角色list
     * @return
     */
    Set<String> getRoleNameSet(List<Role> roles);

    /**
     * 根据用户id,查询该用户的所有角色
     * @param userId 用户id
     * @return
     */
    List<Role> findRolesByUserId(Integer userId);

}
