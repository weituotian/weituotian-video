package com.weituotian.system.dao.mapper;

import com.weituotian.core.jpa.domain.SimpleDtoMapper;
import com.weituotian.core.utils.DtoUtils;
import com.weituotian.system.entity.Menu;
import com.weituotian.system.entity.vo.MenuVo;
import org.springframework.stereotype.Component;

/**
 * 定义menu和menuVo之间的转换
 * Created by ange on 2017/2/4.
 */
@Component
public class MenuVoMapper extends SimpleDtoMapper<Menu, MenuVo> {

    @Override
    public MenuVo toDto(Menu menu) {
        MenuVo menuVo = new MenuVo();
        DtoUtils.copyProperties(menu, menuVo);
        if (menu.getResource() != null) {
            menuVo.setHasResource(true);
            menuVo.setResourceName(menu.getResource().getName());
            menuVo.setResourceUrl(menu.getResource().getUrl());
        }
        if (menu.getPid() == null) {
            menuVo.setPid(-1);//-1代表js页面的根节点
        }
        return menuVo;
    }

}
