package com.weituotian.system.dao;

import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.List;

public interface IBaseDao<T, PK extends Serializable> {

    String DELETE_ALL_QUERY_STRING = "delete from %s x";

    void setEntityClass(Class<T> entityClass);

    /**
     * 获取hibernate session对象
     *
     * @return
     */
    Session getSession();

    Session openSession();

    /**
     * 创建一个criteria
     *
     * @return
     */
    CriteriaBuilder getCriteriaBuilder();

    void flush();

    PK save(T entity);

    void persist(T entity);

    T merge(T entity);

    void saveOrUpdate(T entity);

    void update(T entity);

    T findOne(PK id);

    List<T> findAll();

    /**
     * 通过id数组找到实体
     *
     * @param ids       id数组
     * @return list
     */
    List<T> findIds(PK[] ids);

    void delete(PK id);

    void deleteBatch(PK[] ids);

    void delete(T entity);

    /**
     * 分页
     * 返回的结果都存放在了pageInfo里面
     *
     * @param spec
     * @param pageInfo
     */
    List<T> findPage(Specification<T> spec, PageInfo<?> pageInfo);

}
