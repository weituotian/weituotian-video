package com.weituotian.system.dao.mapper;

import com.weituotian.core.jpa.domain.SimpleDtoMapper;
import com.weituotian.core.result.JsTreeNode;
import com.weituotian.system.entity.Menu;

/**
 * Created by ange on 2017/2/5.
 */
public class Menu2JsTreeNodeMapper extends SimpleDtoMapper<Menu,JsTreeNode> {

    @Override
    public JsTreeNode toDto(Menu menu) {
        return toJsTreeNode(menu);
    }

    public static JsTreeNode toJsTreeNode(Menu menu) {
        JsTreeNode node = new JsTreeNode();
        node.setId("" + menu.getId());
        node.setPid("" + menu.getPid());
        node.setText(menu.getText());
        node.setIcon(menu.getIcon());
        return node;
    }
}
