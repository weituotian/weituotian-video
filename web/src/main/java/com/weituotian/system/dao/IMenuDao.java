package com.weituotian.system.dao;

import com.weituotian.core.result.JsTreeNode;
import com.weituotian.system.entity.Menu;

import java.util.List;

public interface IMenuDao extends IBaseDao<Menu, Integer> {

    List<Menu> findAllMenu();

    List<JsTreeNode> findJsTreeNodeByPid(Integer pid);

    List<Menu> findRootMenu();
}
