package com.weituotian.system.entity;

import com.weituotian.core.convert.EnumConverter;
import com.weituotian.system.entity.enums.OpenState;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ange on 2017/1/28.
 */
@Entity
@Table(name = "resource")
public class Resource implements Serializable {
    private Integer id;
    private String name;
    private String moudle;
    private String controller;
    private String method;
    private String url;
    private OpenState status;
    private List<Role> roles;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "paymentableGenerator")
    @GenericGenerator(name = "paymentableGenerator", strategy = "native")
    //@GenericGenerator是hibernate独有的
    //以下是jpa独有的
    /*@GeneratedValue(strategy = GenerationType.TABLE, generator = "tablegen2")
    @TableGenerator(name = "tablegen2", table = "myidtable", pkColumnName = "pkcolumn1", valueColumnName = "valuecolumn1", pkColumnValue = "pkcolumnvalue2", allocationSize = 1)*/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "url", nullable = false, length = 100)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Basic
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
//    @Convert(converter = EnumConverter.class)
    public OpenState getStatus() {
        return status;
    }

    public void setStatus(OpenState status) {
        this.status = status;
    }

    @Basic
    @Column(name = "moudle", nullable = false, length = 40)
    public String getMoudle() {
        return moudle;
    }

    public void setMoudle(String moudle) {
        this.moudle = moudle;
    }

    @Basic
    @Column(name = "controller", nullable = false, length = 40)
    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    @Basic
    @Column(name = "method", nullable = true, length = 40)
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Resource resource = (Resource) o;

        if (id != resource.id) return false;
        if (name != null ? !name.equals(resource.name) : resource.name != null) return false;
        if (url != null ? !url.equals(resource.url) : resource.url != null) return false;
        if (status != null ? !status.equals(resource.status) : resource.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @ManyToMany(mappedBy = "resources")
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

}
