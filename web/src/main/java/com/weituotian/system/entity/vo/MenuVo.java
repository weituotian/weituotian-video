package com.weituotian.system.entity.vo;

import com.weituotian.system.entity.enums.OpenState;

/**
 * 菜单vo
 * Created by ange on 2017/2/3.
 */
public class MenuVo {
    private int id;
    private String text;
    private OpenState status;
    private Short seq;
    private String icon;
    private Integer pid;
    private boolean hasResource = false;
    private String resourceName = "";
    private String resourceUrl = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public OpenState getStatus() {
        return status;
    }

    public void setStatus(OpenState status) {
        this.status = status;
    }

    public Short getSeq() {
        return seq;
    }

    public void setSeq(Short seq) {
        this.seq = seq;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public boolean isHasResource() {
        return hasResource;
    }

    public void setHasResource(boolean hasResource) {
        this.hasResource = hasResource;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }
}
