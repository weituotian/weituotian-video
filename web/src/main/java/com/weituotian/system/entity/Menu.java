package com.weituotian.system.entity;

import com.weituotian.system.entity.enums.OpenState;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 菜单
 * Created by ange on 2017/1/28.
 */
@Entity
@Table(name = "menu")
public class Menu implements Serializable {
    private int id;
    private String text;
    private OpenState status;
    private Short seq;
    private String icon;
    private List<Menu> children;
    private Menu parent;
    private Integer pid;
    private Resource resource;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "text", nullable = true, length = 40)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "status", nullable = true)
    @Enumerated(EnumType.STRING)
    public OpenState getStatus() {
        return status;
    }

    public void setStatus(OpenState status) {
        this.status = status;
    }


    @Basic
    @Column(name = "seq", nullable = true)
    public Short getSeq() {
        return seq;
    }

    public void setSeq(Short seq) {
        this.seq = seq;
    }

    @Basic
    @Column(name = "icon", nullable = true, length = 100)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", status=" + status +
                ", seq=" + seq +
                ", icon='" + icon + '\'' +
                ", children=" + children +
                ", pid=" + pid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        if (id != menu.id) return false;
        if (text != null ? !text.equals(menu.text) : menu.text != null) return false;
        if (status != null ? !status.equals(menu.status) : menu.status != null) return false;
        if (seq != null ? !seq.equals(menu.seq) : menu.seq != null) return false;
        if (icon != null ? !icon.equals(menu.icon) : menu.icon != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (seq != null ? seq.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "parent")
    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }

    @ManyToOne
    @JoinColumn(name = "pid", referencedColumnName = "id",foreignKey = @ForeignKey(name="fk_menu_parent"))
    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    @Basic
    @Column(name = "pid", updatable = false, insertable = false)
    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    @ManyToOne
    @JoinColumn(name = "resource_id", referencedColumnName = "id")
    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
