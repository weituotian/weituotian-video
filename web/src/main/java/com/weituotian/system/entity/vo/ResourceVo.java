package com.weituotian.system.entity.vo;

import com.weituotian.system.entity.enums.OpenState;

/**
 * 资源vo
 * Created by ange on 2017/2/2.
 */
public class ResourceVo {
    private int id;
    private String name;
    private String moudle;
    private String controller;
    private String method;
    private String url;
    private OpenState status;
    private String checked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMoudle() {
        return moudle;
    }

    public void setMoudle(String moudle) {
        this.moudle = moudle;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public OpenState getStatus() {
        return status;
    }

    public void setStatus(OpenState status) {
        this.status = status;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
