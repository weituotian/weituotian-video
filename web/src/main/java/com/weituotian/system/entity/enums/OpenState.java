package com.weituotian.system.entity.enums;

/**
 * 表示开关状态的enum
 * Created by ange on 2017/2/2.
 */
public enum OpenState {
    OPEN("OPEN", "开启"), CLOSE("CLOSE", "关闭");

    private String value;

    private String title;

    public String getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    OpenState(String value, String title) {
        this.value = value;
        this.title = title;
    }

    @Override
    public String toString() {
        return "OpenState{" +
                "value='" + value + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    public static OpenState getEnum(String value) {
        for (OpenState v : values())
            if (v.getValue().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException();
    }
}
