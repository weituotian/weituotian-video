package com.weituotian.system.entity.enums;

/**
 * 用户类型
 * Created by ange on 2017/4/13.
 */
public enum UserType {
    TOPADMIN("TOPADMIN", "最高管理员"),
    ADMIN("ADMIN", "管理员"), MEMBER("MEMBER", "会员");

    private String value;

    private String title;

    public String getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    UserType(String value, String title) {
        this.value = value;
        this.title = title;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "value=" + value +
                ", title='" + title + '\'' +
                '}';
    }
}
