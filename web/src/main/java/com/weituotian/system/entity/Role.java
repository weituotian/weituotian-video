package com.weituotian.system.entity;

import com.weituotian.system.entity.enums.OpenState;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ange on 2017/1/28.
 */
@Entity
@Table(name = "role")
public class Role implements Serializable {
    private int id;
    private String name;
    private int seq;
    private OpenState status;
    private List<User> users;
    private List<Resource> resources;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "seq", nullable = false)
    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    @Basic
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    public OpenState getStatus() {
        return status;
    }

    public void setStatus(OpenState status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != role.id) return false;
        if (seq != role.seq) return false;
        if (!name.equals(role.name)) return false;
        return status == role.status;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + seq;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @ManyToMany()
    //mappedBy = "roles"
    @JoinTable(name = "user_role", schema = "rbac_hibernate", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false))
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @ManyToMany
    @JoinTable(name = "role_resource", schema = "rbac_hibernate", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "resource_id", referencedColumnName = "id", nullable = false))
    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
}
