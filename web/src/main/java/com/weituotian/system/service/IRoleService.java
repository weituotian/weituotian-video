package com.weituotian.system.service;

import com.weituotian.core.result.Option;
import com.weituotian.system.entity.Role;

import java.util.List;
import java.util.Map;

/**
 *
 * Created by ange on 2016/9/14.
 */
public interface IRoleService extends IAdminBaseService<Role,Integer> {

    /**
     * 安全批量删除,删除角色的同时,用户角色关联表的数据都会被删除
     * @param ids
     */
    void deleteByIdsSafe(Integer[] ids);

    /**
     * 获得所有的角色列表[暂时不用]
     * 传入的用户id拥有的角色会被选择
     * @param userId 用户id
     * @return
     */
    List<Option> getAllRoleListByUser(Integer userId);

    /**
     * 根据用户查询id查询该用户所拥有的角色id列表
     *
     * @param userId 用户id
     * @return
     */
    List<Integer> findRoleIdListByUserId(Integer userId);

    /**
     * 根据用户查询id,为所有的role生成一个对应html的option的对象
     * 如果该用户有该角色的话,option会被设置选中
     * @param userId 用户id
     * @return
     */
    List<Option> findAllRolesOptionsByUserId(Integer userId);

    /**
     * 为角色分配资源
     * @param roleId 角色id
     * @param resIds 资源id数组
     */
    void grant(Integer roleId, Integer[] resIds);

    /**
     * 获取角色状态map
     * @return
     */
    Map<Integer, String> getOpenStatus();
}
