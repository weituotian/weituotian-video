package com.weituotian.system.service.impl;

import com.weituotian.core.jpa.domain.SimpleSpecification;
import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.dao.IResourceDao;
import com.weituotian.system.entity.Resource;
import com.weituotian.system.entity.enums.OpenState;
import com.weituotian.system.entity.vo.ResourceVo;
import com.weituotian.system.service.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 资源服务
 * Created by Administrator on 2016-09-30.
 */
@Service("resourceService")
@Transactional
public class ResourceService extends AdminBaseService<Resource, Integer> implements IResourceService {

    private final IResourceDao resourceDao;

    @Autowired
    public ResourceService(IResourceDao resourceDao) {
        this.resourceDao = resourceDao;
    }

    @Override
    public IBaseDao<Resource, Integer> getBaseDao() {
        return resourceDao;
    }

    @Override
    public Specification<Resource> pageSpecification(PageInfo<?> pageInfo) {
        return new SimpleSpecification<Resource>() {

            @Override
            public Predicate toPredicate(Root<Resource> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<Predicate>();

                for (Map.Entry<String, Object> entry : pageInfo.getCondition().entrySet()) {
                    String name = entry.getKey();
                    String value = (String) entry.getValue();
                    if (name.equals("name")) {
                        predicates.add(cb.like(root.get("name"), "%" + value + "%"));
                    } else if (name.equals("moudle")) {
                        predicates.add(cb.equal(root.get("moudle"), value));
                    } else if (name.equals("controller")) {
                        predicates.add(cb.equal(root.get("controller"), value));
                    } else {
                        predicates.add(cb.equal(root.get(name), value));
                    }
                }

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

    }

    @Override
    public List<String> findMoudles() {
        return resourceDao.findMoudles();
    }

    @Override
    public List<String> findControllers() {
        return resourceDao.findControllers();
    }

    @Override
    public List<String> findControllersByMoudleId(String moudle) {
        return resourceDao.findControllersByMoudleId(moudle);
    }

    @Override
    public List<ResourceVo> findResourceVoAllByRoleId(Integer roleId) {
        List<Resource> roleRes = resourceDao.findResourcesByRoleId(roleId);
        List<Resource> allRes = resourceDao.findAll();
        List<ResourceVo> resVos = new ArrayList<>();
        for (Resource res : allRes) {
            ResourceVo resVo = new ResourceVo();
            resVo.setId(res.getId());
            resVo.setMoudle(res.getMoudle());
            resVo.setController(res.getController());
            resVo.setMethod(res.getMethod());
            resVo.setName(res.getName());
            resVo.setUrl(res.getUrl());
            resVo.setStatus(res.getStatus());

            resVo.setChecked("");
            if (roleRes.contains(res)) {
                resVo.setChecked("checked");
            }
            resVos.add(resVo);
        }
        return resVos;
    }

    @Override
    public List<Resource> findResourcesByRoleId(Integer roleId) {
        return resourceDao.findResourcesByRoleId(roleId);
    }

    @Override
    public void AddContextPath(List<Resource> res, String contextPath) {
        for (Resource resource : res) {
            resource.setUrl(contextPath + resource.getUrl());
        }
    }

    @Override
    public Resource createDefaultResource(String moudleName, String controllerName) {
        Resource resource = new Resource();
        resource.setStatus(OpenState.OPEN);

        resource.setMoudle(moudleName);
        resource.setController(controllerName);
        resource.setMethod("");
        return resource;
    }

    @Override
    public void autoCreateCRUD(String controller, String descript, String moudleName, String controllerName) {

        Resource resource = createDefaultResource(moudleName, controllerName);
        resource.setUrl("/" + controller + "/list");
        resource.setName(descript + "列表");
        resourceDao.merge(resource);

        Resource resource1 = createDefaultResource(moudleName, controllerName);
        resource1.setUrl("/" + controller + "/add");
        resource1.setName(descript + "新建");
        resourceDao.merge(resource1);


        Resource resource2 = createDefaultResource(moudleName, controllerName);
        resource2.setUrl("/" + controller + "/doadd");
        resource2.setName(descript + "处理新建");
        resourceDao.merge(resource2);

        Resource resource3 = createDefaultResource(moudleName, controllerName);
        resource3.setUrl("/" + controller + "/edit");
        resource3.setName(descript + "编辑");
        resourceDao.merge(resource3);

        Resource resource4 = createDefaultResource(moudleName, controllerName);
        resource4.setUrl("/" + controller + "/doedit");
        resource4.setName(descript + "处理编辑");
        resourceDao.merge(resource4);

        Resource resource5 = createDefaultResource(moudleName, controllerName);
        resource5.setUrl("/" + controller + "/delete");
        resource5.setName(descript + "删除");
        resourceDao.merge(resource5);

    }


}
