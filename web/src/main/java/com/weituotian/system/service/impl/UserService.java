package com.weituotian.system.service.impl;

import com.weituotian.core.jpa.domain.SimpleSpecification;
import com.weituotian.core.jpa.domain.Specification;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.system.dao.IBaseDao;
import com.weituotian.system.dao.IRoleDao;
import com.weituotian.system.dao.IUserDao;
import com.weituotian.system.entity.Role;
import com.weituotian.system.entity.User;
import com.weituotian.system.service.IUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户服务
 * Created by ange on 2016/9/14.
 */
@Service("userService")
@Transactional
public class UserService extends AdminBaseService<User, Integer> implements IUserService {


    private final IUserDao userDao;

    private final IRoleDao roleDao;

    @Autowired
    public UserService(IUserDao userDao, IRoleDao roleDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
    }

    @Override
    public IBaseDao<User, Integer> getBaseDao() {
        return userDao;
    }

    @Override
    public Specification<User> pageSpecification(PageInfo<?> pageInfo) {
        return new SimpleSpecification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<Predicate>();

                //拼接查询条件
                for (Map.Entry<String, Object> entry : pageInfo.getCondition().entrySet()) {
                    String name = entry.getKey();
                    String value = (String) entry.getValue();
                    if (name.equals("name")) {
                        predicates.add(cb.like(root.get("loginname"), "%" + value + "%"));
                    } else {
                        predicates.add(cb.equal(root.get(name), value));
                    }
                }

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }

    /*以下方法特有*/
    @Override
    public Map<Integer, String> getOpenStatus() {
        Map<Integer, String> radio = new HashMap<Integer, String>();
        radio.put(0, "关");
        radio.put(1, "开");
        return radio;
    }

    /**
     * 根据用户名查询用户
     *
     * @param username 用户名
     * @return 用户bean
     */
    @Override
    public User findByLoginName(String username) {
        return userDao.findByLoginName(username);
    }

    @Override
    public User findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    public void updateRoles(Integer userId, Integer[] roleIds) {
        User user = userDao.findOne(userId);
        List<Role> roles = user.getRoles();
        roles.clear(); //先删除该用户的所有角色
        roles.addAll(roleDao.findIds(roleIds));
    }

    @Override
    public void updateUser(User user) {
        User oldUser = userDao.findOne(user.getId());
        if (user.getPassword() == null || StringUtils.isBlank(user.getPassword())) {
            //没有提交修改密码,保持不变
            user.setPassword(oldUser.getPassword());
        } else {
            user.setPassword(DigestUtils.md5Hex(user.getPassword()));//md5密码加密
        }
        this.update(user);
    }

}
