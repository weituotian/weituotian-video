package com.weituotian.system.service;

import com.weituotian.core.jpa.domain.DtoMapper;
import com.weituotian.core.jpa.domain.SimpleDtoMapper;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.moudle.common.service.IBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 基础服务
 * Created by ange on 2016/9/26.
 */
public interface IAdminBaseService<T, PK extends Serializable> extends IBaseService<T, PK>{

    /**
     * 获得有分页的列表
     *
     * @param pageInfo 分页信息类
     */
    void findPage(PageInfo<T> pageInfo);

    /**
     * 获得分页 ,vo形式
     * @param voClazz
     * @param pageInfo
     * @param mapper
     * @param <V>
     */
    <V> void findPageVo(Class<V> voClazz, PageInfo<V> pageInfo, SimpleDtoMapper<T, V> mapper);

}
