package com.weituotian.system.service;

import com.weituotian.system.entity.User;

import java.util.Map;

/**
 * 用户服务接口
 * Created by ange on 2016/9/14.
 */
public interface IUserService extends IAdminBaseService<User, Integer> {

    /**
     * 增加/更新用户的角色
     *
     * @param userId  用户id
     * @param roleIds 角色id数组
     */
    void updateRoles(Integer userId, Integer[] roleIds);


    /**
     * 更新用户
     *
     * @param user    用户bean
     */
    void updateUser(User user);

    /**
     * 获取状态map
     *
     * @return 状态map
     */
    Map<Integer, String> getOpenStatus();

    /**
     * 根据用户名查询用户
     *
     * @param username 用户名
     * @return 用户bean
     */
    User findByLoginName(String username);

    User findByName(String text);
}
