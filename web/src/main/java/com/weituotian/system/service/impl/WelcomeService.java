package com.weituotian.system.service.impl;

import com.weituotian.moudle.member.dao.IMemberDao;
import com.weituotian.moudle.member.dao.IVideoDao;
import com.weituotian.system.service.IWelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 后台欢迎页面
 * Created by ange on 2017/4/11.
 */
@Service
@Transactional
public class WelcomeService implements IWelcomeService {

    private final IVideoDao videoDao;

    private final IMemberDao memberDao;

    @Autowired
    public WelcomeService(IVideoDao videoDao, IMemberDao memberDao) {
        this.videoDao = videoDao;
        this.memberDao = memberDao;
    }

    public Integer getMemberVideosCount(Integer memberId){
        return videoDao.getMemberVideosCount(memberId);
    }

    public Integer getMemberCommentsCount(Integer memberId){
        return videoDao.getMemberVideoCommentsCount(memberId);
    }

    public Integer getNewlyMemberCount(){
        return memberDao.getNewlyMemberCount();
    }

    public Integer getNewlyVideoCount(){
        return videoDao.getNewlyVideoCount();
    }
}
