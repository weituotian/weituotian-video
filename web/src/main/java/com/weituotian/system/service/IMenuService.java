package com.weituotian.system.service;

import com.weituotian.core.result.JsTreeNode;
import com.weituotian.core.utils.PageInfo;
import com.weituotian.system.entity.Menu;
import com.weituotian.system.entity.vo.MenuVo;

import java.util.List;

/**
 *
 * Created by ange on 2016/9/14.
 */
public interface IMenuService extends IAdminBaseService<Menu,Integer> {

    void addMenu(Menu menu,Integer pid,Integer resourceId);

    void updateMenu(Menu menu,Integer pid,Integer resourceId);

    /**
     * 分页获取菜单vo
     * @param pageInfo
     */
    void findPageMenuVo(PageInfo<MenuVo> pageInfo);

    /**
     * 获取所有menuvo,用于构建首页的菜单
     */
    List<MenuVo> findAllMenuVo();

    /**
     * 获取当前用户的所有menuvo,用于构建首页的菜单
     * @return
     */
    List<MenuVo> findUserMenuVo();

    /**
     * 通过pid找到该pid对应的菜单的所有子菜单,生成jstreenode
     */
    List<JsTreeNode> findJsTreeNodeByPid(Integer pid);

    /**
     * 通过id找到jstreenode,一般是找根节点
     * @param id
     * @return
     */
    JsTreeNode findJsTreeNode(Integer id);

    List<JsTreeNode> findJsTreeRootNodes();
}
