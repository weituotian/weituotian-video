package com.weituotian.system.service;

/**
 * Created by ange on 2017/4/11.
 */
public interface IWelcomeService {

    Integer getMemberVideosCount(Integer memberId);

    Integer getMemberCommentsCount(Integer memberId);

    /**
     * 获得新增会员数量
     * @return
     */
    Integer getNewlyMemberCount();

    Integer getNewlyVideoCount();
}
