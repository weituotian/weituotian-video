package com.weituotian.system.service;

import com.weituotian.system.entity.Resource;
import com.weituotian.system.entity.vo.ResourceVo;

import java.util.List;

/**
 * 资源接口
 * Created by Administrator on 2016-09-30.
 */
public interface IResourceService extends IAdminBaseService<Resource,Integer> {

    /**
     * 找到资源表中定义的所有模块
     * @return
     */
    List<String> findMoudles();

    /**
     * 找到资源表中定义的所有控制器名称
     * @return
     */
    List<String> findControllers();

    /**
     * 找到资源表中属于指定模块的所有控制器名称
     * @return
     */
    List<String> findControllersByMoudleId(String moudle);

    /**
     * 获取resourcevo,
     * 如果给出的roleid是有资格资源的,resourcevo会被选上
     * @param roleId 角色id
     * @return
     */
    List<ResourceVo> findResourceVoAllByRoleId(Integer roleId);


    /**
     * 根据角色id查询角色所拥有的资源列表
     *
     * @param roleId
     * @return
     */
    List<Resource> findResourcesByRoleId(Integer roleId);


    /**
     * 每一个菜单的url加上contextPath
     * @param res
     * @param contextPath
     */
    void AddContextPath(List<Resource> res, String contextPath);


    /**
     * 创建一个简单的Resource
     * @param moudleName
     * @param controllerName
     * @return
     */
    Resource createDefaultResource(String moudleName, String controllerName);

    /**
     * 一键创建某个控制器的增删查改url
     *
     * @param controller_name 控制器名称
     */
    void autoCreateCRUD(String controller, String descript, String moudleName, String controllerName);
}
