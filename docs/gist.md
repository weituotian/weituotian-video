
## freemarker
1. 遍历enum

## js
```js
"通过通过".indexOf("过") 
1
//设置radio选中：
$("input[name=radio]").prop("checked",true);
```

## 模态框
```js
var $modal = $("#myModal");
$modal.modal("show");
```

## layer
- https://www.layui.com/doc/modules/layer.html#success
```js
            layer_index = layer.open({
                type: 2,
                title: $this.attr("title"),
                shadeClose: true,
                shade: false,
                scrollbar: false,
//                maxmin: true, //开启最大化最小化按钮
                area: ['80%', '60%'],
                content: '${common.grant_url}?id=' + userId
            });
```

## criteria
- 分页查询我的收藏，无搜索条件
```
        Session session = getSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Video> cq = cb.createQuery(Video.class);

        Root<Member> member = cq.from(Member.class);
        Join<Member, Video> videos = member.join(Member_.videoCollect, JoinType.LEFT);

        Predicate predicate=cb.equal(member.get(Member_.id), userId);

        //条件
        cq.where(predicate);
        cq.select(videos);

        TypedQuery<Video> query = session.createQuery(cq);

        //分页
        query.setFirstResult(pageInfo.getFrom());
        query.setMaxResults(pageInfo.getPagesize());

        List<Video> result = query.getResultList();

        //查询总数
        CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
        //条件
        cq.where(predicate);
        queryCount.select(cb.countDistinct(videos));
        Query<Long> query1 = session.createQuery(queryCount);
//        Long total = (Long) query1.getSingleResult();
        Long total = executeCountQuery(query1);
        pageInfo.setTotal(total.intValue());

        return result
```
