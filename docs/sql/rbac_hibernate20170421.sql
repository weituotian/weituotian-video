/*
Navicat MySQL Data Transfer

Source Server         : phpstudy
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : rbac_hibernate

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-04-21 23:57:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for attachment
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `temp` bit(1) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attachment
-- ----------------------------
INSERT INTO `attachment` VALUES ('1', '2017-03-04', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\ab10f6c6071f49d382ccbeb8b1849467.jpg', '', 'image/jpeg', null, null);
INSERT INTO `attachment` VALUES ('2', '2017-03-04', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\1b1e1582ff2a4a70b9ba9f5722695e4b.png', '', 'image/png', null, null);
INSERT INTO `attachment` VALUES ('3', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\68ee8428545944c983e0763226cb4bdf.jpeg', '', 'image/jpeg', null, null);
INSERT INTO `attachment` VALUES ('4', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\6304d8829ea54e599c32fcaf28a3aa6d.jpeg', '', 'image/jpeg', null, null);
INSERT INTO `attachment` VALUES ('5', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\086507260da04564b55358d3b4938e14.jpeg', '', 'image/jpeg', null, null);
INSERT INTO `attachment` VALUES ('6', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\50d8230db9c14c4ba0cd6c739b7d5bc0.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('7', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\fdd11617e89c45a4885ef6ce0fb397c8.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('8', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\f5780f52d02348868e3943cae0578468.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('9', '2017-03-06', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\41021b8e1cad41dca8731ff6a71ad709.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('10', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\c995c023997840978bef6fa76289a118.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('11', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\ba36f3e529f3458081294f348c9fbf38.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('119', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\2990e3b035384dc9bde55d3764ac091c.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('120', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\94c49bdb3f264713a8a2770e7eb4ecc7.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('121', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\86e4313bc95247708bdab016861f747f.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('122', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\87960812c85146d0b5fd49ebaf2a4378.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('123', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\fe8769b217b848e8be8c89e64a5c7e4a.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('124', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\af61dbeb992f4252a44ed969554e6529.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('125', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\a36accd726044abea895ffd8fef61a1f.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('126', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\c9614040d18d4a1bbf00b76fc92abf9b.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('127', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\bb66305bac4c4b69bdae638efc562115.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('128', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\ae59bba5e6804ce58503b028fc9a1ea2.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('129', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\e5c0f63b92ad40628ab17fa5550c9966.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('130', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\1f89a08ce8014ac0b007b503eda9bf84.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('131', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\b4c1c32ffc634d8697f93c8340e3d0f7.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('132', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\6bf801ab2cd14d8db5f8b84a096615bc.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('133', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\94132eef4142456db98c7f8fe4936240.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('134', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\6b5f80fe137a47c8ab3b12c3eceee588.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('135', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\334eedbfedbe4fe3ae67bda7f01cb25b.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('136', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\5ca2bf422a3f487fb4885a31fb19b43c.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('137', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\69b9533481424786adf593ee4e0fd417.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('138', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\c70da2ecb15c4c53b3093b4c7ab722b2.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('139', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\4a95b37fb2504131841e97155661085e.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('140', '2017-03-07', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\9311e1ec54cc43438fa34db27dcdb454.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('141', '2017-03-08', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\42d9eccf290d4e32ae333e037879ef09.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('142', '2017-03-09', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\fcf9444b44254241b9b45bc39e61629e.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('143', '2017-03-09', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\28fc1e4cbd194150a9c0ddddbb5d2d96.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('144', '2017-03-09', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\9e7ee1dedd37406882db55012378e710.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('145', '2017-03-09', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\7098363e6f1b47d788f97601973e9628.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('146', '2017-03-21', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\7ddf6bcc11e84fad811d73ce7d4bf84a.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('147', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\f72da0f32a0643fdb130e7ae68d29b86.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('148', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\3382ada0b5af4e93b9f8a1531da2a246.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('149', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\0ab198560a3e443abfa7a31c5eedeebc.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('150', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\cd0e639c28c84a01a6fcdb1fd86437c5.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('151', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\423be5f64a5346d7b4bdbb7e1bdc2634.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('152', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\1574cedac6c84b4e871fa3670293d74e.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('153', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\ec878748da0b465cb24d8ba0513bb97c.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('154', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\716f8cd179dd4571a22966a1ded48232.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('155', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\94291991d9e14176a6980476e0bd4c9d.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('156', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\7354b42b92474f7286bf49c37f11b627.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('157', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\60a8cde2e8f846429a50319524a20152.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('158', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\9bb5b33d90944b94a22f7e4b7d2021d9.mp4', '', 'video/mp4', '104', '9bb5b33d90944b94a22f7e4b7d2021d9.mp4');
INSERT INTO `attachment` VALUES ('159', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\846e7c2a45cb430ca02e8776ab6668dd.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('160', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\22fc49a61e7644f983a8751befc06395.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('161', '2017-03-22', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\a1fafc6014744ded826860624dfb5b28.mp4', '', 'video/mp4', '104', null);
INSERT INTO `attachment` VALUES ('162', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\d33e97fbf0534685a6c116b3376e1c7a.jpeg', '', 'image/jpeg', '104', null);
INSERT INTO `attachment` VALUES ('163', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\84bfce407f1c4d31ace004de6d687984.png', '', 'image/png', '104', null);
INSERT INTO `attachment` VALUES ('164', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\2a1831643be0419bbac7d69961e2cc54.png', '', 'image/png', '104', null);
INSERT INTO `attachment` VALUES ('165', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\656aba88466b44e8a06ca3786c8b3991.png', '', 'image/png', '104', null);
INSERT INTO `attachment` VALUES ('166', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\eb46bba78d6d476ca69574df0759c5f4.png', '', 'image/png', '104', null);
INSERT INTO `attachment` VALUES ('167', '2017-03-23', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\a543b14ac7004b22920c382fc7e0fa2f.png', '', 'image/png', '104', null);
INSERT INTO `attachment` VALUES ('168', '2017-03-24', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\224d2459d2014ca980b1c34362c2d628.mp4', '', 'video/mp4', '104', '224d2459d2014ca980b1c34362c2d628.mp4');
INSERT INTO `attachment` VALUES ('169', '2017-03-25', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\c17b0731c6fc4687abfa92a76599fe73.jpeg', '', 'image/jpeg', '104', 'c17b0731c6fc4687abfa92a76599fe73.jpeg');
INSERT INTO `attachment` VALUES ('170', '2017-03-28', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\b338c0729ccd4adb8765f0a9d10cbf93.jpeg', '', 'image/jpeg', '105', 'b338c0729ccd4adb8765f0a9d10cbf93.jpeg');
INSERT INTO `attachment` VALUES ('171', '2017-03-28', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\a37aa89e254e408f99d165c608809757.mp4', '', 'video/mp4', '105', 'a37aa89e254e408f99d165c608809757.mp4');
INSERT INTO `attachment` VALUES ('172', '2017-03-28', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\024d4a71485247e4bb997f0c80eab885.jpeg', '', 'image/jpeg', '104', '024d4a71485247e4bb997f0c80eab885.jpeg');
INSERT INTO `attachment` VALUES ('173', '2017-03-28', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\cde7bc915b914fe5967bdcdd4f558816.mp4', '', 'video/mp4', '104', 'cde7bc915b914fe5967bdcdd4f558816.mp4');
INSERT INTO `attachment` VALUES ('174', '2017-03-30', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\426fc087d53b4b71bd5d446baff3382d.mp4', '', 'video/mp4', '104', '426fc087d53b4b71bd5d446baff3382d.mp4');
INSERT INTO `attachment` VALUES ('175', '2017-03-30', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\dce2023e5d4b474fbadbfe5020e66a8f.jpeg', '', 'image/jpeg', '104', 'dce2023e5d4b474fbadbfe5020e66a8f.jpeg');
INSERT INTO `attachment` VALUES ('176', '2017-04-08', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\3f8f3f8305de487781e5c5d7abb546ac.jpg', '', 'image/jpeg', '1', '3f8f3f8305de487781e5c5d7abb546ac.jpg');
INSERT INTO `attachment` VALUES ('177', '2017-04-08', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\b2e1bded975b41a8bda069889c6fb7f6.png', '', 'image/png', '1', 'b2e1bded975b41a8bda069889c6fb7f6.png');
INSERT INTO `attachment` VALUES ('178', '2017-04-08', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\6532bdfc555642ffbe13f2ab483efd28.png', '', 'image/png', '1', '6532bdfc555642ffbe13f2ab483efd28.png');
INSERT INTO `attachment` VALUES ('179', '2017-04-08', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\d5382ed4632b4a6da0f1b743b88e7084.png', '', 'image/png', '1', 'd5382ed4632b4a6da0f1b743b88e7084.png');
INSERT INTO `attachment` VALUES ('180', '2017-04-13', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\5b2cbbab21044661a25f618a8de79234.jpg', '', 'image/jpeg', '1', '5b2cbbab21044661a25f618a8de79234.jpg');
INSERT INTO `attachment` VALUES ('181', '2017-04-13', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\7017daa1811441b5a88d8d6d5734287a.jpg', '', 'image/jpeg', '1', '7017daa1811441b5a88d8d6d5734287a.jpg');
INSERT INTO `attachment` VALUES ('182', '2017-04-13', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\83b273579d5242d885bf2db615f3052d.JPG', '', 'image/jpeg', '1', '83b273579d5242d885bf2db615f3052d.JPG');
INSERT INTO `attachment` VALUES ('183', '2017-04-13', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\33e7daf71f9342858502456058589e34.JPG', '', 'image/jpeg', '1', '33e7daf71f9342858502456058589e34.JPG');
INSERT INTO `attachment` VALUES ('184', '2017-04-13', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\avater\\494511c03ee04c95b3c0147f7cda36df.jpg', '', 'image/jpeg', '1', '494511c03ee04c95b3c0147f7cda36df.jpg');
INSERT INTO `attachment` VALUES ('185', '2017-04-18', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\dd3f54fb1e4d4f139247ea812ff0db37.mp4', '', 'video/mp4', '104', 'dd3f54fb1e4d4f139247ea812ff0db37.mp4');
INSERT INTO `attachment` VALUES ('186', '2017-04-18', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\8e5ce8240760430da59f2562fc4267b9.mp4', '', 'video/mp4', '104', '8e5ce8240760430da59f2562fc4267b9.mp4');
INSERT INTO `attachment` VALUES ('187', '2017-04-19', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\c30a16e07e7f4107a509016d97bfc1ff.mp4', '\0', 'video/mp4', '104', 'c30a16e07e7f4107a509016d97bfc1ff.mp4');
INSERT INTO `attachment` VALUES ('188', '2017-04-19', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\cover\\377122b4c2fb45ed9d810b0bba178047.jpeg', '', 'image/jpeg', '104', '377122b4c2fb45ed9d810b0bba178047.jpeg');
INSERT INTO `attachment` VALUES ('189', '2017-04-19', 'E:\\java web\\weituotian-video\\web\\target\\web\\upload\\video\\f8ea18af06f44418b9815fc15d895816.mp4', '', 'video/mp4', '105', 'f8ea18af06f44418b9815fc15d895816.mp4');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `postTime` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_member` (`member_id`),
  KEY `fk_comment_video` (`video_id`),
  CONSTRAINT `fk_comment_member` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `fk_comment_video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('4', '测试', '2017-03-26 18:35:13', '104', '2');
INSERT INTO `comment` VALUES ('5', '这是一条回复', '2017-03-26 21:07:49', '104', '2');
INSERT INTO `comment` VALUES ('6', '测试回复2', '2017-03-26 21:55:47', '104', '2');
INSERT INTO `comment` VALUES ('7', '测试回复3', '2017-03-26 21:56:02', '104', '2');
INSERT INTO `comment` VALUES ('8', '测试回复4', '2017-03-26 21:56:19', '104', '2');
INSERT INTO `comment` VALUES ('9', '测试回复5', '2017-03-26 21:56:25', '104', '2');
INSERT INTO `comment` VALUES ('10', '测试回复6', '2017-03-26 21:56:32', '104', '2');
INSERT INTO `comment` VALUES ('11', '测试回复7', '2017-03-26 21:56:52', '104', '2');
INSERT INTO `comment` VALUES ('12', '测试回复8', '2017-03-26 21:56:56', '104', '2');
INSERT INTO `comment` VALUES ('13', '测试回复9', '2017-03-26 21:57:00', '104', '2');
INSERT INTO `comment` VALUES ('14', '测试回复10', '2017-03-26 21:57:05', '104', '2');
INSERT INTO `comment` VALUES ('15', '测试回复11', '2017-03-26 21:57:32', '104', '2');
INSERT INTO `comment` VALUES ('16', '测试回复12', '2017-03-26 21:57:35', '104', '2');
INSERT INTO `comment` VALUES ('17', '测试回复13', '2017-03-26 21:57:38', '104', '2');
INSERT INTO `comment` VALUES ('20', '测试qweqwe', '2017-03-10 15:11:17', '104', '1');
INSERT INTO `comment` VALUES ('21', '测试', '2017-04-18 23:59:10', '104', '8');

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES ('2');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `descript` varchar(255) DEFAULT NULL,
  `fans` int(11) DEFAULT '0',
  `level` int(11) DEFAULT '0',
  `regDate` date DEFAULT NULL,
  `sex` enum('UNKNOW','FEMALE','MALE') DEFAULT 'UNKNOW',
  `videos` int(11) DEFAULT '0',
  `follows` int(11) DEFAULT '0',
  `experience` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK4myur39bhn9oj6a9k52t2fmry` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('104', '123', 'a543b14ac7004b22920c382fc7e0fa2f.png', '2017-03-10', '大家好2', '1', '0', '2017-03-23', 'MALE', '4', '0', '0');
INSERT INTO `member` VALUES ('105', '123', '33e7daf71f9342858502456058589e34.JPG', '2017-03-27', '大家好', '1', '0', '2017-03-30', 'MALE', '0', '0', '0');
INSERT INTO `member` VALUES ('106', null, '494511c03ee04c95b3c0147f7cda36df.jpg', '2017-03-27', '大家好', '0', '0', '2017-04-04', 'FEMALE', '0', '0', '0');

-- ----------------------------
-- Table structure for member_collect
-- ----------------------------
DROP TABLE IF EXISTS `member_collect`;
CREATE TABLE `member_collect` (
  `member_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  KEY `FKiy565eit25jkditge02vcq81a` (`video_id`),
  KEY `FK97o0tso1o0ojsbkre9yu6p1vm` (`member_id`),
  CONSTRAINT `FK97o0tso1o0ojsbkre9yu6p1vm` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `FKiy565eit25jkditge02vcq81a` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_collect
-- ----------------------------
INSERT INTO `member_collect` VALUES ('104', '10');

-- ----------------------------
-- Table structure for member_follow
-- ----------------------------
DROP TABLE IF EXISTS `member_follow`;
CREATE TABLE `member_follow` (
  `member_id` int(11) NOT NULL,
  `up_id` int(11) NOT NULL,
  KEY `FK7ugbnfprwm647lka0hge7qo32` (`up_id`),
  KEY `FKc9vux75wrypgt9ej8yg0k6vya` (`member_id`),
  CONSTRAINT `FK7ugbnfprwm647lka0hge7qo32` FOREIGN KEY (`up_id`) REFERENCES `member` (`id`),
  CONSTRAINT `FKc9vux75wrypgt9ej8yg0k6vya` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_follow
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `text` varchar(40) DEFAULT NULL,
  `status` enum('CLOSE','OPEN') CHARACTER SET gbk DEFAULT 'OPEN',
  `seq` smallint(6) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm4wdmfh1iwdhvuphodxrspm3w` (`resource_id`),
  KEY `fk_menu_parent` (`pid`),
  CONSTRAINT `FKmsygif4n2j984d0rk2057krxx` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`),
  CONSTRAINT `fk_menu_parent` FOREIGN KEY (`pid`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '99', '用户管理', 'OPEN', '2', 'fa fa-fw fa-safari', '1');
INSERT INTO `menu` VALUES ('2', '1', '用户新建', 'OPEN', '3', 'fa fa-fw fa-hand-peace-o', '2');
INSERT INTO `menu` VALUES ('3', '99', '角色管理', 'OPEN', '3', 'glyphicon glyphicon-user', '3');
INSERT INTO `menu` VALUES ('4', '3', '角色新建', 'OPEN', '1', 'fa fa-fw fa-hand-peace-o', '4');
INSERT INTO `menu` VALUES ('6', '99', '资源管理', 'OPEN', '4', 'fa fa-fw fa-send-o', '6');
INSERT INTO `menu` VALUES ('7', '6', '资源列表', 'OPEN', '1', 'fa fa-fw fa-list', '7');
INSERT INTO `menu` VALUES ('8', '6', '资源新建', 'OPEN', '2', 'fa fa-fw fa-plus-square', '8');
INSERT INTO `menu` VALUES ('12', '1', '用户列表', 'OPEN', '1', 'fa fa-fw fa-flag', '12');
INSERT INTO `menu` VALUES ('19', '3', '角色列表', 'OPEN', '0', 'fa fa-fw fa-hand-peace-o', '19');
INSERT INTO `menu` VALUES ('71', '99', '会话管理', 'OPEN', '0', 'fa fa-fw fa-sheqel', '71');
INSERT INTO `menu` VALUES ('72', '71', '会话列表', 'OPEN', '0', 'fa fa-fw fa-cc-jcb', '72');
INSERT INTO `menu` VALUES ('74', '1', '用户管理子test12', 'CLOSE', '1', 'fa fa-fw fa-cc-diners-club', '74');
INSERT INTO `menu` VALUES ('77', '1', '用户管理test2', 'OPEN', '1', 'fa fa-fw fa-fonticons', '1');
INSERT INTO `menu` VALUES ('82', '77', '用户管理xxxx2', 'OPEN', '2', 'fa fa-fw fa-gg-circle', null);
INSERT INTO `menu` VALUES ('83', '82', '用户管理xxxx3', 'OPEN', '2', 'fa fa-fw fa-balance-scale', '78');
INSERT INTO `menu` VALUES ('86', '99', '菜单管理', 'OPEN', '7', 'fa fa-fw fa-battery-2', null);
INSERT INTO `menu` VALUES ('87', '86', '新建菜单', 'OPEN', '1', 'fa fa-fw fa-cloud', '99');
INSERT INTO `menu` VALUES ('88', '86', '菜单列表', 'OPEN', '1', 'fa fa-fw fa-calendar-check-o', '98');
INSERT INTO `menu` VALUES ('89', null, '会员中心', 'OPEN', '1', 'fa fa-fw fa-diamond', '110');
INSERT INTO `menu` VALUES ('90', '89', '我的资料', 'OPEN', '1', 'fa fa-fw fa-object-ungroup', '78');
INSERT INTO `menu` VALUES ('91', '89', '我的视频', 'OPEN', '1', 'fa fa-fw fa-file-video-o', '116');
INSERT INTO `menu` VALUES ('92', '89', '上传视频', 'OPEN', '1', 'fa fa-fw fa-superscript', '113');
INSERT INTO `menu` VALUES ('93', null, '运维管理', 'OPEN', '1', 'fa fa-fw fa-cc-visa', '133');
INSERT INTO `menu` VALUES ('94', '93', '视频管理', 'OPEN', '1', 'fa fa-fw fa-file-video-o', '121');
INSERT INTO `menu` VALUES ('95', '93', '会员列表', 'OPEN', '1', 'fa fa-fw fa-male', '123');
INSERT INTO `menu` VALUES ('96', '93', '评论管理', 'OPEN', '1', 'fa fa-fw fa-amazon', '127');
INSERT INTO `menu` VALUES ('99', null, '系统管理', 'OPEN', '1', 'fa fa-fw fa-internet-explorer', '109');

-- ----------------------------
-- Table structure for patition
-- ----------------------------
DROP TABLE IF EXISTS `patition`;
CREATE TABLE `patition` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patition
-- ----------------------------
INSERT INTO `patition` VALUES ('1', '搞笑');
INSERT INTO `patition` VALUES ('2', '街拍');

-- ----------------------------
-- Table structure for reply
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply` (
  `id` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `postTime` datetime DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6w0ns67lrq1jdiwi5xvtj1vxx` (`comment_id`),
  KEY `FKen6vrmi5oth4bg6ybfc202fmu` (`member_id`),
  CONSTRAINT `FK6w0ns67lrq1jdiwi5xvtj1vxx` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`),
  CONSTRAINT `FKen6vrmi5oth4bg6ybfc202fmu` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reply
-- ----------------------------

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源id',
  `moudle_id` int(11) DEFAULT NULL,
  `name` varchar(40) NOT NULL COMMENT '名字',
  `url` varchar(100) NOT NULL COMMENT '地址',
  `status` enum('CLOSE','OPEN') NOT NULL DEFAULT 'OPEN' COMMENT '状态',
  `controller` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `moudle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKatvqkf00c7r84k2pvpmyjtj42` (`moudle_id`),
  CONSTRAINT `FKatvqkf00c7r84k2pvpmyjtj42` FOREIGN KEY (`moudle_id`) REFERENCES `resource_moudle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of resource
-- ----------------------------
INSERT INTO `resource` VALUES ('1', null, '用户管理', '/system/user', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('2', null, '用户新建', '/system/user/add', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('3', null, '角色管理', '/system/role', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('4', null, '角色新建', '/system/role/add', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('5', null, '处理新建', '/system/user/doadd', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('6', null, '资源管理', '/system/resource', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('7', null, '资源列表', '/system/resource/list', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('8', null, '资源新建', '/system/resource/add', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('9', null, '处理新建', '/system/resource/doadd', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('10', null, '资源编辑', '/system/resource/edit', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('11', null, '处理编辑', '/system/resource/doedit', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('12', null, '用户列表', '/system/user/list', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('13', null, '用户编辑', '/system/user/edit', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('14', null, '处理编辑', '/system/user/doedit', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('15', null, '用户删除', '/system/user/delete', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('16', null, '处理新建', '/system/role/doadd', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('17', null, '角色编辑', '/system/role/edit', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('18', null, '处理编辑', '/system/role/doedit', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('19', null, '角色列表', '/system/role/list', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('20', null, '角色删除', '/system/role/delete', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('21', null, '资源删除', '/system/resource/delete', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('22', null, '角色分配', '/system/role/grant', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('23', null, '处理分配', '/system/role/dogrant', 'OPEN', '角色管理', '', '系统管理');
INSERT INTO `resource` VALUES ('39', null, '资源一键创建', '/system/resource/autocreate', 'OPEN', '资源管理', '', '系统管理');
INSERT INTO `resource` VALUES ('71', null, '会话管理', '/system/session', 'OPEN', '会话管理', '', '系统管理');
INSERT INTO `resource` VALUES ('72', null, '会话列表', '/system/session/list', 'OPEN', '会话管理', '', '系统管理');
INSERT INTO `resource` VALUES ('73', null, '会话踢出', '/system/session/kick', 'OPEN', '会话管理', '', '系统管理');
INSERT INTO `resource` VALUES ('74', null, '用户管理子test12', '/system/yongtest', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('77', null, '用户管理test245', '/system/eee', 'OPEN', '用户管理', '', '系统管理');
INSERT INTO `resource` VALUES ('78', null, '我的资料', '/member/my/edit', 'OPEN', '会员中心', '', '会员中心');
INSERT INTO `resource` VALUES ('98', null, '菜单列表', '/system/menu/list', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('99', null, '菜单新建', '/system/menu/add', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('100', null, '菜单处理新建', '/system/menu/doadd', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('101', null, '菜单编辑', '/system/menu/edit', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('102', null, '菜单处理编辑', '/system/menu/doedit', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('103', null, '菜单删除', '/system/menu/delete', 'OPEN', '菜单管理', '', '系统管理');
INSERT INTO `resource` VALUES ('109', null, '系统管理菜单Header', '/system', 'OPEN', '系统管理', '', '系统管理');
INSERT INTO `resource` VALUES ('110', null, '会员中心菜单Header', '/member', 'OPEN', '会员中心', '', '会员中心');
INSERT INTO `resource` VALUES ('111', null, '我的资料修改', '/member/my/doedit', 'OPEN', '会员中心', '', '会员中心');
INSERT INTO `resource` VALUES ('112', null, '头像上传', '/member/my/avaterupload', 'OPEN', '会员中心', '', '会员中心');
INSERT INTO `resource` VALUES ('113', null, '视频上传页面', '/member/video/add', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('114', null, '视频上传ajax', '/member/video/ajaxvideo', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('115', null, '会员封面ajax', '/member/video/ajaxcover', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('116', null, '会员视频列表', '/member/video/index', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('117', null, '新增视频', '/member/video/doadd', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('118', null, '编辑视频', '/member/video/doedit', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('119', null, '删除视频', '/member/video/delete', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('120', null, '编辑视频', '/member/video/edit', 'OPEN', '会员视频', '', '会员中心');
INSERT INTO `resource` VALUES ('121', null, '视频审核', '/manager/video/index', 'OPEN', '视频管理', '', '运维管理');
INSERT INTO `resource` VALUES ('122', null, '删除视频', '/manager/video/delete', 'OPEN', '视频管理', '', '运维管理');
INSERT INTO `resource` VALUES ('123', null, '会员列表', '/manager/member/index', 'OPEN', '会员管理', '', '运维管理');
INSERT INTO `resource` VALUES ('124', null, '会员删除', '/manager/member/delete', 'OPEN', '会员管理', '', '运维管理');
INSERT INTO `resource` VALUES ('125', null, '会员编辑页面', '/manager/member/edit', 'OPEN', '会员管理', '', '运维管理');
INSERT INTO `resource` VALUES ('126', null, '处理会员编辑', '/manager/member/doedit', 'OPEN', '会员管理', '', '运维管理');
INSERT INTO `resource` VALUES ('127', null, '评论列表', '/manager/comment/index', 'OPEN', '评论管理', '', '运维管理');
INSERT INTO `resource` VALUES ('130', null, '评论编辑', '/manager/comment/edit', 'OPEN', '评论管理', '', '运维管理');
INSERT INTO `resource` VALUES ('131', null, '评论处理编辑', '/manager/comment/doedit', 'OPEN', '评论管理', '', '运维管理');
INSERT INTO `resource` VALUES ('132', null, '评论删除', '/manager/comment/delete', 'OPEN', '评论管理', '', '运维管理');
INSERT INTO `resource` VALUES ('133', null, '运维管理菜单header', '/manager', 'OPEN', '运维管理', '', '运维管理');
INSERT INTO `resource` VALUES ('134', null, '查看视频', '/manager/video/view', 'OPEN', '视频管理', '', '运维管理');

-- ----------------------------
-- Table structure for resources_backup
-- ----------------------------
DROP TABLE IF EXISTS `resources_backup`;
CREATE TABLE `resources_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源id',
  `moudle_id` int(11) DEFAULT NULL,
  `name` varchar(40) NOT NULL COMMENT '名字',
  `url` varchar(100) NOT NULL COMMENT '地址',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父资源id',
  `description` varchar(40) NOT NULL COMMENT '描述',
  `status` enum('close','open') NOT NULL DEFAULT 'open' COMMENT '状态',
  `type_id` int(11) NOT NULL COMMENT '资源类型id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `seq` int(11) NOT NULL COMMENT '排序',
  `icon` varchar(40) DEFAULT '' COMMENT '图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of resources_backup
-- ----------------------------
INSERT INTO `resources_backup` VALUES ('1', null, '用户管理', '/user', '0', '', 'open', '1', '2016-10-09 16:59:55', '2', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('2', null, '用户新建', '/user/add', '1', '', 'open', '1', '2016-10-10 11:13:42', '3', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('3', null, '角色管理', '/role', '0', '', 'open', '1', '2016-10-10 11:14:24', '3', 'glyphicon glyphicon-user');
INSERT INTO `resources_backup` VALUES ('4', null, '角色新建', '/role/add', '3', '', 'open', '1', '2016-10-10 11:15:26', '1', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('5', null, '处理新建', '/user/doadd', '1', '', 'open', '3', '2016-10-10 11:19:56', '1', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('6', null, '资源管理', '/resource', '0', '', 'open', '1', '2016-10-09 16:59:55', '4', 'fa fa-fw fa-send-o');
INSERT INTO `resources_backup` VALUES ('7', null, '资源列表', '/resource/index', '6', '', 'open', '1', '2016-10-09 16:59:55', '1', 'fa fa-fw fa-list');
INSERT INTO `resources_backup` VALUES ('8', null, '资源新建', '/resource/add', '6', '', 'open', '1', '2016-10-09 16:59:55', '2', 'fa fa-fw fa-plus-square');
INSERT INTO `resources_backup` VALUES ('9', null, '处理新建', '/resource/doadd', '6', '', 'open', '3', '2016-10-09 16:59:55', '4', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('10', null, '资源编辑', '/resource/edit', '6', '', 'open', '4', '2016-10-09 16:59:55', '1', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('11', null, '处理编辑', '/resource/doedit', '6', '', 'open', '3', '2016-10-09 16:59:55', '5', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('12', null, '用户列表', '/user/list', '1', '', 'open', '1', '2016-11-15 23:38:29', '1', 'fa fa-fw fa-flag');
INSERT INTO `resources_backup` VALUES ('13', null, '用户编辑', '/user/edit', '1', '', 'open', '4', '2016-10-10 11:19:56', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('14', null, '处理编辑', '/user/doedit', '1', '', 'open', '3', '2016-10-10 11:19:56', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('15', null, '用户删除', '/user/delete', '1', '', 'open', '3', '2016-10-10 11:19:56', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('16', null, '处理新建', '/role/doadd', '3', '', 'open', '3', '2016-10-10 11:15:26', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('17', null, '角色编辑', '/role/edit', '3', '', 'open', '4', '2016-10-10 11:15:26', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('18', null, '处理编辑', '/role/doedit', '3', '', 'open', '3', '2016-10-10 11:15:26', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('19', null, '角色列表', '/role/list', '3', '', 'open', '1', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('20', null, '角色删除', '/role/delete', '3', '', 'open', '3', '2016-10-10 11:15:26', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('21', null, '资源删除', '/resource/delete', '6', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('22', null, '角色分配', '/role/grant', '3', '', 'open', '4', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('23', null, '处理分配', '/role/dogrant', '3', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('24', null, '资源类型列表', '/resourcetype/list', '25', '', 'open', '1', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('25', null, '资源类型管理', '/resourcetype', '0', '', 'open', '1', '2016-10-10 11:14:24', '5', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('26', null, '资源类型新建', '/resourcetype/add', '25', '', 'open', '1', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('27', null, '处理新建', '/resourcetype/doadd', '25', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('28', null, '资源类型编辑', '/resourcetype/edit', '25', '', 'open', '4', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('29', null, '处理编辑', '/resourcetype/doedit', '25', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('30', null, '资源类型删除', '/resourcetype/delete', '25', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('31', null, '病人管理', '/patient', '0', '', 'open', '1', '2016-10-10 11:14:24', '8', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('32', null, '病人列表', '/patient/list', '31', '', 'open', '1', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('33', null, '病人新建', '/patient/add', '31', '', 'open', '1', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('34', null, '处理新建', '/patient/doadd', '31', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('35', null, '病人编辑', '/patient/edit', '31', '', 'open', '4', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('36', null, '处理编辑', '/patient/doedit', '31', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('37', null, '病人删除', '/patient/delete', '31', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('39', null, '资源一键创建', '/resource/autocreate', '6', '', 'open', '3', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('49', null, '科室管理', '/department', '0', '', 'open', '1', '2016-11-17 00:29:35', '6', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('50', null, '科室列表', '/department/list', '49', '', 'open', '1', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('51', null, '科室新建', '/department/add', '49', '', 'open', '1', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('52', null, '处理新建', '/department/doadd', '49', '', 'open', '3', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('53', null, '科室编辑', '/department/edit', '49', '', 'open', '4', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('54', null, '处理编辑', '/department/doedit', '49', '', 'open', '3', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('55', null, '科室删除', '/department/delete', '49', '', 'open', '3', '2016-11-17 00:29:35', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('56', null, '医生管理', '/doctor', '0', '', 'open', '1', '2016-11-17 15:24:52', '7', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('57', null, '医生列表', '/doctor/list', '56', '', 'open', '1', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('58', null, '医生新建', '/doctor/add', '56', '', 'open', '1', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('59', null, '处理新建', '/doctor/doadd', '56', '', 'open', '3', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('60', null, '医生编辑', '/doctor/edit', '56', '', 'open', '4', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('61', null, '处理编辑', '/doctor/doedit', '56', '', 'open', '3', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('62', null, '医生删除', '/doctor/delete', '56', '', 'open', '3', '2016-11-17 15:24:52', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('63', null, '挂号管理', '/register', '0', '', 'open', '1', '2016-11-18 00:38:33', '9', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('64', null, '挂号列表', '/register/list', '63', '', 'open', '1', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('65', null, '挂号新建', '/register/add', '63', '', 'open', '1', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('66', null, '处理新建', '/register/doadd', '63', '', 'open', '3', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('67', null, '挂号编辑', '/register/edit', '63', '', 'open', '4', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('68', null, '处理编辑', '/register/doedit', '63', '', 'open', '3', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('69', null, '挂号删除', '/register/delete', '63', '', 'open', '3', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('70', null, '病人选择', '/patient/select', '31', '', 'open', '4', '2016-10-10 11:14:24', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('71', null, '会话管理', '/session', '0', '', 'open', '1', '2016-11-18 00:38:33', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('72', null, '会话列表', '/session/list', '71', '', 'open', '1', '0000-00-00 00:00:00', '0', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('73', null, '会话踢出', '/session/kick', '71', '踢出', 'open', '3', '2016-11-23 21:15:24', '1', 'fa fa-fw fa-hand-peace-o');
INSERT INTO `resources_backup` VALUES ('74', null, '用户管理子test12', '/yongtest', '1', 'qwe', 'close', '2', '2017-01-19 09:50:11', '1', 'fa fa-fw fa-cc-diners-club');
INSERT INTO `resources_backup` VALUES ('77', null, '用户管理test2', '/', '1', 'a', 'open', '1', '2017-01-19 16:38:20', '1', 'fa fa-fw fa-car');

-- ----------------------------
-- Table structure for resource_moudle
-- ----------------------------
DROP TABLE IF EXISTS `resource_moudle`;
CREATE TABLE `resource_moudle` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resource_moudle
-- ----------------------------
INSERT INTO `resource_moudle` VALUES ('1', '用户管理');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(40) NOT NULL COMMENT '名字',
  `seq` int(11) NOT NULL COMMENT '顺序',
  `status` enum('CLOSE','OPEN') NOT NULL DEFAULT 'CLOSE' COMMENT '状态,1正常,0停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('2', '普通会员', '1', 'OPEN');
INSERT INTO `role` VALUES ('5', '运维管理员', '2', 'OPEN');
INSERT INTO `role` VALUES ('17', '最高管理员', '1', 'OPEN');

-- ----------------------------
-- Table structure for role_resource
-- ----------------------------
DROP TABLE IF EXISTS `role_resource`;
CREATE TABLE `role_resource` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `resource_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`,`resource_id`),
  KEY `FK2ew344f4luv6k533ti7bh7h25` (`resource_id`),
  CONSTRAINT `FKh8lunkrwoyio367ec8y12bis1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKr2orp5em3dob6f299ra9oyexr` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_resource
-- ----------------------------
INSERT INTO `role_resource` VALUES ('2', '78');
INSERT INTO `role_resource` VALUES ('2', '110');
INSERT INTO `role_resource` VALUES ('2', '111');
INSERT INTO `role_resource` VALUES ('2', '112');
INSERT INTO `role_resource` VALUES ('5', '112');
INSERT INTO `role_resource` VALUES ('2', '113');
INSERT INTO `role_resource` VALUES ('2', '114');
INSERT INTO `role_resource` VALUES ('2', '115');
INSERT INTO `role_resource` VALUES ('2', '116');
INSERT INTO `role_resource` VALUES ('2', '117');
INSERT INTO `role_resource` VALUES ('2', '118');
INSERT INTO `role_resource` VALUES ('2', '119');
INSERT INTO `role_resource` VALUES ('2', '120');
INSERT INTO `role_resource` VALUES ('5', '121');
INSERT INTO `role_resource` VALUES ('5', '123');
INSERT INTO `role_resource` VALUES ('5', '124');
INSERT INTO `role_resource` VALUES ('5', '125');
INSERT INTO `role_resource` VALUES ('5', '126');
INSERT INTO `role_resource` VALUES ('5', '127');
INSERT INTO `role_resource` VALUES ('5', '130');
INSERT INTO `role_resource` VALUES ('5', '131');
INSERT INTO `role_resource` VALUES ('5', '132');
INSERT INTO `role_resource` VALUES ('5', '133');
INSERT INTO `role_resource` VALUES ('5', '134');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(40) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `user_type` enum('MEMBER','ADMIN','TOPADMIN') DEFAULT 'MEMBER' COMMENT '0为最高管理员,1为管理员，2为会员',
  `status` enum('OPEN','CLOSE') DEFAULT 'OPEN',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '4297f44b13955235245b2497399d7a93', 'asd@163.com', 'admin2', 'TOPADMIN', 'OPEN');
INSERT INTO `user` VALUES ('13', 'snoopy', '4297f44b13955235245b2497399d7a93', 'asd12@163.com', null, 'TOPADMIN', 'OPEN');
INSERT INTO `user` VALUES ('14', 'dreamlu', '4297f44b13955235245b2497399d7a93', 'asd@qwe.com', null, 'ADMIN', 'OPEN');
INSERT INTO `user` VALUES ('16', 'yunwei', '4297f44b13955235245b2497399d7a93', '111@qq.com', null, 'ADMIN', 'OPEN');
INSERT INTO `user` VALUES ('104', 'weituotian', '4297f44b13955235245b2497399d7a93', 'asd2@163.com', '超人1', 'MEMBER', 'OPEN');
INSERT INTO `user` VALUES ('105', 'weituotian1', '4297f44b13955235245b2497399d7a93', 'asd3@163.com', '超人22', 'MEMBER', 'OPEN');
INSERT INTO `user` VALUES ('106', 'weituotian2', '4297f44b13955235245b2497399d7a93', 'asd4@163.com', '小', 'MEMBER', 'OPEN');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK7ir6hi5jr98lclgjgbyxafneu` (`role_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKcnrjdc801vcdj8n017nlriv61` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('104', '2');
INSERT INTO `user_role` VALUES ('105', '2');
INSERT INTO `user_role` VALUES ('106', '2');
INSERT INTO `user_role` VALUES ('16', '5');
INSERT INTO `user_role` VALUES ('104', '17');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditTime` datetime DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `collect` int(11) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `descript` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `play` int(11) DEFAULT NULL,
  `totalTime` int(11) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `videoState` enum('Auditing','ReEdit','AuditFailure','Audited','Uncommitted') CHARACTER SET gbk DEFAULT 'Auditing',
  `member_id` int(11) DEFAULT NULL,
  `partition_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `attachment_id` int(11) DEFAULT NULL,
  `open_state` enum('OPEN','CLOSE') CHARACTER SET gbk DEFAULT 'OPEN',
  PRIMARY KEY (`id`),
  KEY `fk_video_member` (`member_id`),
  KEY `fk_video_partition` (`partition_id`),
  KEY `fk_video_attachment` (`attachment_id`),
  CONSTRAINT `fk_video_attachment` FOREIGN KEY (`attachment_id`) REFERENCES `attachment` (`id`),
  CONSTRAINT `fk_video_member` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
  CONSTRAINT `fk_video_partition` FOREIGN KEY (`partition_id`) REFERENCES `patition` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', null, '0', '1', 'b338c0729ccd4adb8765f0a9d10cbf93.jpeg', '2017-03-07 20:17:32', '上推荐了阿萨德计划', null, '12', null, '2017-03-07 20:17:32', 'Audited', '105', '1', '码云新作', '189', 'OPEN');
INSERT INTO `video` VALUES ('2', null, '0', '1', 'd33e97fbf0534685a6c116b3376e1c7a.jpeg', '2017-03-07 20:44:13', 'qwerrasd', null, '8', null, '2017-03-07 20:44:13', 'Audited', '104', '1', '我们', '185', 'OPEN');
INSERT INTO `video` VALUES ('8', null, '0', '0', '024d4a71485247e4bb997f0c80eab885.jpeg', '2017-03-28 16:43:31', '谢谢大家观看', null, '1', null, '2017-03-28 16:43:31', 'Audited', '104', '2', '测试，', '186', 'OPEN');
INSERT INTO `video` VALUES ('10', null, '0', '1', '377122b4c2fb45ed9d810b0bba178047.jpeg', '2017-04-19 00:03:06', '屠苏太累', null, '1', null, '2017-04-19 00:03:06', 'Audited', '104', '1', '啦啦啦', '187', 'OPEN');

-- ----------------------------
-- Table structure for video_tag
-- ----------------------------
DROP TABLE IF EXISTS `video_tag`;
CREATE TABLE `video_tag` (
  `video_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `FK3stb6cgk3gatx69ltr6o2vkxw` (`tag_id`),
  KEY `FK7a7a4rx1y3tr2ycwuqcrf580b` (`video_id`),
  CONSTRAINT `FK3stb6cgk3gatx69ltr6o2vkxw` FOREIGN KEY (`tag_id`) REFERENCES `video_tags` (`id`),
  CONSTRAINT `FK7a7a4rx1y3tr2ycwuqcrf580b` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video_tag
-- ----------------------------
INSERT INTO `video_tag` VALUES ('2', '1');

-- ----------------------------
-- Table structure for video_tags
-- ----------------------------
DROP TABLE IF EXISTS `video_tags`;
CREATE TABLE `video_tags` (
  `id` int(11) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of video_tags
-- ----------------------------
INSERT INTO `video_tags` VALUES ('1', '2017-03-25 00:44:00', '好笑');
