
# 参考文章

## 视频上传页面
- dmuploader上传例子
https://git.oschina.net/chinagtech/demos/blob/master/demos/index.html?dir=0&filepath=demos%2Findex.html&oid=70978e2a424dd28557d0853359b297644f461f7b&sha=e2d54c8642152a9cd9d377d30df01805b82c1e8a

- cropper.js 实现HTML5 裁剪图片并上传(裁剪上传头像。)
http://blog.csdn.net/qq727013465/article/details/51823231

- H5学习之路-图片上传（cropper、webuploader）
http://blog.csdn.net/qiyongkang520/article/details/53055517#reply

- java实现图片与base64字符串之间的转换  
http://blog.csdn.net/hfhwfw/article/details/5544408

- Cropper github  
https://github.com/fengyuanchen/cropper#methods

- 前端首选微软雅黑字体设定  
http://blog.csdn.net/seng3018/article/details/7484587

- Java 实现二维码及有Logo 的二维码（SpringMVC+Zxing+Jsp）  
http://blog.csdn.net/wenteryan/article/details/51373712

- layer confirm

## shiro
1. shiro 官方reference文档 http://shiro.apache.org/reference.html
1. sessionId过期问题
    1. http://shiro.apache.org/web.html#Web-NativeSessionTimeout
    1. session 验证任务 sessionValidationScheduler http://shiro.apache.org/session-management.html
    1. 
    

## hibernate

1. eager load 转换为 lazy load的问题  
   1. JPA 2.1 Entity Graph - Part 1: Named entity graphs
   http://www.tuicool.com/articles/aUf6ze
   1.How to override FetchType.EAGER to be lazy at runtime
   http://stackoverflow.com/questions/10997321/how-to-override-fetchtype-eager-to-be-lazy-at-runtime?lq=1
   1.Ignore a FetchType.EAGER in a relationship
   http://stackoverflow.com/questions/17847289/ignore-a-fetchtype-eager-in-a-relationship
   1.
1. Criteria Queries
    1.https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html/querycriteria.html#querycriteria-typedquery-multiselect
1. When to use the select clause in the JPA criteria API?
    1.http://stackoverflow.com/questions/30290824/when-to-use-the-select-clause-in-the-jpa-criteria-api

